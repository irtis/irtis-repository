package cz.muni.irtis.repository.database.questionnaires;


import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.database.identity.PersonEntity;
import cz.muni.irtis.repository.model.utils.Time;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Optional;
import java.util.TimeZone;

@Entity
@Table(name = "people_questionnaires")
public class PersonQuestionnaireEntity extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "person_id", nullable = false)
    private PersonEntity person;
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "questionnaire_id", nullable = false)
    private QuestionnaireEntity questionnaire;

    // TODO: delete. Keep until the DB schema normalized by adding a AssignedQuestionnaire entity
    @Column(name = "assignment_id")
    private Long assignmentId;

    private Long created;       // Time when the survey was created by the administrator or system
    private Long updated;       // Time when the survey was updated by the administrator
    private Long downloaded;    // Time when the client downloaded the survey
    private Long starting;      // Time when the survey can be available to the user
    private Long ending;        // Time when the survey cannot be available to the user
    private Long invoking;        // Time when the survey can become available to the user
    private Long closing;       // Time when the survey would be open for the user to answer (closing = ([starting, ending] -> NOW() + validity * 60000)
    private Integer buffer;

    @Column(name = "max_notifications")
    private Integer maxNotifications;
    @Column(name = "notification_period")
    private String notificationPeriod;
    private Integer validity;

    private Long notified;      // Time when the survey was notified to the user via notification GUI
    private Long silenced;      // Time when the survey was silenced by the user
    private Long opened;        // Time when the survey was opened by the user
    private Long completed;     // Time when the survey was completed by the user

    private Long uploaded;      // Time when the survey was received from the user on the server

    @Transient
    private String answeredExport;
    @Transient
    private int dayInStudy;
    @Transient
    private long notifiedCount;

    public PersonQuestionnaireEntity() {
    }

    /**
     * Constructor for creating a new entity.
     * Usually for questionnaires created from templates on client application.
     *
     * @param person
     * @param questionnaire
     */
    public PersonQuestionnaireEntity(PersonEntity person, QuestionnaireEntity questionnaire) {
        this.id = null;
        this.person = person;
        this.questionnaire = questionnaire;
        this.created = System.currentTimeMillis();
        this.updated = System.currentTimeMillis();
    }


    public Long getId() {
        return id;
    }

    public PersonEntity getPerson() {
        return person;
    }

    public QuestionnaireEntity getQuestionnaire() {
        return questionnaire;
    }

    public Long getCreated() {
        return created;
    }

    public Long getUpdated() {
        return updated;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }

    public Long getStarting() {
        return starting;
    }

    public void setStarting(Long starting) {
        this.starting = starting;
    }

    public Long getEnding() {
        return ending;
    }

    public void setEnding(Long ending) {
        this.ending = ending;
    }

    public Long getInvoking() {
        return invoking;
    }

    public void setInvoking(Long invoking) {
        this.invoking = invoking;
    }

    public Long getClosing() {
        return closing;
    }

    public Long getNotified() {
        return notified;
    }

    public Long getSilenced() {
        return silenced;
    }

    public Long getOpened() {
        return opened;
    }

    public Long getCompleted() {
        return completed;
    }

    public Long getDownloaded() {
        return downloaded;
    }

    public Long getUploaded() {
        return uploaded;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPerson(PersonEntity person) {
        this.person = person;
    }

    public void setQuestionnaire(QuestionnaireEntity questionnaire) {
        this.questionnaire = questionnaire;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public void setNotified(Long notified) {
        this.notified = notified;
    }

    public void setSilenced(Long silenced) {
        this.silenced = silenced;
    }

    public void setOpened(Long opened) {
        this.opened = opened;
    }

    public void setCompleted(Long completed) {
        this.completed = completed;
    }

    public void setClosing(Long closing) {
        this.closing = closing;
    }

    public void setDownloaded(Long downloaded) {
        this.downloaded = downloaded;
    }

    public void setDownloaded() {
        setDownloaded(System.currentTimeMillis());
    }

    public void setUploaded(Long uploaded) {
        this.uploaded = uploaded;
    }

    public void setUploaded() {
        setUploaded(System.currentTimeMillis());
    }

    public int getDayInStudy() {
        return dayInStudy;
    }

    public void setDayInStudy(int dayInStudy) {
        this.dayInStudy = dayInStudy;
    }

    public long getNotifiedCount() {
        return notifiedCount;
    }

    public void setNotifiedCount(long notifiedCount) {
        this.notifiedCount = notifiedCount;
    }

    public String getAnsweredExport() {
        return answeredExport;
    }

    public void setAnsweredExport(String answeredExport) {
        this.answeredExport = answeredExport;
    }

    public String getCreatedFormatted() {
        return Time.getTimeFormatted(getCreated());
    }

    public String getClosingFormatted() {
        return Time.getTimeFormatted(getClosing());
    }

    public String getCompletedFormatted() {
        return Time.getTimeFormatted(getCompleted());
    }

    public String getDownloadedFormatted() {
        return Time.getTimeFormatted(getDownloaded());
    }

    public String getUploadedFormatted() {
        return Time.getTimeFormatted(getUploaded());
    }

    public String getUploadedFileFormat() {
        return Time.getFileFormatDateTime(getUploaded());
    }

    public String getOpenedFormatted() {
        return Time.getTimeFormatted(getOpened());
    }

    public String getNotifiedFormatted() {
        return Time.getTimeFormatted(getNotified());
    }

    public String getSilencedFormatted() {
        return Time.getTimeFormatted(getSilenced());
    }

    public String getStartingFormatted() {
        return Time.getTimeFormatted(getStarting());
    }

    public String getEndingFormatted() {
        return Time.getTimeFormatted(getEnding());
    }

    public String getUpdatedFormatted() {
        return Time.getTimeFormatted(getUpdated());
    }

    public Long getPersonId() {
        if(getPerson()!=null) {
            return getPerson().getId();
        } else {
            return null;
        }
    }

    public String getPersonName() {
        if(getPerson()!=null) {
            return getPerson().getName();
        } else {
            return null;
        }
    }

    public String getQuestionnaireTitle() {
        if(getQuestionnaire()!=null) {
            return getQuestionnaire().getTitle();
        } else {
            return null;
        }
    }

    public Long getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(Long assignmentId) {
        this.assignmentId = assignmentId;
    }

    public void setStarting(String date, String time) {
        Time.getLocalMilis(date, time).ifPresent(this::setStarting);
    }

    public void setEnding(String date, String time) {
        Time.getLocalMilis(date, time).ifPresent(this::setEnding);
    }

    public Integer getMaxNotifications() {
        return maxNotifications;
    }

    public void setMaxNotifications(Integer maxNotifications) {
        this.maxNotifications = maxNotifications;
    }

    public String getNotificationPeriod() {
        return notificationPeriod;
    }

    public void setNotificationPeriod(String notificationPeriod) {
        this.notificationPeriod = notificationPeriod;
    }

    public String getValidFrom() {
        return Time.getUiDate(starting);
    }

    public String getValidTo() {
        return Time.getUiDate(ending);
    }

    public String getStartingTime() {
        return Time.getUiHours(starting);
    }

    public String getEndingTime() {
        return Time.getUiHours(ending);
    }

    public String getInvokedTime() {
        if (invoking != null) {
            return Time.getUiHours(invoking);
        } else {
            return "";
        }
    }

    public Integer getValidity() {
        return validity;
    }

    public void setValidity(Integer validity) {
        this.validity = validity;
    }

    public Integer getBuffer() {
        return buffer;
    }

    public void setBuffer(Integer buffer) {
        this.buffer = buffer;
    }
}