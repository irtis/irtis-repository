package cz.muni.irtis.repository.database.questionnaires;

import cz.muni.irtis.repository.model.questionnaires.AssignedQuestionnaire;

import java.util.List;
import java.util.Optional;

public interface PersonsQuestionnairesRepositoryExtended {
    /**
     * Finds PersonQuestionnaireEntity and groups them by assignmentId into AssignedQuestionnaire.
     * @return List of AssignedQuestionnaire representing assignments
     */
    List<AssignedQuestionnaire> findAllAssignments();

    /**
     * Finds PersonQuestionnaireEntity group by assignmentId
     * @param assignmentId group id
     * @return AssignedQuestionnaire representing assignments
     */
    Optional<AssignedQuestionnaire> findByAssignmentId(Long assignmentId);

    /**
     * Finds PersonQuestionnaireEntity group by assignmentId and deletes them
     * @param assignmentId group id
     */
    void deleteByAssignmentId(Long assignmentId);

    /**
     * Creates a group of PersonQuestionnaireEntity. Sets the same assignmentId for all based on DB sequence.
     * @param assignment assignmentId should be empty
     */
    void createAssignment(AssignedQuestionnaire assignment);

    /**
     * Updates, removes, or adds members of a group of PersonQuestionnaireEntity.
     * @param assignment assignmentId needs to be set
     */
    void updateAssignment(AssignedQuestionnaire assignment);
}
