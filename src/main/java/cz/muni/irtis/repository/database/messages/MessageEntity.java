package cz.muni.irtis.repository.database.messages;


import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.database.identity.PersonEntity;
import cz.muni.irtis.repository.model.messages.Message;
import cz.muni.irtis.repository.model.utils.Time;

import javax.persistence.*;

@Entity
@Table(name = "messages")
public class MessageEntity extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "person_id", nullable = true)
    private PersonEntity person;
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "creator_id", nullable = true)
    private PersonEntity creator;
    private Integer type;
    @Column(name="text", length=1024)
    private String text;
    @Column(name="response", length=1024)
    private String response;
    private Long created;
    private Long updated;
    @Column(name="valid_from")
    private Long validFrom;
    @Column(name="valid_to")
    private Long validTo;
    private Long downloaded;
    private Long notified;
    private Long opened;
    private Long read;
    private Long uploaded;

    public MessageEntity() {
    }

    public MessageEntity(Long created, String text, PersonEntity person) {
        this.created = created;
        this.text = text;
        this.person = person;
    }

    static public MessageEntity build(Message item) {
        MessageEntity entity = new MessageEntity();
        entity.setId(item.getId());
        if(item.getPerson()!=null) {
            entity.setPerson(PersonEntity.build(item.getPerson()));
        }
        if(item.getCreator()!=null) {
            entity.setCreator(PersonEntity.build(item.getCreator()));
        }
        entity.setType(item.getType());
        entity.setText(item.getText());
        entity.setResponse(item.getResponse());
        entity.setCreated(item.getCreated());
        entity.setUpdated(item.getUpdated());
        entity.setValidFrom(item.getValidFrom());
        entity.setValidTo(item.getValidTo());
        entity.setDownloaded(item.getDownloaded());
        entity.setNotified(item.getNotified());
        entity.setOpened(item.getOpened());
        entity.setRead(item.getRead());
        entity.setUploaded(item.getUploaded());
        return entity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonEntity getPerson() {
        return person;
    }

    public void setPerson(PersonEntity person) {
        this.person = person;
    }

    public PersonEntity getCreator() {
        return creator;
    }

    public void setCreator(PersonEntity creator) {
        this.creator = creator;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Long getUpdated() {
        return updated;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public Long getValidTo() {
        return validTo;
    }

    public void setValidTo(Long validTo) {
        this.validTo = validTo;
    }

    public Long getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(Long downloaded) {
        this.downloaded = downloaded;
    }

    public Long getNotified() {
        return notified;
    }

    public void setNotified(Long notified) {
        this.notified = notified;
    }

    public Long getOpened() {
        return opened;
    }

    public void setOpened(Long opened) {
        this.opened = opened;
    }

    public Long getRead() {
        return read;
    }

    public void setRead(Long read) {
        this.read = read;
    }

    public Long getUploaded() {
        return uploaded;
    }

    public void setUploaded(Long uploaded) {
        this.uploaded = uploaded;
    }

    public String getNotifiedString() {
        return Time.getTimeFormatted(notified);
    }

    public String getOpenedString() {
        return Time.getTimeFormatted(opened);
    }

    public String getCreatedString() {
        return Time.getTimeFormatted(created);
    }

    public String getDownloadedString() {
        return Time.getTimeFormatted(downloaded);
    }

    public String getReadString() {
        return Time.getTimeFormatted(read);
    }
}
