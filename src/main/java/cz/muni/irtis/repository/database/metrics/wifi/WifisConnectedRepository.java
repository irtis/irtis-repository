package cz.muni.irtis.repository.database.metrics.wifi;

import cz.muni.irtis.repository.database.metrics.MetricIdentityEntity;
import cz.muni.irtis.repository.database.metrics.export.WifiConnectedExport;
import cz.muni.irtis.repository.database.metrics.transfer.WifiDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WifisConnectedRepository extends JpaRepository<WifiConnectedEntity, MetricIdentityEntity> {
    @Query("SELECT new cz.muni.irtis.repository.database.metrics.transfer.WifiDTO(t.time) " +
            "FROM WifiConnectedEntity e " +
            "JOIN TimeEntity t ON t.id = e.metricIdentity.datetimeId " +
            "WHERE person_id = ?1 " +
            "AND t.time >= ?2 AND t.time <= ?3 " +
            "ORDER BY t.time ASC")
    List<WifiDTO> findAllByPersonAndIntervalDto(Long person_id, Long tMin, Long tMax);

    @Query("SELECT new cz.muni.irtis.repository.database.metrics.transfer.WifiDTO(t.time) " +
            "FROM WifiConnectedEntity e " +
            "JOIN TimeEntity t ON t.id = e.metricIdentity.datetimeId " +
            "WHERE t.time >= ?1 AND t.time <= ?2 " +
            "ORDER BY t.time ASC")
    List<WifiDTO> findAllByInterval(Long tMin, Long tMax);

    @Query("SELECT new cz.muni.irtis.repository.database.metrics.export.WifiConnectedExport(t.time, e.metricIdentity.personId, e.wifiSsid.ssid) " +
            "FROM WifiConnectedEntity e " +
            "JOIN TimeEntity t ON t.id = e.metricIdentity.datetimeId " +
            "WHERE person_id = ?1 " +
            "AND t.time >= ?2 AND t.time <= ?3 " +
            "ORDER BY t.time ASC")
    List<WifiConnectedExport> findAllByPersonAndInterval(Long person_id, Long tMin, Long tMax);
}
