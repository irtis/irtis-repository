package cz.muni.irtis.repository.database.questionnaires;

import java.util.List;

public interface QuestionnaireRepositoryExtended {
    QuestionnaireEntity saveWithHistory(QuestionnaireEntity questionnaire);
    List<AnswerLink> getOwnerAnswerLinks(Long ownerId);
}
