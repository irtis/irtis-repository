package cz.muni.irtis.repository.database.persistence;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum EventType {
    EVENT_CAPTURING_STARTED("Capturing started", 1),
    EVENT_CAPTURING_STOPPED("Capturing stopped", 2),
    EVENT_CAPTURING_SCREENSHOTS_STARTED("Capturing screenshots started", 3),
    EVENT_CAPTURING_SCREENSHOTS_STOPPED("Capturing screenshots stopped", 4),

    EVENT_QUESTIONNAIRES_NOTIFIED("Questionnaires notified", 5),
    EVENT_QUESTIONNAIRES_POSTPONED("Questionnaires postponed", 6),
    EVENT_QUESTIONNAIRES_SILENCED("Questionnaires silenced", 7),
    EVENT_QUESTIONNAIRES_DOWNLOADED("Questionnaires downloaded", 8),
    EVENT_QUESTIONNAIRES_UPLOADED("Questionnaires uploaded", 9),
    EVENT_QUESTIONNAIRES_OPENED("Questionnaires opened", 10),
    EVENT_QUESTIONNAIRES_COMPLETED("Questionnaires completed", 11),
    EVENT_QUESTIONNAIRES_TEMPLATES_DOWNLOADED("Questionnaires templates downloaded", 18),
    EVENT_QUESTIONNAIRES_INVOCATION_ACTIVE("Questionnaires invocation is active", 19),
    EVENT_QUESTIONNAIRES_INVOCATION_INACTIVE("Questionnaires invocation is not active", 20),
    EVENT_QUESTIONNAIRES_INVOCATION_EMPTY("Questionnaires invocation is empty", 21),
    EVENT_QUESTIONNAIRES_INVOCATION_NOTHINGTOSHOW("Questionnaires invocation has nothing to show now", 22),

    EVENT_STORAGE_INSUFFICIENT_SPACE("Insufficient storage space", 12),

    EVENT_SYNCHRONIZATION_PROCESSING("Synchronization processing", 13),

    EVENT_MESSAGES_DOWNLOADED("Messages downloaded", 14),
    EVENT_MESSAGES_NOTIFIED("Messages notified", 15),
    EVENT_MESSAGES_READ("Messages read", 16),
    EVENT_MESSAGES_UPLOADED("Messages uploaded", 17);


    private String type;
    private int value;

    private static final Map<Integer, String> values = Arrays.stream(
            EventType.values())
                .collect(Collectors
                        .toMap(EventType::getValue, EventType::getType));

    EventType(String type, int value) {
        this.type = type;
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public String getType() {
        return type;
    }

    public static String getType(int code) {
        return values.get(code);
    }
}
