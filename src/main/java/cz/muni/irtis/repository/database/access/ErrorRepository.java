package cz.muni.irtis.repository.database.access;

import cz.muni.irtis.repository.database.identity.PersonEntity;
import cz.muni.irtis.repository.database.messages.MessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ErrorRepository extends JpaRepository<ErrorEntity, Long> {
    List<ErrorEntity> findAllByPersonInAndTimeBetween(List<PersonEntity> people, Long studyStart, Long studyEnd);
}
