package cz.muni.irtis.repository.database.metrics.transfer;

public class ScreenDTO {
    private Long time;
    private Boolean state;

    public ScreenDTO(Long time, Boolean state) {
        this.time = time;
        this.state = state;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }
}
