package cz.muni.irtis.repository.database.metrics;


import cz.muni.irtis.repository.database.metrics.export.BatteryExport;
import cz.muni.irtis.repository.database.metrics.transfer.BatteryDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BatteryRepository extends JpaRepository<BatteryEntity, MetricIdentityEntity> {
    @Query("SELECT new cz.muni.irtis.repository.database.metrics.transfer.BatteryDTO(t.time, e.statePercent) " +
            "FROM BatteryEntity e " +
            "JOIN TimeEntity t ON t.id = e.metricIdentity.datetimeId " +
            "WHERE person_id = ?1 " +
            "AND t.time >= ?2 AND t.time <= ?3 " +
            "ORDER BY t.time ASC")
    List<BatteryDTO> findAllByPersonAndInterval(Long person_id, Long tMin, Long tMax);

    @Query("SELECT new cz.muni.irtis.repository.database.metrics.transfer.BatteryDTO(t.time, e.statePercent) " +
            "FROM BatteryEntity e " +
            "JOIN TimeEntity t ON t.id = e.metricIdentity.datetimeId " +
            "WHERE t.time >= ?1 AND t.time <= ?2 " +
            "ORDER BY t.time ASC")
    List<BatteryDTO> findAllByInterval(Long tMin, Long tMax);

    @Query("SELECT new cz.muni.irtis.repository.database.metrics.export.BatteryExport(t.time, e.metricIdentity.personId, e.statePercent) " +
            "FROM BatteryEntity e " +
            "JOIN TimeEntity t ON t.id = e.metricIdentity.datetimeId " +
            "WHERE person_id = ?1 " +
            "AND t.time >= ?2 AND t.time <= ?3 " +
            "ORDER BY t.time ASC")
    List<BatteryExport> findAllByPersonAndIntervalExport(Long person_id, Long tMin, Long tMax);
}
