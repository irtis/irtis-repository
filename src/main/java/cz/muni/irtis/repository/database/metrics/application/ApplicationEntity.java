package cz.muni.irtis.repository.database.metrics.application;

import cz.muni.irtis.repository.database.EntityBase;

import javax.persistence.*;

@Entity
@Table(
    name = "metrics_applications",
    indexes = {
        @Index(name = "index_name", columnList="name", unique = true)
    }
)
public class ApplicationEntity extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    public ApplicationEntity() {
    }

    public ApplicationEntity(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
