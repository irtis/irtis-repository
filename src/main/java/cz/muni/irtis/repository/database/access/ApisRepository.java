package cz.muni.irtis.repository.database.access;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ApisRepository extends JpaRepository<ApiEntity, Long> {
    Optional<ApiEntity> findByIdentifierAndKey(@Param("identifier") String identifier, @Param("key") String key);
}
