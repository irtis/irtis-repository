package cz.muni.irtis.repository.database.metrics.application;

import cz.muni.irtis.repository.database.metrics.MetricIdentityEntity;
import cz.muni.irtis.repository.database.metrics.export.ApplicationBackgroundExport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ApplicationsBackgroundRepository extends JpaRepository<ApplicationBackgroundEntity, MetricIdentityEntity> {
    @Query(
            "select new cz.muni.irtis.repository.database.metrics.export.ApplicationBackgroundExport(t.time, maf.metricIdentity.personId, ma.name, ma.id) " +
                    "from ApplicationBackgroundEntity maf " +
                    "join ApplicationEntity ma on ma.id = maf.application.id " +
                    "join TimeEntity t on t.id = maf.metricIdentity.datetimeId " +
                    "where person_id = ?1 " +
                    "and t.time >= ?2 and t.time <= ?3 " +
                    "order by t.time ASC")
    List<ApplicationBackgroundExport> findAllByPersonAndInterval(Long personId, Long tMin, Long tMax);
}
