package cz.muni.irtis.repository.database.metrics;

import javax.persistence.*;

@Entity
@Table(
    name = "metrics_time",
    indexes = {
        @Index(name = "index_time", columnList="time", unique = false)
    }
)
public class TimeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long time;
//    private LocalDateTime dateTime;


    public TimeEntity() {
    }

    public TimeEntity(Long dateTime) {
        this.time = dateTime;
    }

    public TimeEntity(long dateTime) {
        setTime(dateTime);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

//    public void setDateTime(long dateTime) {
//        if (dateTime < 9999999999L) {
//            throw new IllegalArgumentException("The timestamp is < 9999999999. This endpoint consumes timestamps" +
//                    " in the java format (System.currenttimemilis) which is unix_timestamp * 1000.");
//        }
//
//        this.dateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(dateTime),
//                TimeZone.getDefault().toZoneId());
//    }
}
