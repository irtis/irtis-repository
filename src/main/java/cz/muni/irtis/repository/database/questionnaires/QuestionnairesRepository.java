package cz.muni.irtis.repository.database.questionnaires;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface QuestionnairesRepository extends JpaRepository<QuestionnaireEntity, Long>, QuestionnaireRepositoryExtended {
    Optional<QuestionnaireEntity> findByIdentifier(@Param("identifier") String identifier);

    @Query(value = "SELECT e FROM QuestionnaireEntity e WHERE e.type = :type")
    Optional<List<QuestionnaireEntity>> findByType(@Param("type") Integer type);

    @Query(value = "SELECT e FROM QuestionnaireEntity e WHERE e.type = :type AND e.validFrom <= :now AND e.validTo >= :now")
    Optional<List<QuestionnaireEntity>> findByType(@Param("type") Integer type, @Param("now") Long now);
}

