package cz.muni.irtis.repository.database.persistence;

import cz.muni.irtis.repository.database.identity.PersonEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AliveRepository extends JpaRepository<AliveEntity, Long> {
    @Query("SELECT a " +
            "FROM AliveEntity a " +
            "WHERE a.person = ?1 " +
            "AND a.time >= ?2 AND a.time <= ?3 " +
            "ORDER BY a.time ASC")
    Optional<List<AliveEntity>> findAllOfAliveByPersonAndInterval(PersonEntity person, Long tMin, Long tMax);

    @Query("SELECT a " +
            "FROM AliveEntity a " +
            "WHERE a.person = ?1 " +
            "ORDER BY a.time DESC")
    Page<AliveEntity> findAllOfAliveByPerson(PersonEntity person, PageRequest page);

    AliveEntity findTopById(Long id);

    @Query(value = "SELECT a.* " +
            "FROM alive a " +
            "WHERE a.person_id = :id " +
            "ORDER BY a.time DESC " +
            "LIMIT 1", nativeQuery = true
    )
    AliveEntity findAllOfAliveByPerson1(Long id);

    @Query(value = "SELECT a.* " +
            "FROM alive a " +
            "WHERE a.person_id = :id AND a.screenshots = true " +
            "ORDER BY a.time DESC " +
            "LIMIT 1", nativeQuery = true
    )
    AliveEntity getLastScreenshot(Long id);
}
