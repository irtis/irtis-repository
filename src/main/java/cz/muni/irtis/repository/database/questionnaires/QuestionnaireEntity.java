package cz.muni.irtis.repository.database.questionnaires;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.model.questionnaires.Questionnaire;
import cz.muni.irtis.repository.model.utils.Time;

import javax.persistence.*;
import java.text.Normalizer;
import java.util.Set;

@Entity
@Table(name = "questionnaires")
public class QuestionnaireEntity extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer type;
    @Column(unique=true)
    private String identifier;
    private String title;
    private Integer validity;
    @Column(name = "silence_delay")
    private Integer silenceDelay;
    @Column(name = "social_group")
    private String group;
    private Long created;
    private Long updated;
    private String tag;
    private Boolean configurable;

    // TODO: remove
    @JsonIgnore
    @Column(name = "valid_from")
    private Long validFrom;
    @JsonIgnore
    @Column(name = "valid_to")
    private Long validTo;

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="questionnaire", orphanRemoval=true)
    @JsonManagedReference
    private Set<QuestionEntity> questions;

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="questionnaire", orphanRemoval=true)
    @JsonManagedReference
    private Set<AnswerLink> answerLinks;


    public QuestionnaireEntity() {
    }

    static public QuestionnaireEntity build(Questionnaire item) {
        QuestionnaireEntity entity = new QuestionnaireEntity();
        entity.id = item.getId();
        entity.type = item.getType();
        entity.identifier = item.getIdentifier();
        entity.title = item.getTitle();
        entity.validity = item.getValidity();
        entity.silenceDelay = item.getSilenceDelay();
        entity.group = item.getGroup();
        entity.created = item.getCreated();
        entity.updated = item.getUpdated();
        entity.configurable = item.getConfigurable();
        return entity;
    }

    public QuestionnaireEntity(Long id, Integer type, String identifier, String title, Integer validity, Integer silenceDelay, String group, Long created, Long updated) {
        this.id = id;
        this.type = type;
        this.identifier = identifier;
        this.title = title;
        this.validity = validity;
        this.silenceDelay = silenceDelay;
        this.group = group;
        this.created = created;
        this.updated = updated;
    }


    public Long getId() {
        return id;
    }

    public Integer getType() {
        return type;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getTitle() {
        return title;
    }

    public Integer getValidity() {
        return validity;
    }

    public Integer getSilenceDelay() {
        return silenceDelay;
    }

    public String getGroup() {
        return group;
    }

    public Long getCreated() {
        return created;
    }

    public Long getUpdated() {
        return updated;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setValidity(Integer validity) {
        this.validity = validity;
    }

    public void setSilenceDelay(Integer silenceDelay) {
        this.silenceDelay = silenceDelay;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }
    public void setUpdated() {
        setUpdated(System.currentTimeMillis());
    }

    public String getCreatedFormatted() {
        return Time.getTimeFormatted(getCreated());
    }

    public Set<QuestionEntity> getQuestions() {
        return questions;
    }

    public void setQuestions(Set<QuestionEntity> questions) {
        this.questions = questions;
    }

    public int getQuestionCount() {
        return questions == null
                ? 0
                : questions.size();
    }

    // TODO: delete & rewrite assignedquestionnaire template
    public String getName() {
        return this.getTitle();
    }

    public String getSelectString() {
        return id + "; " + title;
    }

    public Set<AnswerLink> getAnswerLinks() {
        return answerLinks;
    }

    public void setAnswerLinks(Set<AnswerLink> answerLinks) {
        this.answerLinks = answerLinks;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public Long getValidTo() {
        return validTo;
    }

    public void setValidTo(Long validTo) {
        this.validTo = validTo;
    }

    public Boolean getConfigurable() {
        return configurable;
    }

    public void setConfigurable(Boolean configurable) {
        this.configurable = configurable;
    }

    /**
     * Create identifier from title by:
     *  removing all non-word characters,
     *  replacing spaces by '-',
     *  lowercasing,
     *  adding id to the end.
     *
     * @throws IllegalStateException if id or title is null
     */
    public void createIdentifier() {
        if (id == null || title == null) {
            throw new IllegalStateException("Cannot create identifier. Id od title is null.");
        }
        identifier = Normalizer.normalize(title, Normalizer.Form.NFD)
            .trim()
            .toLowerCase()
            .replaceAll("(?!\\s)\\W", "")
            .replaceAll(" ", "-")
                + "-" + id;
    }
}