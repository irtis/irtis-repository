package cz.muni.irtis.repository.database.metrics.export;

public class ScreenTapsExport extends MetricExport {
    // "taken_on","datetime_id","person_id","x","y"
    // time;personId;name;x;y

    private Integer x;
    private Integer y;

    public ScreenTapsExport(Long time, Long personId, Integer x, Integer y) {
        this.x = x;
        this.y = y;
        super.setTime(time);
        super.setPersonId(personId);
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }
}
