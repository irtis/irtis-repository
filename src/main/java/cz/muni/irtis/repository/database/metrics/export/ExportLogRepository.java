package cz.muni.irtis.repository.database.metrics.export;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ExportLogRepository extends JpaRepository<ExportLogEntity, Long> {
    Optional<ExportLogEntity> findFirstByStudyName(String studyName);
}
