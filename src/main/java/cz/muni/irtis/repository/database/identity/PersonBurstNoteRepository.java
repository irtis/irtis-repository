package cz.muni.irtis.repository.database.identity;

import cz.muni.irtis.repository.database.notes.PersonBurstNoteEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PersonBurstNoteRepository extends JpaRepository<PersonBurstNoteEntity, Long> {
    Optional<PersonBurstNoteEntity> findFirstByPersonBurstId(Long personBurstId);
}
