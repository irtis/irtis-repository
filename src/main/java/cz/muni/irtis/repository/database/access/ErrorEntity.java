package cz.muni.irtis.repository.database.access;

import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.database.identity.PersonEntity;
import cz.muni.irtis.repository.model.access.Access;
import cz.muni.irtis.repository.model.access.Error;
import cz.muni.irtis.repository.model.utils.Time;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "errors")
public class ErrorEntity extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "person_id", nullable = true)
    private PersonEntity person;
    @Column(name="type")
    private Integer type;
    @Column(name="identifier")
    private String identifier;
    @Column(name="content", length=1024*10)
    private String content;
    @Column(name="location")
    private String location;
    @Column(name="time")
    private Long time;

    public ErrorEntity() {
    }

    static public ErrorEntity build(Error item) {
        ErrorEntity entity = new ErrorEntity();
        entity.setId(item.getId());
        if(item.getPerson()!=null) {
            entity.setPerson(PersonEntity.build(item.getPerson()));
        }
        entity.setType(item.getType());
        entity.setIdentifier(item.getIdentifier());
        entity.setContent(item.getContent());
        entity.setLocation(item.getLocation());
        entity.setTime(item.getTime());
        return entity;
    }


    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPerson(PersonEntity person) {
        this.person = person;
    }

    public PersonEntity getPerson() {
        return person;
    }

    public Long getPersonId() {
        if(getPerson()!=null) {
            return getPerson().getId();
        } else {
            return null;
        }
    }

    public String getPersonName() {
        if(getPerson()!=null) {
            return getPerson().getName();
        } else {
            return null;
        }
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Long getTime() {
        return time;
    }

    // TODO: move to Time
    public String getTimeFormatted() {
        DateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.SSS");
        Date result = new Date(getTime());
        return format.format(result);
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getTimeString() {
        return Time.getTimeFormattedForUi(time);
    }


}
