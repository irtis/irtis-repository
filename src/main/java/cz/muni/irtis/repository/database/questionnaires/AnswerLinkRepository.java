package cz.muni.irtis.repository.database.questionnaires;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnswerLinkRepository extends JpaRepository<AnswerLink, AnswerLinkPK> {
}
