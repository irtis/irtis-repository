package cz.muni.irtis.repository.database.access;

import javax.persistence.*;

@Entity
@Table(name = "updates")
public class UpdateEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "version_code")
    private Integer version;
    @Column(name = "version_name")
    private String versionName;
    private Long created;
    private String source;

    public Long getId() {
        return id;
    }

    public Integer getVersion() {
        return version;
    }

    public String getVersionName() {
        return versionName;
    }

    public Long getCreated() {
        return created;
    }

    public String getSource() {
        return source;
    }
}
