package cz.muni.irtis.repository.database.questionnaires;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

//@Entity
//@Table(name = "answer_template")
//public class AnswerTemplate {
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JsonIgnore
//    private QuestionTemplate question;
//
//    private Integer position;
//    private String text;
//    private Integer sliderNo;
//    private Integer link;
//    private String exclusions;
//
//    public Long getId() {
//        return id;
//    }
//
//    public QuestionTemplate getQuestion() {
//        return question;
//    }
//
//    public void setQuestion(QuestionTemplate question) {
//        this.question = question;
//    }
//
//    public Integer getPosition() {
//        return position;
//    }
//
//    public void setPosition(Integer position) {
//        this.position = position;
//    }
//
//    public String getText() {
//        return text;
//    }
//
//    public void setText(String text) {
//        this.text = text;
//    }
//
//    public Integer getSliderNo() {
//        return sliderNo;
//    }
//
//    public void setSliderNo(Integer sliderNo) {
//        this.sliderNo = sliderNo;
//    }
//
//    public Integer getLink() {
//        return link;
//    }
//
//    public void setLink(Integer link) {
//        this.link = link;
//    }
//
//    public String getExclusions() {
//        return exclusions;
//    }
//
//    public void setExclusions(String exclusions) {
//        this.exclusions = exclusions;
//    }
//}
