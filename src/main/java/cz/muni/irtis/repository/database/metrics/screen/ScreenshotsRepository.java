package cz.muni.irtis.repository.database.metrics.screen;

import cz.muni.irtis.repository.database.metrics.MetricIdentityEntity;
import cz.muni.irtis.repository.database.metrics.export.ScreenshotExport;
import cz.muni.irtis.repository.database.metrics.screen.ScreenshotEntity;
import cz.muni.irtis.repository.database.metrics.transfer.ScreenshotDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScreenshotsRepository extends JpaRepository<ScreenshotEntity, MetricIdentityEntity> {
    @Query("SELECT new cz.muni.irtis.repository.database.metrics.transfer.ScreenshotDTO(t.time) " +
            "FROM ScreenshotEntity e " +
            "JOIN TimeEntity t ON t.id = e.metricIdentity.datetimeId " +
            "WHERE person_id = ?1 " +
            "AND t.time >= ?2 AND t.time <= ?3 " +
            "ORDER BY t.time ASC")
    List<ScreenshotDTO> findAllByPersonAndInterval(Long person_id, Long tMin, Long tMax);

    @Query("SELECT new cz.muni.irtis.repository.database.metrics.transfer.ScreenshotDTO(t.time) " +
            "FROM ScreenshotEntity e " +
            "JOIN TimeEntity t ON t.id = e.metricIdentity.datetimeId " +
            "WHERE t.time >= ?1 AND t.time <= ?2 " +
            "ORDER BY t.time ASC")
    List<ScreenshotDTO> findAllByInterval(Long tMin, Long tMax);

    @Query("SELECT new cz.muni.irtis.repository.database.metrics.export.ScreenshotExport(t.time, e.metricIdentity.personId, e.filename, e.metadata) " +
            "FROM ScreenshotEntity e " +
            "JOIN TimeEntity t ON t.id = e.metricIdentity.datetimeId " +
            "WHERE person_id = ?1 " +
            "AND t.time >= ?2 AND t.time <= ?3 " +
            "ORDER BY t.time ASC")
    List<ScreenshotExport> findAllByPersonAndIntervaExport(Long person_id, Long tMin, Long tMax);

    @Query(value = "SELECT * FROM metrics_screenshots s WHERE person_id = :person AND filename = :filename", nativeQuery = true)
    ScreenshotEntity getOneByPersonAndFilename(@Param("person") Long person, @Param("filename") String filename);
}
