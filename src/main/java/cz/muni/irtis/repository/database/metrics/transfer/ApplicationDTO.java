package cz.muni.irtis.repository.database.metrics.transfer;


import cz.muni.irtis.repository.model.utils.Time;

public class ApplicationDTO {
    private Long id;
    private Long time;
    private String name;

    public ApplicationDTO(Long id, Long time, String name) {
        this.id = id;
        this.time = time;
        this.name = name;
    }

    public ApplicationDTO(Long time, String name) {
        this.time = time;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public Long getTime() {
        return time;
    }

    public String getTimeFormatted() {
        return Time.getElapsedTimeFormatted(getTime());
    }

    public Long getTimePerPerson() {
        return getTime()/14;
    }
    public Long getTimePerPersonDay() {
        return getTimePerPerson()/7;
    }

    public String getTimePerPersonDayFormatted() {
        return Time.getElapsedTimeFormatted(getTimePerPersonDay());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
