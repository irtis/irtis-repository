package cz.muni.irtis.repository.database.questionnaires;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.muni.irtis.repository.database.EntityBase;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@Entity
@Table(name = "questionnaires_answers")
public class AnswerEntity extends EntityBase {
//    @EmbeddedId
//    private AnswerId answer;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
//    @ManyToOne
//    @JoinColumn(name="question_id", nullable=false)
//    private QuestionEntity question;
//    @ManyToOne
//    @JoinColumn(name="questionnaire_id", nullable=false)
//    private QuestionnaireEntity questionnaire;

    @ManyToOne
    @JoinColumn(name="question_template_id", foreignKey=@ForeignKey(name="fk_question_template_id"))
    @JsonBackReference
    private QuestionTemplate questionTemplate;

    private Integer link;
    private String exclusions;
    private String text;
    private Integer position;
    private Integer sliderNo;

    public AnswerEntity() {
    }

    public AnswerEntity(QuestionEntity question, QuestionnaireEntity questionnaire, Integer link, String exclusions, String text, Integer position) {
//        this.question = question;
//        this.questionnaire = questionnaire;
        this.link = link;
        this.exclusions = exclusions;
        this.text = text;
        this.position = position;
    }

    public Long getId() {
        return id;
    }

//    public QuestionEntity getQuestion() {
//        return question;
//    }
//
//    public QuestionnaireEntity getQuestionnaire() {
//        return questionnaire;
//    }

    public Integer getLink() {
        return link;
    }

    public String getText() {
        return text;
    }

    public Integer getPosition() {
        return position;
    }

    public List<Integer> getExclusions() {
        if(exclusions!=null) {
            List<Integer> items = new ArrayList<>();
            List<String> list = Arrays.asList(exclusions.split(","));
            Iterator<String> iterator = list.iterator();
            while (iterator.hasNext()) {
                items.add(Integer.valueOf(iterator.next().trim()));
            }
            return items;
        } else {
            return null;
        }
    }

    public String getExclusionsString() {
        return exclusions;
    }

    public void setExclusions(List<Integer> items) {
        if(items!=null) {
            exclusions = StringUtils.join(items, ","); // String join is in java.lang.string.join()
        }
    }

    public void setExclusionsString(String exclusions) {
        this.exclusions = exclusions;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    public void setQuestion(QuestionEntity question) {
//        this.question = question;
//    }
//
//    public void setQuestionnaire(QuestionnaireEntity questionnaire) {
//        this.questionnaire = questionnaire;
//    }

    public void setLink(Integer link) {
        this.link = link;
    }

    public void setExclusions(String exclusions) {
        this.exclusions = exclusions;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getSliderNo() {
        return sliderNo;
    }

    public void setSliderNo(Integer sliderNo) {
        this.sliderNo = sliderNo;
    }

//    public QuestionTemplate getQuestionTemplate() {
//        return questionTemplate;
//    }
//
//    public void setQuestionTemplate(QuestionTemplate questionTemplate) {
//        this.questionTemplate = questionTemplate;
//    }
}
