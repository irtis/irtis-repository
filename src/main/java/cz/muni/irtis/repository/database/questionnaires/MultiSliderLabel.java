package cz.muni.irtis.repository.database.questionnaires;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "multi_slider_labels")
public class MultiSliderLabel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String text;
    private int sliderNo;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JsonIgnore
//    private QuestionTemplate question;

    public Long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getSliderNo() {
        return sliderNo;
    }

    public void setSliderNo(int sliderNo) {
        this.sliderNo = sliderNo;
    }

//    public QuestionTemplate getQuestion() {
//        return question;
//    }
//
//    public void setQuestion(QuestionTemplate question) {
//        this.question = question;
//    }
}
