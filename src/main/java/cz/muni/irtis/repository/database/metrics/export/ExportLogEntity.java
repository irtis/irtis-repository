package cz.muni.irtis.repository.database.metrics.export;

import javax.persistence.*;

/**
 * Hold last export date
 */
@Entity
@Table(name = "export_log")
public class ExportLogEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "study_name", nullable = false)
    private String studyName;

    private Long activities;
    private Long battery;
    private Long headphones;
    private Long locations;
    private Long playback;
    private Long runtime;
    private Long screen;
    private Long screen_taps;
    private Long screenshots;
    private Long steps;
    private Long applications_foreground;
    private Long applications_background;
    private Long wifis_connected;
    private Long questionnaires;

    public ExportLogEntity() {
    }

    public ExportLogEntity(String studyName) {
        this.studyName = studyName;
    }

    public Long getId() {
        return id;
    }

    public String getStudyName() {
        return studyName;
    }

    public void setStudyName(String studyName) {
        this.studyName = studyName;
    }

    public Long getActivities() {
        return activities;
    }

    public void setActivities(Long activities) {
        this.activities = activities;
    }

    public Long getBattery() {
        return battery;
    }

    public void setBattery(Long battery) {
        this.battery = battery;
    }

    public Long getHeadphones() {
        return headphones;
    }

    public void setHeadphones(Long headphones) {
        this.headphones = headphones;
    }

    public Long getLocations() {
        return locations;
    }

    public void setLocations(Long locations) {
        this.locations = locations;
    }

    public Long getPlayback() {
        return playback;
    }

    public void setPlayback(Long playback) {
        this.playback = playback;
    }

    public Long getRuntime() {
        return runtime;
    }

    public void setRuntime(Long runtime) {
        this.runtime = runtime;
    }

    public Long getScreen() {
        return screen;
    }

    public void setScreen(Long screen) {
        this.screen = screen;
    }

    public Long getScreen_taps() {
        return screen_taps;
    }

    public void setScreen_taps(Long screen_taps) {
        this.screen_taps = screen_taps;
    }

    public Long getScreenshots() {
        return screenshots;
    }

    public void setScreenshots(Long screenshots) {
        this.screenshots = screenshots;
    }

    public Long getSteps() {
        return steps;
    }

    public void setSteps(Long steps) {
        this.steps = steps;
    }

    public Long getApplications_foreground() {
        return applications_foreground;
    }

    public void setApplications_foreground(Long applications_foreground) {
        this.applications_foreground = applications_foreground;
    }

    public Long getApplications_background() {
        return applications_background;
    }

    public void setApplications_background(Long applications_background) {
        this.applications_background = applications_background;
    }

    public Long getWifis_connected() {
        return wifis_connected;
    }

    public void setWifis_connected(Long wifis_connected) {
        this.wifis_connected = wifis_connected;
    }

    public Long getQuestionnaires() {
        return questionnaires;
    }

    public void setQuestionnaires(Long questionnaires) {
        this.questionnaires = questionnaires;
    }
}
