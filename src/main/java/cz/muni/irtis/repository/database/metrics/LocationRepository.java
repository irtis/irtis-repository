package cz.muni.irtis.repository.database.metrics;

import cz.muni.irtis.repository.database.metrics.export.LocationExport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationRepository extends JpaRepository<LocationEntity, MetricIdentityEntity> {
    @Query("SELECT new cz.muni.irtis.repository.database.metrics.export.LocationExport(t.time, e.metricIdentity.personId, e.latitude, e.longitude) " +
            "FROM LocationEntity e " +
            "JOIN TimeEntity t ON t.id = e.metricIdentity.datetimeId " +
            "WHERE person_id = ?1 " +
            "AND t.time >= ?2 AND t.time <= ?3 " +
            "ORDER BY t.time ASC")
    List<LocationExport> findAllByPersonAndInterval(Long person_id, Long tMin, Long tMax);
}
