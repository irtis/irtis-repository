package cz.muni.irtis.repository.database.metrics;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DatetimeRepository extends JpaRepository<TimeEntity, Long> {

    Optional<TimeEntity> findTop1ByTime(@Param("time") Long time);

    @Query("SELECT t From TimeEntity t WHERE t.time > 20190000000000000L")
    Page<TimeEntity> findAllByBroken(PageRequest r);
}
