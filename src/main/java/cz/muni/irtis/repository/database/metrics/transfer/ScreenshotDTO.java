package cz.muni.irtis.repository.database.metrics.transfer;


public class ScreenshotDTO {
    private Long time;

    public ScreenshotDTO(Long time) {
        this.time = time;
    }

    public Long getTime() {
        return time;
    }
}
