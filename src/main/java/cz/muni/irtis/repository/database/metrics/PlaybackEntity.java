package cz.muni.irtis.repository.database.metrics;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "metrics_playback")
public class PlaybackEntity {
    @EmbeddedId
    private MetricIdentityEntity metricIdentity;
    private Boolean active;


    public PlaybackEntity() {
    }

    public PlaybackEntity(MetricIdentityEntity metricIdentity, Boolean active) {
        this.metricIdentity = metricIdentity;
        this.active = active;
    }

    public void setMetricIdentity(MetricIdentityEntity metricIdentity) {
        this.metricIdentity = metricIdentity;
    }

    public MetricIdentityEntity getMetricIdentity() {
        return metricIdentity;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
