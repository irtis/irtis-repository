package cz.muni.irtis.repository.database.metrics.screen;

import cz.muni.irtis.repository.database.metrics.MetricIdentityEntity;
import cz.muni.irtis.repository.database.metrics.export.ScreenExport;
import cz.muni.irtis.repository.database.metrics.screen.ScreenEntity;
import cz.muni.irtis.repository.database.metrics.transfer.ScreenDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ScreensRepository extends JpaRepository<ScreenEntity, MetricIdentityEntity> {
    @Query("SELECT new cz.muni.irtis.repository.database.metrics.transfer.ScreenDTO(t.time, e.state) " +
            "FROM ScreenEntity e " +
            "JOIN TimeEntity t ON t.id = e.metricIdentity.datetimeId " +
            "WHERE person_id = ?1 AND e.state = true " +
            "AND t.time >= ?2 AND t.time <= ?3 " +
            "ORDER BY t.time ASC")
    List<ScreenDTO> findAllByPersonAndInterval(Long person_id, Long tMin, Long tMax);

    @Query("SELECT new cz.muni.irtis.repository.database.metrics.export.ScreenExport(t.time, e.metricIdentity.personId, e.state) " +
            "FROM ScreenEntity e " +
            "JOIN TimeEntity t ON t.id = e.metricIdentity.datetimeId " +
            "WHERE person_id = ?1 " +
            "AND t.time >= ?2 AND t.time <= ?3 " +
            "ORDER BY t.time ASC")
    List<ScreenExport> findAllByPersonAndIntervalExport(Long person_id, Long tMin, Long tMax);

    @Query(value = "select mt.time from metrics_time mt " +
            "join (select datetime_id from metrics_screen where person_id = :person_id) ms " +
            "on ms.datetime_id = mt.id " +
            "where mt.time > :beg and mt.time < :end order by mt.time desc limit 1", nativeQuery = true)
    Long findLastTimeByPid(Long person_id, Long beg, Long end);

    @Query(value = "select ms.id from metrics_time mt " +
            "join (select id, datetime_id from metrics_screen where person_id = :person_id ) ms " +
            "on ms.datetime_id = mt.id " +
            "where mt.time = :tm", nativeQuery = true)
    Optional<Long> findIdByTimeAndPid(Long person_id, Long tm);
}