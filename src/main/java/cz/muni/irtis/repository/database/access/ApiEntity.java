package cz.muni.irtis.repository.database.access;

import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.model.access.Api;

import javax.persistence.*;

@Entity
@Table(
    name = "apis",
    indexes = {
        @Index(name = "index_identification", columnList="identifier, key", unique = true)
    }
)
public class ApiEntity extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String identifier;
    private String key;
    private Boolean active;

    public ApiEntity() {
    }

    public ApiEntity(Long id, String name, String identifier, String key, Boolean active) {
        this.id = id;
        this.name = name;
        this.identifier = identifier;
        this.key = key;
        this.active = active;
    }

    static public ApiEntity build(Api item) {
        ApiEntity entity = new ApiEntity();
        entity.setId(item.getId());
        entity.setName(item.getName());
        entity.setIdentifier(item.getIdentifier());
        entity.setKey(item.getKey());
        entity.setActive(item.getActive());
        return entity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Boolean getActive() {
        return active;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}

