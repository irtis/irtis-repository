package cz.muni.irtis.repository.database.questionnaires;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "answer_links")
public class AnswerLink {
    @EmbeddedId
    private AnswerLinkPK id;

    @ManyToOne
    @MapsId("questionnaireId")
    @JoinColumn(name = "questionnaire_id", foreignKey = @ForeignKey(name = "fk_questionnaire_id"))
    @JsonBackReference
    private QuestionnaireEntity questionnaire;

    @ManyToOne
    @MapsId("targetQuestionId")
    @JoinColumn(name = "target_question_id", foreignKey = @ForeignKey(name = "fk_target_question_id"))
    private QuestionEntity targetQuestion;

    @ManyToOne
    @MapsId("ownerQuestionId")
    @JoinColumn(name = "owner_question_id", foreignKey = @ForeignKey(name = "fk_owner_question_id"))
    private QuestionEntity ownerQuestion;

    @ManyToOne
    @MapsId("answerId")
    @JoinColumn(name = "answer_id", foreignKey = @ForeignKey(name = "fk_answer_id"))
    private AnswerEntity answer;

    @Transient
    private String choice;
    @Transient
    private String selectString;

    public AnswerLink() {
    }

    public AnswerLink(AnswerLink answerLink, String choice) {
        this.id = answerLink.getId();
        this.questionnaire = answerLink.getQuestionnaire();
        this.targetQuestion = answerLink.getTargetQuestion();
        this.ownerQuestion = answerLink.getOwnerQuestion();
        this.answer = answerLink.getAnswer();
        this.choice = choice;
        this.selectString = ownerQuestion.getSelectString();
    }

    public AnswerLinkPK getId() {
        return id;
    }

    public void setId(AnswerLinkPK id) {
        this.id = id;
    }

    public QuestionnaireEntity getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(QuestionnaireEntity questionnaire) {
        this.questionnaire = questionnaire;
    }

    public QuestionEntity getTargetQuestion() {
        return targetQuestion;
    }

    public void setTargetQuestion(QuestionEntity targetQuestion) {
        this.targetQuestion = targetQuestion;
    }

    public QuestionEntity getOwnerQuestion() {
        return ownerQuestion;
    }

    public void setOwnerQuestion(QuestionEntity ownerQuestion) {
        this.ownerQuestion = ownerQuestion;
    }

    public AnswerEntity getAnswer() {
        return answer;
    }

    public void setAnswer(AnswerEntity answer) {
        this.answer = answer;
    }

    public String getChoice() {
        return choice;
    }

    public void setChoice(String choice) {
        this.choice = choice;
    }

    public String getSelectString() {
        return selectString;
    }

    public void setSelectString(String selectString) {
        this.selectString = selectString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnswerLink that = (AnswerLink) o;
        if (questionnaire == null ||
            targetQuestion == null ||
            ownerQuestion == null ||
            answer == null ||
            that.questionnaire == null ||
            that.targetQuestion == null ||
            that.ownerQuestion == null ||
            that.answer == null) return false;
        return Objects.equals(questionnaire.getId(), that.questionnaire.getId()) &&
                Objects.equals(targetQuestion.getId(), that.targetQuestion.getId()) &&
                Objects.equals(ownerQuestion.getId(), that.ownerQuestion.getId()) &&
                Objects.equals(answer.getId(), that.answer.getId());
    }

    @Override
    public int hashCode() {
        if (questionnaire == null ||
            targetQuestion == null ||
            ownerQuestion == null ||
            answer == null) return 0;
        return Objects.hash(questionnaire.getId(), targetQuestion.getId(), ownerQuestion.getId(), answer.getId());
    }
}
