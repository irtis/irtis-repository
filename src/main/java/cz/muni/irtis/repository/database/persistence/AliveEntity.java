package cz.muni.irtis.repository.database.persistence;

import cz.muni.irtis.repository.database.identity.PersonEntity;
import javax.persistence.*;

@Entity
@Table(name = "alive")
public class AliveEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "person_id", nullable = false)
    private PersonEntity person;
    private Boolean application;
    private Boolean capture;
    private Boolean screenshots;
    private Long time;


    public AliveEntity() {
    }

    public AliveEntity(PersonEntity person, Boolean application, Boolean capture, Boolean screenshots, Long time) {
        this.person = person;
        this.application = application;
        this.capture = capture;
        this.screenshots = screenshots;
        this.time = time;
    }

    public Long getId() {
        return id;
    }

    public PersonEntity getPerson() {
        return person;
    }

    public Boolean getApplication() {
        return application;
    }

    public Boolean getCapture() {
        return capture;
    }

    public Boolean getScreenshots() {
        return screenshots;
    }

    public void setScreenshots(Boolean screenshot) {
        this.screenshots = screenshot;
    }

    public Long getTime() {
        return time;
    }
}
