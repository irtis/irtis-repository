package cz.muni.irtis.repository.database.metrics;

import cz.muni.irtis.repository.database.metrics.export.NotificationExport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationRepository extends JpaRepository<NotificationEntity, MetricIdentityEntity> {
    @Query("SELECT new cz.muni.irtis.repository.database.metrics.export.NotificationExport(t.time, e.metricIdentity.personId, e.identifier, e.posted, e.removed, e.text, e.title) " +
            "FROM NotificationEntity e " +
            "JOIN TimeEntity t ON t.id = e.metricIdentity.datetimeId " +
            "WHERE person_id = ?1 " +
            "AND t.time >= ?2 AND t.time <= ?3 " +
            "ORDER BY t.time ASC")
    List<NotificationExport> findAllByPersonAndInterval(Long person_id, Long tMin, Long tMax);
}
