package cz.muni.irtis.repository.database.identity;

import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.model.persons.Person;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(
    name = "people",
    indexes = {
        @Index(name = "index_token", columnList="token", unique = true),
        @Index(name = "index_login", columnList="id, password", unique = true)
    }
)
public class PersonEntity extends EntityBase {
    @Id
    @GeneratedValue(generator = "person_generator")
    @SequenceGenerator(
        name = "person_generator",
        sequenceName = "person_sequence",
        initialValue = 1000
    )
    private Long id;
    // TODO: rewrite to Many-to-many
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "group_id", nullable = true)
    private PersonGroupEntity group;

    @ManyToOne
    private PersonEntity manager;
    @OneToMany(mappedBy="manager")
    private Set<PersonEntity> respondents;

    private String password;
    private String name;
    private String email;
    private String phone;
    private String token;
    private String device;
    private String api;

    public PersonEntity() {
    }

    public PersonEntity(String name, String email, String phone, String device) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.device = device;
    }

    static public PersonEntity build(Person item) {
        PersonEntity entity = new PersonEntity();
        entity.setId(item.getId());
        if(item.getGroup()!=null) {
            entity.setGroup(PersonGroupEntity.build(item.getGroup()));
        }
        entity.setPassword(item.getPassword());
        entity.setName(item.getName());
        entity.setEmail(item.getEmail());
        entity.setPhone(item.getPhone());
        entity.setToken(item.getToken());
        entity.setDevice(item.getDevice());
        entity.setApi(item.getApi());
        return entity;
    }

    static public PersonEntity build(Long id) {
        PersonEntity entity = new PersonEntity();
        entity.setId(id);
        return entity;
    }

    static public PersonEntity build(String name) {
        PersonEntity entity = new PersonEntity();
        entity.setName(name);
        return entity;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonGroupEntity getGroup() {
        return group;
    }

    public void setGroup(PersonGroupEntity group) {
        this.group = group;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }


    public PersonEntity getManager() {
        return manager;
    }

    public void setManager(PersonEntity manager) {
        this.manager = manager;
    }

    public Set<PersonEntity> getRespondents() {
        return respondents;
    }

    public void setRespondents(Set<PersonEntity> respondents) {
        this.respondents = respondents;
    }

    public String toJson() {
        return "{" +
            "id: " + (getId()!=null ? getId().toString() : "null") + ", " +
            "device: " + (getDevice()!=null ? getDevice() : "null") + ", " +
            "api: " + (getApi()!=null ? getApi() : "null") + ", " +
            "name: " + (getName()!=null ? getName() : "null") + ", " +
            "password: " + (getPassword()!=null ? getPassword() : "null") + ", " +
            "email: " + (getEmail()!=null ? getEmail() : "null") + ", " +
//            "phone: " + (getPhone()!=null ? getPhone() : "null") + ", " +
            "token: " + (getToken()!=null ? getToken() : "null") + ", " +
        "}";
    }
}
