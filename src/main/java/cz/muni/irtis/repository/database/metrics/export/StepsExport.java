package cz.muni.irtis.repository.database.metrics.export;

public class StepsExport extends MetricExport {
    // "taken_on","datetime_id","steps"
    // time;personId;steps

    private Integer steps;

    public StepsExport(Long time, Long personId, Integer steps) {
        this.steps = steps;
        super.setTime(time);
        super.setPersonId(personId);
    }

    public Integer getSteps() {
        return steps;
    }

    public void setSteps(Integer steps) {
        this.steps = steps;
    }
}
