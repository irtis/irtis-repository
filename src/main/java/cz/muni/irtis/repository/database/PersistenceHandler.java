package cz.muni.irtis.repository.database;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Entity;
import javax.tools.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class PersistenceHandler {
    private static StandardServiceRegistry registry;
    private static SessionFactory factory;

    static public PersistenceHandler build() {
        PersistenceHandler instance = new PersistenceHandler();
        if(factory == null) {
            instance.load();
        }
        return instance;
    }

    private SessionFactory load() {
        if (factory == null) {
            try {
                Properties config = PersistenceConfigLoader.build()
                        .getConfig();

                Configuration configuration = new Configuration();
//                configuration.configure(new File("persistence.xml"));
                Properties properties = configuration.getProperties();

                StandardServiceRegistryBuilder registryBuilder =
                        new StandardServiceRegistryBuilder();

                properties.put("hibernate.dialect", config.get("hibernate.dialect"));
                properties.put("javax.persistence.jdbc.driver", config.get("javax.persistence.jdbc.driver"));
                properties.put("hibernate.show_sql", config.get("hibernate.show_sql"));
                properties.put("hibernate.hbm2ddl.auto", config.get("hibernate.hbm2ddl.auto"));

                properties.put("hibernate.connection.url", config.get("javax.persistence.jdbc.url"));
                properties.put("hibernate.connection.username", config.get("javax.persistence.jdbc.user"));
                properties.put("hibernate.connection.password", config.get("javax.persistence.jdbc.password"));

                registryBuilder.applySettings(properties);
                registry = registryBuilder.build();
                MetadataSources sources = new MetadataSources(registry);

                getEntities("cz.muni.irtis.repository.database")
                        .stream()
                        .forEach(sources::addAnnotatedClass);

                factory = sources.buildMetadata().buildSessionFactory();
            } catch (Exception e) {
                System.out.println(this.getClass().getName() + ": Session factory creation failed: " + e.getMessage());
                if (registry != null) {
                    StandardServiceRegistryBuilder.destroy(registry);
                }
            }
        }
        return factory;
    }

    public Session getSession() throws HibernateException {
        Session session = null;
        try {
            session = factory.openSession();
        } catch(Throwable t){
            System.err.println("Exception while getting session.. ");
            t.printStackTrace();
        }
        if(session == null) {
            System.err.println("session is discovered null");
        }

        return session;
    }

    public void shutdown() {
        if (registry != null) {
            StandardServiceRegistryBuilder.destroy(registry);
        }
    }

    private Collection<Class> getEntities(final String pack) {
        final StandardJavaFileManager fileManager = ToolProvider.getSystemJavaCompiler().getStandardFileManager(null, null, null);
        try {
            return StreamSupport.stream(fileManager.list(StandardLocation.CLASS_PATH, pack, Collections.singleton(JavaFileObject.Kind.CLASS), true).spliterator(), false)
                    .map(FileObject::getName)
                    .map(name -> {
                        try {
                            String[] parts = !name.contains(".jar") ?
                                name
                                    .replace(".class", "")
                                    .replace(")", "")
                                    .split(Pattern.quote(File.separator)) :
                                name
                                    .substring(name.indexOf(".jar")+4)
                                    .replace(".class", "")
                                    .replace("(", "")
                                    .replace(")", "")
                                    .split(Pattern.quote("/"));

                            String className = parts[parts.length - 1];

                            String subpack = !name.contains(".jar") ?
                                name
                                    .substring(name.replace(File.separator, ".").indexOf(pack) + pack.length(), name.indexOf(className))
                                    .replaceAll("^"+Pattern.quote(File.separator), "")
                                    .replaceAll(Pattern.quote(File.separator)+"$", "")
                                    .replace(File.separator, ".") :
                                name
                                    .substring(name.indexOf(".jar")+4)
                                    .substring(name.substring(name.indexOf(".jar")+4).replace("/", ".").indexOf(pack) + pack.length())
                                    .replace(className, "")
                                    .replace(".class", "")
                                    .replace("(", "")
                                    .replace(")", "")
                                    .replaceAll("^"+Pattern.quote("/"), "")
                                    .replaceAll(Pattern.quote("/")+"$", "")
                                    .replace("/", ".");

                            String fullClassName = pack + "." + (!subpack.isEmpty() ? subpack + "." : "") + className;

                            return Class.forName(fullClassName);
                        } catch (ClassNotFoundException e) {
                            throw new RuntimeException(e);
                        }

                    })
                    .filter(aClass -> aClass.isAnnotationPresent(Entity.class))
                    .collect(Collectors.toCollection(ArrayList::new));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
