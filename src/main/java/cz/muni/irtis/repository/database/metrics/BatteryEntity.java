package cz.muni.irtis.repository.database.metrics;


import javax.persistence.*;

@Entity
@Table(name = "metrics_battery")
public class BatteryEntity {
    @EmbeddedId
    private MetricIdentityEntity metricIdentity;
    @Column(name = "state_percent")
    private int statePercent;


    public BatteryEntity() {
    }

    public BatteryEntity(MetricIdentityEntity metricIdentity, int statePercent) {
        this.metricIdentity = metricIdentity;
        this.statePercent = statePercent;
    }

    public int getStatePercent() {
        return statePercent;
    }

    public void setStatePercent(int statePercent) {
        this.statePercent = statePercent;
    }

    public MetricIdentityEntity getMetricIdentity() {
        return metricIdentity;
    }

    public void setMetricIdentity(MetricIdentityEntity metricIdentity) {
        this.metricIdentity = metricIdentity;
    }
}
