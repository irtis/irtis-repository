package cz.muni.irtis.repository.database.metrics.application;


import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.database.metrics.MetricIdentityEntity;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "metrics_applications_background")
public class ApplicationBackgroundEntity extends EntityBase {
    @EmbeddedId
    private MetricIdentityEntity metricIdentity;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "application_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private ApplicationEntity application;
    @Column(name="cached_time")
    private Long cachedTime;

    public ApplicationBackgroundEntity() {
    }

    public ApplicationBackgroundEntity(MetricIdentityEntity metricIdentity, ApplicationEntity appName) {
        this.metricIdentity = metricIdentity;
        this.application = appName;
    }

    public MetricIdentityEntity getMetricIdentity() {
        return metricIdentity;
    }

    public void setMetricIdentity(MetricIdentityEntity metricIdentity) {
        this.metricIdentity = metricIdentity;
    }

    public ApplicationEntity getApplication() {
        return application;
    }

    public void setApplication(ApplicationEntity application) {
        this.application = application;
    }

    public Long getCachedTime() {
        return cachedTime;
    }

    public void setCachedTime(Long cachedTime) {
        this.cachedTime = cachedTime;
    }

    @Override
    public Long getId() {
        return application.getId();
    }
}
