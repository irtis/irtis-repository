package cz.muni.irtis.repository.database.metrics;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "metrics_steps")
public class StepEntity {
    @EmbeddedId
    private MetricIdentityEntity metricIdentity;
    private Integer steps;


    public StepEntity() {
    }

    public StepEntity(MetricIdentityEntity metricIdentity, Integer steps) {
        this.metricIdentity = metricIdentity;
        this.steps = steps;
    }

    public void setMetricIdentity(MetricIdentityEntity metricIdentity) {
        this.metricIdentity = metricIdentity;
    }

    public MetricIdentityEntity getMetricIdentity() {
        return metricIdentity;
    }

    public Integer getSteps() {
        return steps;
    }

    public void setSteps(Integer active) {
        this.steps = active;
    }
}
