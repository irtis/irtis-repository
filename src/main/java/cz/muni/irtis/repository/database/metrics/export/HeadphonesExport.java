package cz.muni.irtis.repository.database.metrics.export;

public class HeadphonesExport extends MetricExport {
    // "taken_on","datetime_id","person_id","connected"
    // time;personId;connected

    private Boolean connected;

    public HeadphonesExport(Long time, Long personId, Boolean connected) {
        this.connected = connected;
        super.setTime(time);
        super.setPersonId(personId);
    }

    public Boolean getConnected() {
        return connected;
    }

    public void setConnected(Boolean connected) {
        this.connected = connected;
    }
}
