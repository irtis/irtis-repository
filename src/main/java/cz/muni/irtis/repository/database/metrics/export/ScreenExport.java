package cz.muni.irtis.repository.database.metrics.export;

public class ScreenExport extends MetricExport {
    // "taken_on","datetime_id","person_id","state"
    // time;personId;state

    private Boolean state;

    public ScreenExport(Long time, Long personId, Boolean state) {
        this.state = state;
        super.setTime(time);
        super.setPersonId(personId);
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }
}
