package cz.muni.irtis.repository.database.metrics.screen;


import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.database.metrics.MetricIdentityEntity;

import javax.persistence.*;

@Entity
@Table(name = "metrics_screenshots")
public class ScreenshotEntity extends EntityBase {
    public static final int METADATA_SIZE = 1024*10;

    @EmbeddedId
    private MetricIdentityEntity metricIdentity;
    @Column(name="application_id")
    private Long applicationId;
    private String filename;
    @Column(name="metadata", length=METADATA_SIZE)
    private String metadata;
    @Column(name="is_dark_mode")
    private Boolean isDarkMode;
    @Column(name="is_keyboard")
    private Boolean isKeyboard;
    @Column(name="is_processed")
    private Boolean isProcessed;
    @Column(name="cached_time")
    private Long cachedTime;


    public ScreenshotEntity() {
    }

    public ScreenshotEntity(MetricIdentityEntity metricIdentity, String filename, String metadata) {
        this.metricIdentity = metricIdentity;
        this.filename = filename;
        this.metadata = metadata;
    }

    public ScreenshotEntity(MetricIdentityEntity metricIdentity, String filename) {
        this.metricIdentity = metricIdentity;
        this.filename = filename;
        this.metadata = null;
    }

    @Override
    public Long getId() {
        return null;
    }

    public MetricIdentityEntity getMetricIdentity() {
        return metricIdentity;
    }

    public void setMetricIdentity(MetricIdentityEntity metricIdentity) {
        this.metricIdentity = metricIdentity;
    }

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        if (metadata != null && metadata.length() >= METADATA_SIZE) {
            this.metadata = metadata.substring(0, METADATA_SIZE);
        } else {
            this.metadata = metadata;
        }
    }

    public Boolean getDarkMode() {
        return isDarkMode;
    }

    public void setDarkMode(Boolean darkMode) {
        isDarkMode = darkMode;
    }

    public Boolean getKeyboard() {
        return isKeyboard;
    }

    public void setKeyboard(Boolean keyboard) {
        isKeyboard = keyboard;
    }

    public Boolean getProcessed() {
        return isProcessed;
    }

    public void setProcessed(Boolean processed) {
        isProcessed = processed;
    }

    public Long getCachedTime() {
        return cachedTime;
    }

    public void setCachedTime(Long cachedTime) {
        this.cachedTime = cachedTime;
    }
}
