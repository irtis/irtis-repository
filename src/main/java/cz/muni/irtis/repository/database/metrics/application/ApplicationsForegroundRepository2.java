package cz.muni.irtis.repository.database.metrics.application;

import cz.muni.irtis.repository.database.metrics.MetricIdentityEntity;
import cz.muni.irtis.repository.database.metrics.export.ApplicationForegroundExport2;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ApplicationsForegroundRepository2 extends JpaRepository<ApplicationForegroundEntity2, MetricIdentityEntity> {
    @Query(
            "select new cz.muni.irtis.repository.database.metrics.export.ApplicationForegroundExport2(t.time, maf.metricIdentity.personId, ma.name, ma.id) " +
                    "from ApplicationForegroundEntity2 maf " +
                    "join ApplicationEntity ma on ma.id = maf.application.id " +
                    "join TimeEntity t on t.id = maf.metricIdentity.datetimeId " +
                    "where person_id = ?1 " +
                    "and t.time >= ?2 and t.time <= ?3 " +
                    "order by t.time ASC")
    List<ApplicationForegroundExport2> findAllByPersonAndInterval(Long personId, Long tMin, Long tMax);
}
