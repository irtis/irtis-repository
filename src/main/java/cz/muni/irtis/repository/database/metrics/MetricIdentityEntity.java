package cz.muni.irtis.repository.database.metrics;


import org.springframework.lang.NonNull;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class MetricIdentityEntity implements Serializable {
    @NonNull
    @Column(name = "person_id")
    private Long personId;
    @NonNull
    @Column(name = "datetime_id")
    private Long datetimeId;


    public MetricIdentityEntity() {
    }

    public MetricIdentityEntity(Long personId, Long datetimeId) {
        this.personId = personId;
        this.datetimeId = datetimeId;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public Long getDatetimeId() {
        return datetimeId;
    }

    public void setDatetimeId(Long datetimeId) {
        this.datetimeId = datetimeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MetricIdentityEntity that = (MetricIdentityEntity) o;

        if (!personId.equals(that.personId)) return false;
        return datetimeId.equals(that.datetimeId);
    }

    @Override
    public int hashCode() {
        int result = personId.hashCode();
        result = 31 * result + datetimeId.hashCode();
        return result;
    }
}
