package cz.muni.irtis.repository.database.notes;

import cz.muni.irtis.repository.database.identity.BurstEntity;

import javax.persistence.*;

@Entity
@Table(name = "people_bursts_notes")
public class PersonBurstNoteEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "person_burst_id", nullable = false)
    private BurstEntity personBurst;

    @Column(name="content", length=1024*8)
    private String content;

    public PersonBurstNoteEntity() {
    }

    public PersonBurstNoteEntity(BurstEntity pb, String content) {
        this.personBurst = pb;
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public BurstEntity getPersonBurst() {
        return personBurst;
    }

    public void setPersonBurst(BurstEntity personBurst) {
        this.personBurst = personBurst;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

