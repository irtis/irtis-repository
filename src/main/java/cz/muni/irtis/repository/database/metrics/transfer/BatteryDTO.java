package cz.muni.irtis.repository.database.metrics.transfer;



import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


public class BatteryDTO {
    private Long time;
    private Integer percent;

    public BatteryDTO(Long time, Integer percent) {
        this.time = time;
        this.percent = percent;
    }

    public Long getTime() {
        return time;
    }

    public Integer getPercent() {
        return percent;
    }
}
