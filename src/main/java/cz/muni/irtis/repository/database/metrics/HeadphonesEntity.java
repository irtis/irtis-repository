package cz.muni.irtis.repository.database.metrics;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "metrics_headphones")
public class HeadphonesEntity {
    @EmbeddedId
    private MetricIdentityEntity metricIdentity;
    private Boolean connected;


    public HeadphonesEntity() {
    }

    public HeadphonesEntity(MetricIdentityEntity metricIdentity, Boolean connected) {
        this.metricIdentity = metricIdentity;
        this.connected = connected;
    }

    public void setMetricIdentity(MetricIdentityEntity metricIdentity) {
        this.metricIdentity = metricIdentity;
    }

    public MetricIdentityEntity getMetricIdentity() {
        return metricIdentity;
    }

    public Boolean getConnected() {
        return connected;
    }

    public void setConnected(Boolean connected) {
        this.connected = connected;
    }
}
