package cz.muni.irtis.repository.database.identity;

import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.model.persons.Person;
import cz.muni.irtis.repository.model.persons.PersonConversation;

import javax.persistence.*;

@Entity
@Table(
    name = "people_conversations",
    indexes = {
        @Index(name = "index_person", columnList="person_id", unique = false),
        @Index(name = "index_time", columnList="time", unique = false),
        @Index(name = "index_message", columnList="person_id, owner, message", unique = false)
    }
)
public class PersonConversationEntity extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "person_id", nullable = false)
    private PersonEntity person;
    private Long time;
    private String title;
    private String owner;
    @Column(name="message", length=1024)
    private String message;
    private String file;
    private Float execution;
    private Float confidence;


    public PersonConversationEntity() {
    }

    public PersonConversationEntity(PersonEntity person, Long time, String title, String owner, String message, String file, Float execution, Float confidence) {
        this.person = person;
        this.time = time;
        this.title = title;
        this.owner = owner;
        this.message = message;
        this.file = file;
        this.execution = execution;
        this.confidence = confidence;
    }

    public static PersonConversationEntity build(PersonConversation conversation) {
        PersonConversationEntity entity = new PersonConversationEntity(
            PersonEntity.build(conversation.getPerson()),
            conversation.getTime(),
            conversation.getTitle(),
            conversation.getOwner(),
            conversation.getMessage(),
            conversation.getFile(),
            conversation.getExecution(),
            conversation.getConfidence()
        );

        entity.setId(conversation.getId());

        return entity;
    }

    static public PersonConversationEntity build(Long id) {
        PersonConversationEntity entity = new PersonConversationEntity();
        entity.setId(id);
        return entity;
    }

    static public PersonConversationEntity build(String title) {
        PersonConversationEntity entity = new PersonConversationEntity();
        entity.setTitle(title);
        return entity;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonEntity getPerson() {
        return person;
    }

    public void setPerson(PersonEntity person) {
        this.person = person;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public Float getExecution() {
        return execution;
    }

    public void setExecution(Float execution) {
        this.execution = execution;
    }

    public Float getConfidence() {
        return confidence;
    }

    public void setConfidence(Float confidence) {
        this.confidence = confidence;
    }
}
