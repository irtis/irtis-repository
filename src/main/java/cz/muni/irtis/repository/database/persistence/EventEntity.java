package cz.muni.irtis.repository.database.persistence;

import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.database.identity.PersonEntity;
import cz.muni.irtis.repository.model.utils.Time;

import javax.persistence.*;

@Entity
@Table(name = "events")
public class EventEntity extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "person_id", nullable = false)
    private PersonEntity person;
    private Integer identifier;
    private Long time;

    public EventEntity() {
    }

    public EventEntity(PersonEntity person, Integer identifier, Long time) {
        this.person = person;
        this.identifier = identifier;
        this.time = time;
    }

    public Long getId() {
        return id;
    }

    public PersonEntity getPerson() {
        return person;
    }

    public Integer getIdentifier() {
        return identifier;
    }

    public Long getTime() {
        return time;
    }

    public String getTimeString() {
        return Time.getTimeFormatted(time);
    }

    public String getIdentifierString() {
        String tryGetValue = EventType.getType(identifier);
        if (tryGetValue == null || "".equals(tryGetValue)) {
            tryGetValue = Integer.toString(identifier);
        }
        return tryGetValue;
    }
}
