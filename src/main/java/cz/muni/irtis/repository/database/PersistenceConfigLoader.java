package cz.muni.irtis.repository.database;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * PersistenceConfigLoader
 *
 */
public class PersistenceConfigLoader {

    static public PersistenceConfigLoader build() {
        return new PersistenceConfigLoader();
    }

    public Properties getConfig() {
        Properties config = getBase();
        boolean isLocalLoaded = false;
        if(isLocal()) {
            Properties result = getLocal();
            if(result!=null) {
                config.putAll(result);
                isLocalLoaded = true;
            }
        }
        if(!isLocalLoaded && isServer()) {
            config.putAll(getServer());
        }

        return config;
    }

    private Properties getBase() {
        Properties config = new Properties();
        try {
            File file = new File("persistence.xml");

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dbFactory.newDocumentBuilder();
            Document document = builder.parse(file);
            document.getDocumentElement().normalize(); // See this http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
//            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

            NodeList ePropertiesItems = document.getElementsByTagName("properties");
            for (int iPropertiesItems = 0; iPropertiesItems < ePropertiesItems.getLength(); iPropertiesItems++) {
                Node nNode = ePropertiesItems.item(iPropertiesItems);
//                System.out.println("\nCurrent Element :" + nNode.getNodeName());
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element ePropertiesElement = (Element) nNode;
                    NodeList ePropertyItems = ePropertiesElement.getElementsByTagName("property");
                    for (int iPropertyItems = 0; iPropertyItems < ePropertyItems.getLength(); iPropertyItems++) {
                        Element ePropertyElement = (Element) ePropertyItems.item(iPropertyItems);
                        if (ePropertyElement.getNodeType() == Node.ELEMENT_NODE) {
                            String name = ePropertyElement.getAttribute("name");
                            String value = ePropertyElement.getAttribute("value");
                            config.put(name, value);
//                            System.out.println("Name: "+name+"; Value: "+value);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return config;
    }

    private Properties getServer() {
        Properties config = new Properties();
        try {
            File file = new File("persistence.server.xml");

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dbFactory.newDocumentBuilder();
            Document document = builder.parse(file);
            document.getDocumentElement().normalize(); // See this http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work

            NodeList ePropertiesItems = document.getElementsByTagName("properties");
            for (int iPropertiesItems = 0; iPropertiesItems < ePropertiesItems.getLength(); iPropertiesItems++) {
                Node nNode = ePropertiesItems.item(iPropertiesItems);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element ePropertiesElement = (Element) nNode;
                    NodeList ePropertyItems = ePropertiesElement.getElementsByTagName("property");
                    for (int iPropertyItems = 0; iPropertyItems < ePropertyItems.getLength(); iPropertyItems++) {
                        Element ePropertyElement = (Element) ePropertyItems.item(iPropertyItems);
                        if (ePropertyElement.getNodeType() == Node.ELEMENT_NODE) {
                            String name = ePropertyElement.getAttribute("name");
                            String value = ePropertyElement.getAttribute("value");
                            config.put(name, value);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(config.get("enabled").equals("true")) {
            return config;
        } else {
            return null;
        }
    }

    private Properties getLocal() {
        Properties config = new Properties();
        try {
            File file = new File("persistence.local.xml");

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = dbFactory.newDocumentBuilder();
            Document document = builder.parse(file);
            document.getDocumentElement().normalize(); // See this http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work

            NodeList ePropertiesItems = document.getElementsByTagName("properties");
            for (int iPropertiesItems = 0; iPropertiesItems < ePropertiesItems.getLength(); iPropertiesItems++) {
                Node nNode = ePropertiesItems.item(iPropertiesItems);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element ePropertiesElement = (Element) nNode;
                    NodeList ePropertyItems = ePropertiesElement.getElementsByTagName("property");
                    for (int iPropertyItems = 0; iPropertyItems < ePropertyItems.getLength(); iPropertyItems++) {
                        Element ePropertyElement = (Element) ePropertyItems.item(iPropertyItems);
                        if (ePropertyElement.getNodeType() == Node.ELEMENT_NODE) {
                            String name = ePropertyElement.getAttribute("name");
                            String value = ePropertyElement.getAttribute("value");
                            config.put(name, value);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if(config.get("enabled").equals("true")) {
            return config;
        } else {
            return null;
        }
    }


    private boolean isLocal() {
        return new File("persistence.local.xml").exists();
    }

    private boolean isServer() {
        return new File("persistence.server.xml").exists();
    }
}
