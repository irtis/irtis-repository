package cz.muni.irtis.repository.database.metrics.export;

public class ScreenshotExport extends MetricExport {
    // "taken_on","datetime_id","person_id","filename","metadata"
    // time;personId;filename;metadata

    private String filename;
    private String metadata;

    public ScreenshotExport(Long time, Long personId, String filename, String metadata) {
        this.filename = filename;
        this.metadata = metadata;
        super.setTime(time);
        super.setPersonId(personId);
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }
}
