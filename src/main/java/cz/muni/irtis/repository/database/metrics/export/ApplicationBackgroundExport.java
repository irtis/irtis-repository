package cz.muni.irtis.repository.database.metrics.export;

public class ApplicationBackgroundExport extends MetricExport {
    //"taken_on","name","datetime_id","person_id","application_id"
    //time;personId;name;applicationId

    private String name;
    private Long applicationId;

    public ApplicationBackgroundExport(Long time, Long personId, String name, Long applicationId) {
        this.name = name;
        this.applicationId = applicationId;
        super.setTime(time);
        super.setPersonId(personId);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }
}
