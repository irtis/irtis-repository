package cz.muni.irtis.repository.database.questionnaires;

import cz.muni.irtis.repository.database.identity.PersonEntity;
import cz.muni.irtis.repository.model.questionnaires.AssignedQuestionnaire;
import cz.muni.irtis.repository.model.utils.Time;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

// TODO: delete. Change DB model to use a M:N mapping entity AssignedQuestionnaire.
@Repository
@Transactional
public class PersonsQuestionnairesRepositoryExtendedImpl implements PersonsQuestionnairesRepositoryExtended {
    @PersistenceContext
    private EntityManager entityManager;

    // TODO: need to set assignment_id column on old records (see script at the end)

    public List<AssignedQuestionnaire> findAllAssignments() {
        List<AssignedQuestionnaire> assignedQuestionnaires = entityManager
                .createQuery(
                        "select new cz.muni.irtis.repository.model.questionnaires.AssignedQuestionnaire(pq.assignmentId) " +
                                "from PersonQuestionnaireEntity pq " +
                                "where pq.assignmentId >= 1000 " +
                                "group by pq.assignmentId",
                        AssignedQuestionnaire.class)
                .getResultList();

        for (AssignedQuestionnaire aq : assignedQuestionnaires) {
            List<PersonQuestionnaireEntity> entityGroup = getAssignmentGroup(aq.getId());

            if (entityGroup.stream().anyMatch(x -> x.getDownloaded() != null)) {
                aq.setDownloaded(true);
            }
            aq.setPersonIds(
                    entityGroup.stream()
                            .map(e -> e.getPerson().getId())
                            .distinct()
                            .collect(Collectors.toList()));
            if (entityGroup.size() > 0) {
                aq.setQuestionnaire(entityGroup.get(0).getQuestionnaire());
            }
        }
        List<AssignedQuestionnaire> ret = assignedQuestionnaires.stream()
                .filter(x -> x.getQuestionnaire() != null)
                .collect(Collectors.toList());
        return ret;
    }

    public Optional<AssignedQuestionnaire> findByAssignmentId(Long assignmentId) {
        List<PersonQuestionnaireEntity> entityGroup = getAssignmentGroup(assignmentId);

        Optional<AssignedQuestionnaire> ret = Optional.empty();
        if (entityGroup.size() > 0) {
            PersonQuestionnaireEntity first = entityGroup.stream()
                    .min(Comparator.comparing(PersonQuestionnaireEntity::getStarting)).get();
            // TODO: update AssignedQuestionnaire constructor to all needed params
            AssignedQuestionnaire assignment = new AssignedQuestionnaire(
                    first.getAssignmentId(),
                    first.getQuestionnaire(),
                    first.getStarting(),
                    first.getEnding(),
                    first.getValidity(),
                    first.getBuffer(),
                    first.getInvoking());

            if (entityGroup.stream().anyMatch(x -> x.getDownloaded() != null)) {
                assignment.setDownloaded(true);
            }
            // Convert to UI strings
            assignment.setValidFrom(first.getValidFrom());
            assignment.setStartingTime(first.getStartingTime());
            assignment.setEndingTime(first.getEndingTime());
            assignment.setValidTo(first.getValidTo());
            assignment.setInvoked(first.getInvokedTime());
            assignment.setNotificationPeriod(first.getNotificationPeriod());
            assignment.setMaxNotifications(first.getMaxNotifications());
            assignment.setPersonIds(
                    entityGroup.stream()
                            .map(e -> e.getPerson().getId())
                            .distinct()
                            .collect(Collectors.toList()));
            assignment.setRepeatAfter(entityGroup.size() / assignment.getPersonIds().size() - 1);
            ret = Optional.of(assignment);
        }
        return ret;
    }

    /**
     * Creates mapping from the AssignedQuestionnaire model to PersonQuestionnaireEntities. An entity is created for
     * every person and every repeating from the model. All the created entities get a
     * generated assignment_id (in addition to its id).
     *
     * @param assignment The model with an empty assignmentId property.
     */
    public void createAssignment(AssignedQuestionnaire assignment) {
        Long newAssignmentId = Long.parseLong(entityManager
                .createNativeQuery("select nextval('assigned_questionnaire_group_id_seq')")
                .getSingleResult().toString());

        List<QuestionnaireInterval> intervals = getQuestionnaireIntervals(assignment);
        for (QuestionnaireInterval interval: intervals) {
            for (Long personId : assignment.getPersonIds()) {
                PersonQuestionnaireEntity entity = createPqeEntity(assignment, personId, interval);
                entity.setAssignmentId(newAssignmentId);
                entityManager.persist(entity);
            }
        }
    }

    // TODO: implement
    public void updateAssignment(AssignedQuestionnaire assignment) {
        List<QuestionnaireInterval> intervals = getQuestionnaireIntervals(assignment);
        List<PersonQuestionnaireEntity> assignmentGroup = getAssignmentGroup(assignment.getId());

        // check downloaded
        if (assignmentGroup.stream().anyMatch(x -> x.getDownloaded() != null)) {
            throw new IllegalStateException("Already downloaded");
        }

        deleteByAssignmentId(assignment.getId());
        for (QuestionnaireInterval interval: intervals) {
            for (Long personId : assignment.getPersonIds()) {
                PersonQuestionnaireEntity entity = createPqeEntity(assignment, personId, interval);
                entity.setAssignmentId(assignment.getId());
                entityManager.persist(entity);
            }
        }
    }

    public void deleteByAssignmentId(Long assignmentId) {
        entityManager
                .createQuery(
                        "delete from PersonQuestionnaireEntity pq " +
                                "where pq.assignmentId = :assignmentId")
                .setParameter("assignmentId", assignmentId)
                .executeUpdate();
    }

    private PersonQuestionnaireEntity createPqeEntity(AssignedQuestionnaire assignment, Long personId, QuestionnaireInterval interval) {
        PersonQuestionnaireEntity entity = new PersonQuestionnaireEntity();
        entity.setQuestionnaire(assignment.getQuestionnaire());
        entity.setPerson(entityManager.find(PersonEntity.class, personId));
        entity.setCreated(System.currentTimeMillis());
        entity.setAssignmentId(assignment.getId());
        entity.setStarting(interval.getStarting());
        entity.setEnding(interval.getEnding());
        entity.setClosing(interval.getClosing());
        entity.setNotificationPeriod(assignment.getNotificationPeriod());
        entity.setMaxNotifications(assignment.getMaxNotifications());
        entity.setValidity(assignment.getExpireInt());
        entity.setBuffer(assignment.getBufferInt());
        entity.setInvoking(interval.getInvoked());
        return entity;
    }

    private PersonQuestionnaireEntity updatePqeEntity(AssignedQuestionnaire assignment, PersonQuestionnaireEntity entity) {
        if (!assignment.getQuestionnaire().getId().equals(entity.getQuestionnaire().getId())) {
            entity.setQuestionnaire(assignment.getQuestionnaire());
        }
        entity.setUpdated(System.currentTimeMillis());
        entity.setStarting(assignment.getValidFrom(), assignment.getStartingTime());
        entity.setEnding(assignment.getValidTo(), assignment.getEndingTime());
        entity.setNotificationPeriod(assignment.getNotificationPeriod());
        entity.setMaxNotifications(assignment.getMaxNotifications());
        entity.setValidity(assignment.getExpireInt());
        entity.setBuffer(assignment.getBufferInt());
        return entity;
    }

    private List<PersonQuestionnaireEntity> getAssignmentGroup(Long assignmentId) {
        return entityManager
                .createQuery(
                        "select pq " +
                                "from PersonQuestionnaireEntity pq " +
                                "where pq.assignmentId = :assignmentId",
                        PersonQuestionnaireEntity.class)
                .setParameter("assignmentId", assignmentId)
                .getResultList();
    }

    private List<QuestionnaireInterval> getQuestionnaireIntervals(AssignedQuestionnaire ass) {
        Optional<Long> validTo = Time.getLocalMilis(ass.getValidTo(), ass.getStartingTime());
        if (!validTo.isPresent()) {
            throw new IllegalArgumentException(
                    "getLocalMilis failed: [" + ass.getValidTo() + "," + ass.getStartingTime()+ "," + ass.getRepeatAfter() + "]");
        }
        List<QuestionnaireInterval> intervals = new ArrayList<>();
        QuestionnaireInterval current = new QuestionnaireInterval(
                ass.getValidFrom(),
                ass.getValidTo(),
                ass.getStartingTime(),
                ass.getEndingTime(),
                ass.getExpireInt(),
                ass.getInvoked());

        if (ass.getRepeatAfter() == 0) {
            intervals.add(current);
        } else {
            for (int i = 0; i <= ass.getRepeatAfter(); i++) {
                intervals.add(current);
                current = current.addHours(24);
            }
        }
        return intervals;
    }

    private class QuestionnaireInterval {
        private long starting;
        private long ending;
        private long closing;
        private Long invoked;

        QuestionnaireInterval(String validFrom, String validTo, String startingTime, String endingTime, int expire, String invoked) {
            setStarting(validFrom, startingTime);
            setEnding(validTo, endingTime);
            if (invoked != null && invoked.length() > 0) {
                setInvoked(validFrom, invoked);
            }
//            closing = ending;
        }

        QuestionnaireInterval(long starting, long ending, long closing, Long invoked) {
            this.starting = starting;
            this.ending = ending;
            this.invoked = invoked;
//            this.closing = closing;
        }

            QuestionnaireInterval(long starting, long ending) {
            this.starting = starting;
            this.ending = ending;
        }

        void setStarting(String date, String time) {
            Time.getLocalMilis(date, time).ifPresent(this::setStarting);
        }

        void setEnding(String date, String time) {
            Time.getLocalMilis(date, time).ifPresent(this::setEnding);
        }

        /**
         * Add hours to starting, ending, and closing times.
         * @param hours
         * @return this
         */
        QuestionnaireInterval addHours(int hours) {
            long milis = Duration.ofHours(hours).toMillis();
            if (invoked == null) {
                return new QuestionnaireInterval(starting + milis, ending + milis);
            } else {
                return new QuestionnaireInterval(starting + milis, ending + milis, closing + milis, invoked + milis);
            }

        }

        long getStarting() {
            return starting;
        }

        long getEnding() {
            return ending;
        }

        long getClosing() {
            return closing;
        }

        private void setStarting(long starting) {
            this.starting = starting;
        }

        private void setEnding(long ending) {
            this.ending = ending;
        }

        public Long getInvoked() {
            return invoked;
        }

        public void setInvoked(Long invoked) {
            this.invoked = invoked;
        }

        void setInvoked(String date, String time) {
            Time.getLocalMilis(date, time).ifPresent(this::setInvoked);
        }
    }

    // // TODO: add grouping by person, questionnaire, starting
    //        long counter = 1L;
    //            for (PersonQuestionnaireEntity pq : entityGroup) {
//                pq.setAssignmentId(counter);
//                entityManager.merge(pq);
//            }
//            counter++;
}
