package cz.muni.irtis.repository.database.metrics.application;

import cz.muni.irtis.repository.database.identity.PersonEntity;
import cz.muni.irtis.repository.database.metrics.MetricIdentityEntity;
import cz.muni.irtis.repository.database.metrics.export.ApplicationForegroundExport;
import cz.muni.irtis.repository.database.metrics.transfer.ApplicationCountDTO;
import cz.muni.irtis.repository.database.metrics.transfer.ApplicationDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ApplicationsForegroundRepository extends JpaRepository<ApplicationForegroundEntity, MetricIdentityEntity> {
    @Query(
        "select new cz.muni.irtis.repository.database.metrics.transfer.ApplicationCountDTO(ma.name, count(maf.application.id)) " +
        "from ApplicationForegroundEntity maf " +
        "join ApplicationEntity ma on ma.id = maf.application.id " +
        "join TimeEntity t on t.id = maf.metricIdentity.datetimeId " +
        "where person_id = ?1 " +
        "and ma.id not in (1) " +
        "and t.time >= ?2 and t.time <= ?3 " +
        "group by ma.id, maf.application.id " +
        "order by count(maf.application.id) desc")
    List<ApplicationCountDTO> findAllOfUsageByPersonAndInterval(PersonEntity person, Long tMin, Long tMax);


    @Query(
        "select new cz.muni.irtis.repository.database.metrics.transfer.ApplicationCountDTO(ma.name, count(maf.application.id)) " +
        "from ApplicationForegroundEntity maf " +
        "join ApplicationEntity ma on ma.id = maf.application.id " +
        "where person_id = :person_id " +
        "and ma.id not in (1) " +
        "group by ma.id, maf.application.id " +
        "order by count(maf.application.id) desc")
    List<ApplicationCountDTO> findAllOfUsageByPerson(@Param("person_id") Long person_id);


    @Query(
        "select new cz.muni.irtis.repository.database.metrics.transfer.ApplicationDTO(t.time, ma.name) " +
        "from ApplicationForegroundEntity maf " +
        "join ApplicationEntity ma on ma.id = maf.application.id " +
        "join TimeEntity t on t.id = maf.metricIdentity.datetimeId " +
        "where person_id = :person_id " +
        "and ma.name = :name " +
        "group by ma.id, t.time " +
        "order by count(maf.application.id) desc")
    List<ApplicationDTO> findAllOfApplicationByPerson(@Param("person_id") Long person_id, @Param("name") String application);

    @Query(
            "select new cz.muni.irtis.repository.database.metrics.transfer.ApplicationDTO(t.time, ma.name) " +
            "from ApplicationForegroundEntity maf " +
            "join ApplicationEntity ma on ma.id = maf.application.id " +
            "join TimeEntity t on t.id = maf.metricIdentity.datetimeId " +
            "where person_id not in (1000, 1101, 1202, 1201, 1203) " +
            "and ma.name = :name " +
            "group by ma.id, t.time " +
            "order by count(maf.application.id) desc")
    List<ApplicationDTO> findAllOfApplicationByPerson(@Param("name") String application);

    @Query(
        "select new cz.muni.irtis.repository.database.metrics.transfer.ApplicationDTO(ma.id, count(maf.application.id), ma.name)" +
        "from ApplicationEntity ma " +
        "join ApplicationForegroundEntity maf on ma.id = maf.application.id " +
        "where person_id not in (1000, 1101, 1202, 1201, 1203) " +
        "group by maf.application.id, ma.id " +
        "order by count(maf.application.id) desc")
    List<ApplicationDTO> findAllOfApplication();

    @Query(
        "select new cz.muni.irtis.repository.database.metrics.transfer.ApplicationDTO(ma.id, count(maf.application.id), ma.name)" +
        "from ApplicationEntity ma " +
        "join ApplicationForegroundEntity maf on ma.id = maf.application.id " +
        "where person_id not in (1000, 1101, 1202, 1201, 1203) " +
        "and ma.name = :name " +
        "group by maf.application.id, ma.id " +
        "order by count(maf.application.id) desc")
    ApplicationDTO findOneOfApplicationByName(@Param("name") String name);

    @Query(
            "select new cz.muni.irtis.repository.database.metrics.export.ApplicationForegroundExport(t.time, maf.metricIdentity.personId, ma.name, ma.id) " +
                    "from ApplicationForegroundEntity maf " +
                    "join ApplicationEntity ma on ma.id = maf.application.id " +
                    "join TimeEntity t on t.id = maf.metricIdentity.datetimeId " +
                    "where person_id = ?1 " +
                    "and t.time >= ?2 and t.time <= ?3 " +
                    "order by t.time ASC")
    List<ApplicationForegroundExport> findAllByPersonAndInterval(Long personId, Long tMin, Long tMax);
}
