package cz.muni.irtis.repository.database.questionnaires;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

// TODO: update usages to use new DB model
@Repository
public interface QuestionsRepository extends JpaRepository<QuestionEntity, Long> {
    // TODO
    //Optional<List<QuestionEntity>> findByQuestionnaireOrderByPosition(@Param("questionnaire_id") QuestionnaireEntity questionnaire);

    @Transactional
    void deleteByQuestionnaire(@Param("questionnaire_id") QuestionnaireEntity questionnaire);

    List<QuestionEntity> findAllByQuestionTemplateTypeOrQuestionTemplateType(Integer type1, Integer type2);
}
