package cz.muni.irtis.repository.database.metrics.export;


public class ActivityExport extends MetricExport {
    private String activity;
    private Integer confidence;

    public ActivityExport(Long time, Long personId, String activity, Integer confidence) {
        this.activity = activity;
        this.confidence = confidence;
        super.setTime(time);
        super.setPersonId(personId);
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Integer getConfidence() {
        return confidence;
    }

    public void setConfidence(Integer confidence) {
        this.confidence = confidence;
    }
}
