package cz.muni.irtis.repository.database.metrics;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "metrics_notifications")
public class NotificationEntity {
    @EmbeddedId
    private MetricIdentityEntity metricIdentity;
    @Column(name = "package")
    private String identifier;
    @Column(name = "posted")
    private Long posted;
    @Column(name = "removed")
    private Long removed;
    @Column(name="title", length=1024)
    private String title;
    @Column(name="text", length=5120)
    private String text;


    public NotificationEntity() {
    }

    public NotificationEntity(MetricIdentityEntity metricIdentity, String identifier, Long posted, Long removed, String title, String text) {
        this.metricIdentity = metricIdentity;
        this.identifier = identifier;
        this.posted = posted;
        this.removed = removed;
        this.title = title;
        this.text = text;
    }

    public MetricIdentityEntity getMetricIdentity() {
        return metricIdentity;
    }

    public void setMetricIdentity(MetricIdentityEntity metricIdentity) {
        this.metricIdentity = metricIdentity;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String packagage) {
        this.identifier = packagage;
    }

    public Long getPosted() {
        return posted;
    }

    public void setPosted(Long posted) {
        this.posted = posted;
    }

    public Long getRemoved() {
        return removed;
    }

    public void setRemoved(Long removed) {
        this.removed = removed;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
