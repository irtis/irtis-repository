package cz.muni.irtis.repository.database.metrics.export;

public class LocationExport extends MetricExport {
    // "taken_on","datetime_id","person_id","latitude","longitude"
    // time;personId;latitude,longitude

    private Double latitude;
    private Double longitude;

    public LocationExport(Long time, Long personId, Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        super.setTime(time);
        super.setPersonId(personId);
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
