package cz.muni.irtis.repository.database.metrics.export;

public class NotificationExport extends MetricExport {
    // "datetime_id","person_id","package","posted","removed","text","title"
    //time;personId;name;package;posted;removed;text;title

    private String packageName;
    private Long posted;
    private Long removed;
    private String text;
    private String title;

    public NotificationExport(Long time, Long personId, String packageName, Long posted, Long removed, String text, String title) {
        this.packageName = packageName;
        this.posted = posted;
        this.removed = removed;
        this.text = text;
        this.title = title;
        super.setTime(time);
        super.setPersonId(personId);
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Long getPosted() {
        return posted;
    }

    public void setPosted(Long posted) {
        this.posted = posted;
    }

    public Long getRemoved() {
        return removed;
    }

    public void setRemoved(Long removed) {
        this.removed = removed;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
