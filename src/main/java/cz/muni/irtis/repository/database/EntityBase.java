package cz.muni.irtis.repository.database;

import com.google.gson.GsonBuilder;
import cz.muni.irtis.repository.model.RepositoryInstanceBase;

/**
 * EntityBase
 * 
 */
abstract public class EntityBase {

    static public <Entity extends EntityBase, Instance extends RepositoryInstanceBase> Entity build(Instance item) {
        return null;
    }

    abstract public Long getId();

    public String toString() {
        return new GsonBuilder().create().toJson(this);
    }
}
