package cz.muni.irtis.repository.database.metrics.screen;

import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.database.metrics.MetricIdentityEntity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "metrics_screen")
public class ScreenEntity extends EntityBase {
    @EmbeddedId
    private MetricIdentityEntity metricIdentity;
    private Boolean state;

    public ScreenEntity() {
    }

    public ScreenEntity(MetricIdentityEntity metricIdentity, Boolean state) {
        this.metricIdentity = metricIdentity;
        this.state = state;
    }

    public MetricIdentityEntity getMetricIdentity() {
        return metricIdentity;
    }

    public void setMetricIdentity(MetricIdentityEntity metricIdentity) {
        this.metricIdentity = metricIdentity;
    }

    @Override
    public Long getId() {
        return null;
    }

    public Boolean getState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }
}
