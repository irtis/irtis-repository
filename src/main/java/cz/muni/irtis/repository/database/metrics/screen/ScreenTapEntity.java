package cz.muni.irtis.repository.database.metrics.screen;

import cz.muni.irtis.repository.database.metrics.MetricIdentityEntity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "metrics_screen_taps")
public class ScreenTapEntity {
    @EmbeddedId
    private MetricIdentityEntity metricIdentity;
    private Integer x;
    private Integer y;

    public ScreenTapEntity() {
    }

    public ScreenTapEntity(MetricIdentityEntity metricIdentity, Integer x, Integer y) {
        this.metricIdentity = metricIdentity;
        this.x = x;
        this.y = y;
    }

    public MetricIdentityEntity getMetricIdentity() {
        return metricIdentity;
    }

    public void setMetricIdentity(MetricIdentityEntity metricIdentity) {
        this.metricIdentity = metricIdentity;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }
}
