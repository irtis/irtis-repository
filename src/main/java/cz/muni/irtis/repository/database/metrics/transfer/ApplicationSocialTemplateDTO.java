package cz.muni.irtis.repository.database.metrics.transfer;



import java.awt.*;

public class ApplicationSocialTemplateDTO {
    private String name;
    private String json;

    public ApplicationSocialTemplateDTO(String name, String json) {
        this.name = name;
        this.json = json;
    }

    public String getName() {
        return name;
    }

    public String getJson() {
        return json;
    }

    public Color getColor() {
        if(name.toLowerCase().equals("youtube")) {
            return new Color(255,0,0);
        } else if(name.toLowerCase().equals("facebook")) {
            return new Color(32, 134,255);
        } else if(name.toLowerCase().equals("messenger")) {
            return new Color(0,0,205);
        } else if(name.toLowerCase().equals("instagram")) {
            return new Color(255,140,0);
        } else if(name.toLowerCase().equals("twitter")) {
            return new Color(38, 174, 255);
        } else if(name.toLowerCase().equals("whatsapp")) {
            return new Color(50,205,50);
        } else if(name.toLowerCase().equals("snapchat")) {
            return new Color(255,255,0);
        } else if(name.toLowerCase().equals("tiktok")) {
            return new Color(199,21,133);
        } else if(name.toLowerCase().equals("viber")) {
            return new Color(123,104,238);
        } else if(name.toLowerCase().equals("tinder")) {
            return new Color(233,150,122);
        } else if(name.toLowerCase().equals("linkedin")) {
            return new Color(70,130,180);
        } else {
            return new Color(128, 128, 128);
        }
    }

    public String getColorBackground() {
        return "rgba("+getColor().getRed()+", "+getColor().getGreen()+", "+getColor().getBlue()+", 0.2)";
    }
    public String getColorBorder() {
        return "rgba("+getColor().getRed()+", "+getColor().getGreen()+", "+getColor().getBlue()+", 1)";
    }
}