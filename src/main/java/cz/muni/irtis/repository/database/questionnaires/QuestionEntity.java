package cz.muni.irtis.repository.database.questionnaires;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.muni.irtis.repository.database.EntityBase;

import javax.persistence.*;

@Entity
@Table(name = "questionnaires_questions")
public class QuestionEntity extends EntityBase {

//    @EmbeddedId
//    private QuestionId question;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name="questionnaire_id", foreignKey=@ForeignKey(name="fk_questionnaire_id"))
    @JsonBackReference
    private QuestionnaireEntity questionnaire;

    @ManyToOne
    @JoinColumn(name="question_template_id", foreignKey=@ForeignKey(name="fk_question_template_id"))
    private QuestionTemplate questionTemplate;

    private Integer type;
    private Integer density;
    private Boolean shuffle;
    private Integer link;
    private String text;
    private Integer position;

    public QuestionEntity() {
    }

    public QuestionEntity(Long id, QuestionnaireEntity questionnaire, Integer type, Integer density, Boolean shuffle, Integer link, String text, Integer position) {
        this.id = id;
        this.questionnaire = questionnaire;
        this.type = type;
        this.density = density;
        this.shuffle = shuffle;
        this.link = link;
        this.text = text;
        this.position = position;
    }

//    public QuestionEntity(QuestionJson response) {
//        this.id = response.getId();
//        this.type = response.getType();
//        this.density = response.getDensity();
//        this.shuffle = response.getShuffle();
//        this.link = response.getLink();
//        this.text = response.getText();
//        this.position = response.getPosition();
//    }

    public Long getId() {
        return id;
    }

//    public QuestionnaireEntity getQuestionnaire() {
//        return questionnaire;
//    }

    public Integer getType() {
        return type;
    }

    public Integer getDensity() {
        return density;
    }

    public Boolean getShuffle() {
        return shuffle;
    }

    public Integer getLink() {
        return link;
    }

    public String getText() {
        return text;
    }

    public Integer getPosition() {
        return position;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    public void setQuestionnaire(QuestionnaireEntity questionnaire) {
//        this.questionnaire = questionnaire;
//    }

    public void setType(Integer type) {
        this.type = type;
    }

    public void setDensity(Integer density) {
        this.density = density;
    }

    public void setShuffle(Boolean shuffle) {
        this.shuffle = shuffle;
    }

    public void setLink(Integer link) {
        this.link = link;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public QuestionTemplate getQuestionTemplate() {
        return questionTemplate;
    }

    public void setQuestionTemplate(QuestionTemplate questionTemplate) {
        this.questionTemplate = questionTemplate;
    }

    public String getSelectString() {
        if (questionTemplate != null) {
            return questionTemplate.getSelectString();
        } else {
            return "ERROR: missing template";
        }
    }
}
