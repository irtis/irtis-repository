package cz.muni.irtis.repository.database.metrics.screen;

import org.hibernate.transform.ResultTransformer;

import java.io.File;
import java.util.List;

public class ScreenshotApplicationObject extends ScreenshotEntity {
    private Long id;
    private Long time;
    private Long applicationId;
    private String applicationName;
    private Long personId;

    public ScreenshotApplicationObject(Long id, Long time, Long applicationId, String applicationName, String filename, Long personId, String metadata) {
        super(null, filename, metadata);
        this.id = id;
        this.time = time;
        this.applicationId = applicationId;
        this.applicationName = applicationName;
        this.personId = personId;
    }

    static public ResultTransformer buildResultTransformer() {
        return new ResultTransformer() {
            @Override
            public Object transformTuple(Object[] params, String[] strings) {
                return new ScreenshotApplicationObject(
                    params[0]!=null ? ((Number) params[0]).longValue() : null,
                    params[1]!=null ? ((Number) params[1]).longValue() : null,
                    params[2]!=null ? ((Number) params[2]).longValue() : null,
                    params[3]!=null ? (String) params[3]: null,
                    params[4]!=null ? (String) params[4]: null,
                    params[5]!=null ? ((Number) params[5]).longValue() : null,
                    params[6]!=null ? (String) params[6] : null
                );
            }

            @Override
            public List transformList(List list) {
                return list;
            }
        };
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Long applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public File getFile() {
        return new File("data/"+personId.toString()+"/screenshots/"+getFilename());
    }
}
