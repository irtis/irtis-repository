package cz.muni.irtis.repository.database.questionnaires;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class AnswerId implements Serializable {
    @Column(name = "questionnaire_id")
    private Long idQuestionnaire;
    @Column(name = "question_id")
    private Long idQuestion;
    @Column(name = "answer_id")
    private Long idAnswer;

    public AnswerId() {
    }

    public AnswerId(Long idQuestionnaire, Long idQuestion, Long idAnswer) {
        this.idQuestionnaire = idQuestionnaire;
        this.idQuestion = idQuestion;
        this.idAnswer = idAnswer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnswerId answerId = (AnswerId) o;
        return Objects.equals(idAnswer, answerId.idAnswer) &&
                Objects.equals(idQuestionnaire, answerId.idQuestionnaire) &&
                Objects.equals(idQuestion, answerId.idQuestion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idAnswer, idQuestionnaire, idQuestion);
    }
}
