package cz.muni.irtis.repository.database.metrics.transfer;


public class WifiDTO {
    private Long time;
    private Integer name;

    public WifiDTO(Long time, Integer name) {
        this.time = time;
        this.name = name;
    }

    public WifiDTO(Long time) {
        this.time = time;
    }

    public Long getTime() {
        return time;
    }

    public Integer getName() {
        return name;
    }
}
