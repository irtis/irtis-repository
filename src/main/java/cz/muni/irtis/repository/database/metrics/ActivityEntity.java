package cz.muni.irtis.repository.database.metrics;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "metrics_activities")
public class ActivityEntity {
    @EmbeddedId
    private MetricIdentityEntity metricIdentity;

    private String activity;
    private int confidence;

    public ActivityEntity() {
    }

    public ActivityEntity(MetricIdentityEntity metricIdentity, String activity, int confidence) {
        this.metricIdentity = metricIdentity;
        this.activity = activity;
        this.confidence = confidence;
    }

    public MetricIdentityEntity getMetricIdentity() {
        return metricIdentity;
    }

    public void setMetricIdentity(MetricIdentityEntity metricIdentity) {
        this.metricIdentity = metricIdentity;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public int getConfidence() {
        return confidence;
    }

    public void setConfidence(int confidence) {
        this.confidence = confidence;
    }
}
