package cz.muni.irtis.repository.database.questionnaires;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonsNotificationsRepository extends JpaRepository<PersonNotificationEntity, Long> {
    @Query("select count(pne) from PersonNotificationEntity pne where pne.personQuestionnaire.id = ?1")
    long countByPersonQuestionnaire(Long personQuestionnaire);
}
