package cz.muni.irtis.repository.database.metrics.phone;

import cz.muni.irtis.repository.database.metrics.MetricIdentityEntity;

import javax.persistence.*;

@Entity
@Table(name = "metrics_phone_smses")
public class PhoneSmsEntity {
    @EmbeddedId
    private MetricIdentityEntity metricIdentity;
    @Column(name="content", length=1024)
    private String content;
    @Column(name = "phone_number")
    private String phoneNumber;
    private String type;
    @Column(name = "message_date")
    private Long messageDate;


    public PhoneSmsEntity() {
    }

    public PhoneSmsEntity(MetricIdentityEntity metricIdentity, String phoneNumber, String type, String content, Long messageDate) {
        this.metricIdentity = metricIdentity;
        this.content = content;
        this.phoneNumber = phoneNumber;
        this.type = type;
        this.messageDate = messageDate;
    }

    public MetricIdentityEntity getMetricIdentity() {
        return metricIdentity;
    }

    public void setMetricIdentity(MetricIdentityEntity metricIdentity) {
        this.metricIdentity = metricIdentity;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(Long messageDate) {
        this.messageDate = messageDate;
    }
}
