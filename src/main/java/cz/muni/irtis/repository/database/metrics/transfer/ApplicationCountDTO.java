package cz.muni.irtis.repository.database.metrics.transfer;




public class ApplicationCountDTO {
    private String name;
    private Long count;

    public ApplicationCountDTO(String name, Long count) {
        this.name = name;
        this.count = count;
    }

    public ApplicationCountDTO(Object[] input) {
        this.name = (String) input[0];
        this.count = (Long) input[1];
    }

    public String getName() {
        return name;
    }

    public Long getCount() {
        return count;
    }
}
