package cz.muni.irtis.repository.database.access;

import cz.muni.irtis.repository.database.identity.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccessRepository extends JpaRepository<AccessEntity, Long> {
    @Query("SELECT a " +
            "FROM AccessEntity a " +
            "WHERE a.person = ?1 " +
            "AND a.link LIKE '/alive%' " +
            "AND a.time >= ?2 AND a.time <= ?3 " +
            "ORDER BY a.time ASC")
    Optional<List<AccessEntity>> findAllOfAliveByPersonAndInterval(PersonEntity person, Long tMin, Long tMax);

    @Query("SELECT a " +
            "FROM AccessEntity a " +
            "WHERE a.person = ?1 " +
            "AND a.link LIKE '/metrics%' " +
            "AND a.time >= ?2 AND a.time <= ?3 " +
            "ORDER BY a.time ASC")
    Optional<List<AccessEntity>> findAllOfSynchronizationByPersonAndInterval(PersonEntity person, Long tMin, Long tMax);

    @Query("SELECT a " +
            "FROM AccessEntity a " +
            "WHERE a.person = ?1 " +
            "AND a.link LIKE '/questionnaires/person/upload%' " +
            "AND a.time >= ?2 AND a.time <= ?3 " +
            "ORDER BY a.time ASC")
    Optional<List<AccessEntity>> findAllOfSurveysByPersonAndInterval(PersonEntity person, Long tMin, Long tMax);

    @Query(value = "SELECT a.* " +
            "FROM access a " +
            "WHERE a.person_id = :id " +
            "ORDER BY a.time DESC " +
            "LIMIT 1", nativeQuery = true
    )
    AccessEntity getLastRecord(Long id);
}
