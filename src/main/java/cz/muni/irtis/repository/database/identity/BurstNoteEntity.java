package cz.muni.irtis.repository.database.identity;

import cz.muni.irtis.repository.database.EntityBase;

import javax.persistence.*;

@Entity
@Table(name = "people_bursts_notes")
public class BurstNoteEntity extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "person_burst_id", nullable = false)
    private BurstEntity personBurst;

    @Column(name="content", length=1024*8)
    private String content;

    public BurstNoteEntity() {
    }

    public Long getId() {
        return id;
    }

    public BurstEntity getPersonBurst() {
        return personBurst;
    }

    public void setPersonBurst(BurstEntity personBurst) {
        this.personBurst = personBurst;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
