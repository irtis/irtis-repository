package cz.muni.irtis.repository.database.metrics.export;

public class BatteryExport extends MetricExport {
    // "taken_on","datetime_id","person_id","state_percent"
    // time;personId;statePercent

    private Integer statePercent;

    public BatteryExport(Long time, Long personId, Integer statePercent) {
        this.statePercent = statePercent;
        super.setTime(time);
        super.setPersonId(personId);
    }

    public Integer getStatePercent() {
        return statePercent;
    }

    public void setStatePercent(Integer statePercent) {
        this.statePercent = statePercent;
    }
}
