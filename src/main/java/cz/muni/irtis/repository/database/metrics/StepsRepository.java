package cz.muni.irtis.repository.database.metrics;

import cz.muni.irtis.repository.database.metrics.export.StepsExport;
import cz.muni.irtis.repository.database.metrics.screen.ScreenTapEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StepsRepository extends JpaRepository<StepEntity, MetricIdentityEntity> {
    @Query("SELECT new cz.muni.irtis.repository.database.metrics.export.StepsExport(t.time, e.metricIdentity.personId, e.steps) " +
            "FROM StepEntity e " +
            "JOIN TimeEntity t ON t.id = e.metricIdentity.datetimeId " +
            "WHERE person_id = ?1 " +
            "AND t.time >= ?2 AND t.time <= ?3 " +
            "ORDER BY t.time ASC")
    List<StepsExport> findAllByPersonAndInterval(Long person_id, Long tMin, Long tMax);
}
