package cz.muni.irtis.repository.database.metrics.export;

public class WifiConnectedExport extends MetricExport {
    // "taken_on","ssid","datetime_id","person_id","wifi_ssid_id"
    // time;personId;ssid

    private String ssid;

    public WifiConnectedExport(Long time, Long personId, String ssid) {
        this.ssid = ssid;
        super.setTime(time);
        super.setPersonId(personId);
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }
}
