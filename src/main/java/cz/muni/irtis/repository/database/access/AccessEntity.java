package cz.muni.irtis.repository.database.access;

import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.database.identity.PersonEntity;
import cz.muni.irtis.repository.model.access.Access;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;

@Entity
@Table(name = "access")
public class AccessEntity extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "person_id", nullable = true)
    private PersonEntity person;
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "api_id", nullable = true)
    private ApiEntity api;
    private String ip;
    private String link;
    private String application;
    private String language;
    private Long time;

    public AccessEntity() {
    }

    static public AccessEntity build(Access item) {
        AccessEntity entity = new AccessEntity();
        entity.setId(item.getId());
        if(item.getPerson()!=null) {
            entity.setPerson(PersonEntity.build(item.getPerson()));
        }
        if(item.getApi()!=null) {
            entity.setApi(ApiEntity.build(item.getApi()));
        }
        entity.setIp(item.getIp());
        entity.setLink(item.getLink());
        entity.setApplication(item.getApplication());
        entity.setLanguage(item.getLanguage());
        entity.setTime(item.getTime());
        return entity;
    }


    @Override
    public Long getId() {
        return id;
    }

    public PersonEntity getPerson() {
        return person;
    }

    public Long getPersonId() {
        if(getPerson()!=null) {
            return getPerson().getId();
        } else {
            return null;
        }
    }

    public String getPersonName() {
        if(getPerson()!=null) {
            return getPerson().getName();
        } else {
            return null;
        }
    }

    public ApiEntity getApi() {
        return api;
    }

    public String getIp() {
        return ip;
    }

    public String getLink() {
        return link;
    }

    public String getLinkFreeOfToken() {
        return getLink().replaceAll("token=([a-zA-Z0-9])+", "").replaceAll("\\?$", "");
    }

    public String getApplication() {
        return application;
    }

    public String getLanguage() {
        return language;
    }

    public Long getTime() {
        return time;
    }

    // TODO: move to Time
    public String getTimeFormatted() {
        DateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.SSS");
        Date result = new Date(getTime());
        return format.format(result);
    }


    public void setId(Long id) {
        this.id = id;
    }

    public void setPerson(PersonEntity person) {
        this.person = person;
    }

    public void setApi(ApiEntity api) {
        this.api = api;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
