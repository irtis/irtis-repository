package cz.muni.irtis.repository.database.questionnaires;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public enum QuestionType {
    TEXT("text", 1),
    CHECK("check", 2),
    SCALE("scale", 3),
    SLIDER("slider", 4),
    NUMBER("number", 5),
    SPEECH("speech", 6),
    TIME_SPINNER("time_spinner", 7),
    TIME_CLOCK("time_clock", 8),
    MULTIPLE_SLIDER("multiple_slider", 9),
    TIME_DATE("time_date", 10),
    TIME_BIRTHDATE("time_birthdate", 11),
    TIME_HOURSMINUTES("time_hoursminutes", 12);

    private static final Map<Integer, String> values = Arrays.stream(
            QuestionType.values())
            .collect(Collectors
                    .toMap(QuestionType::getValue, QuestionType::getType));

    private String type;
    private int value;

    QuestionType(String type, int value) {
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public static String getType(int code) {
        return values.get(code);
    }

    public static Optional<Integer> tryGetValue(String val) {
        return values.entrySet().stream()
                .filter(e -> e.getValue().equalsIgnoreCase(val))
                .findFirst()
                .map(Map.Entry::getKey);
    }

    public static Collection<String> getTypes() {
        return values.values();
    }

    public static boolean isChoiceType(Integer type) {
        return type == CHECK.getValue() || type == SCALE.getValue() || type == MULTIPLE_SLIDER.getValue();
    }
}
