package cz.muni.irtis.repository.database.questionnaires;

import cz.muni.irtis.repository.database.identity.PersonEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonsAnswersRepository extends JpaRepository<PersonAnswerEntity, Long> {
    Optional<PersonAnswerEntity> findOneByPerson(@Param("person") PersonEntity person);

    public Optional<List<PersonAnswerEntity>> findAllByPersonQuestion(
            @Param("person_question_id") PersonQuestionEntity question);

    @Query("select e " +
            "from PersonAnswerEntity e " +
            "where e.question = ?1 " +
            "and e.value is not null")
    public Optional<List<PersonAnswerEntity>> findAllByQuestion(QuestionEntity questionEntity, Sort sort);

    List<PersonAnswerEntity> findAllByPersonQuestionnaire(PersonQuestionnaireEntity pqe);
}
