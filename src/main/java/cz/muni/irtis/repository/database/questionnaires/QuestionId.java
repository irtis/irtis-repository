package cz.muni.irtis.repository.database.questionnaires;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class QuestionId implements Serializable {
    @Column(name = "question_id")
    private Long idQuestion;
    @Column(name = "questionnaire_id")
    private Long idQuestionnaire;

    public QuestionId() {
    }

    public QuestionId(Long idQuestion, Long idQuestionnaire) {
        this.idQuestion = idQuestion;
        this.idQuestionnaire = idQuestionnaire;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuestionId that = (QuestionId) o;
        return Objects.equals(idQuestion, that.idQuestion) &&
                Objects.equals(idQuestionnaire, that.idQuestionnaire);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idQuestion, idQuestionnaire);
    }
}
