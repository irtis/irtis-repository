package cz.muni.irtis.repository.database.questionnaires;


import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.database.identity.PersonEntity;

import javax.persistence.*;

@Entity
@Table(name = "people_questionnaires_questions")
public class PersonQuestionEntity extends EntityBase {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "person_id", nullable = false)
    private PersonEntity person;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "questionnaire_id", nullable = false)
    private QuestionnaireEntity questionnaire;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "people_questionnaire_id", nullable = false)
    private PersonQuestionnaireEntity personQuestionnaire;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "question_id", nullable = false)
    private QuestionEntity question;
    private Boolean skipped;
    private Integer elapsed;


    public PersonQuestionEntity() {
    }

    public PersonQuestionEntity(Long id, PersonEntity person, QuestionnaireEntity questionnaire, PersonQuestionnaireEntity personQuestionnaire,
                                     QuestionEntity question, Boolean skipped, Integer elapsed) {
        this.id = id;
        this.person = person;
        this.questionnaire = questionnaire;
        this.personQuestionnaire = personQuestionnaire;
        this.question = question;
        this.skipped = skipped;
        this.elapsed = elapsed;
    }

    public PersonQuestionEntity(PersonEntity person, QuestionnaireEntity questionnaire, PersonQuestionnaireEntity personQuestionnaire,
                                QuestionEntity question, Boolean skipped, Integer elapsed) {
        this.person = person;
        this.questionnaire = questionnaire;
        this.personQuestionnaire = personQuestionnaire;
        this.question = question;
        this.skipped = skipped;
        this.elapsed = elapsed;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonEntity getPerson() {
        return person;
    }

    public void setPerson(PersonEntity person) {
        this.person = person;
    }

    public QuestionnaireEntity getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(QuestionnaireEntity questionnaire) {
        this.questionnaire = questionnaire;
    }

    public PersonQuestionnaireEntity getPersonQuestionnaire() {
        return personQuestionnaire;
    }

    public void setPersonQuestionnaire(PersonQuestionnaireEntity personQuestionnaire) {
        this.personQuestionnaire = personQuestionnaire;
    }

    public QuestionEntity getQuestion() {
        return question;
    }

    public void setQuestion(QuestionEntity question) {
        this.question = question;
    }

    public Boolean getSkipped() {
        return skipped;
    }

    public void setSkipped(Boolean skipped) {
        this.skipped = skipped;
    }

    public Integer getElapsed() {
        return elapsed;
    }

    public void setElapsed(Integer elapsed) {
        this.elapsed = elapsed;
    }
}