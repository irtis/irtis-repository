package cz.muni.irtis.repository.database.questionnaires;


import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.database.identity.PersonEntity;

import javax.persistence.*;

@Entity
@Table(name = "people_questionnaires_notifications")
public class PersonNotificationEntity extends EntityBase {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "person_id", nullable = false)
    private PersonEntity person;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "questionnaire_id", nullable = false)
    private QuestionnaireEntity questionnaire;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "people_questionnaire_id", nullable = false)
    private PersonQuestionnaireEntity personQuestionnaire;
    @Column(name="action")
    private Integer action;
    @Column(name="time")
    private Long time;


    public PersonNotificationEntity() {
    }

    public PersonNotificationEntity(Long id, PersonEntity person, PersonQuestionnaireEntity personQuestionnaire,
                                    Integer action, Long time) {
        this.id = id;
        this.person = person;
        this.questionnaire = personQuestionnaire.getQuestionnaire();
        this.personQuestionnaire = personQuestionnaire;
        this.action = action;
        this.time = time;
    }

    public PersonNotificationEntity(PersonEntity person, PersonQuestionnaireEntity personQuestionnaire,
                                    Integer action, Long time) {
        this.person = person;
        this.questionnaire = personQuestionnaire.getQuestionnaire();
        this.personQuestionnaire = personQuestionnaire;
        this.action = action;
        this.time = time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonEntity getPerson() {
        return person;
    }

    public void setPerson(PersonEntity person) {
        this.person = person;
    }

    public QuestionnaireEntity getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(QuestionnaireEntity questionnaire) {
        this.questionnaire = questionnaire;
    }

    public PersonQuestionnaireEntity getPersonQuestionnaire() {
        return personQuestionnaire;
    }

    public void setPersonQuestionnaire(PersonQuestionnaireEntity personQuestionnaire) {
        this.personQuestionnaire = personQuestionnaire;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}