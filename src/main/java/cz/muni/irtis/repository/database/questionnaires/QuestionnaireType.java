package cz.muni.irtis.repository.database.questionnaires;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public enum QuestionnaireType {
    REGULAR("regular", 1),
    INVOKED("invoked", 2),
    TRIGGERED("triggered", 3),
    SPECIAL("special", 4);

    private static final Map<Integer, String> values = Arrays.stream(
            QuestionnaireType.values())
            .collect(Collectors
                    .toMap(QuestionnaireType::getValue, QuestionnaireType::getType));

    private String type;
    private int value;

    QuestionnaireType(String type, int value) {
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public static String getType(int code) {
        return values.get(code);
    }

    public static Optional<Integer> tryGetValue(String val) {
        return values.entrySet().stream()
                .filter(e -> e.getValue().equalsIgnoreCase(val))
                .findFirst()
                .map(Map.Entry::getKey);
    }

    public static Collection<String> getTypes() {
        return values.values();
    }
}
