package cz.muni.irtis.repository.database.questionnaires;


import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.database.identity.PersonEntity;
import javax.persistence.*;

@Entity
@Table(name = "people_questionnaires_answers")
public class PersonAnswerEntity extends EntityBase {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "person_id", nullable = false)
    private PersonEntity person;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "questionnaire_id", nullable = false)
    private QuestionnaireEntity questionnaire;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "people_questionnaire_id", nullable = false)
    private PersonQuestionnaireEntity personQuestionnaire;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "question_id", nullable = false)
    private QuestionEntity question;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "people_question_id", nullable = false)
    private PersonQuestionEntity personQuestion;
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "answer_id", nullable = true)
    private AnswerEntity answer;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "multi_slider_label_id", foreignKey=@ForeignKey(name="fk_multi_slider_label_id"))
    private MultiSliderLabel multiSliderLabel;
    @Column(name="value", length=5120)
    private String value;


    public PersonAnswerEntity() {
    }

    public PersonAnswerEntity(Long id, PersonEntity person, QuestionnaireEntity questionnaire, PersonQuestionnaireEntity personQuestionnaire,
                              QuestionEntity question, PersonQuestionEntity personQuestion, AnswerEntity answer, String value) {
        this.id = id;
        this.person = person;
        this.questionnaire = questionnaire;
        this.personQuestionnaire = personQuestionnaire;
        this.question = question;
        this.personQuestion = personQuestion;
        this.answer = answer;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public PersonEntity getPerson() {
        return person;
    }

    public QuestionnaireEntity getQuestionnaire() {
        return questionnaire;
    }

    public QuestionEntity getQuestion() {
        return question;
    }

    public AnswerEntity getAnswer() {
        return answer;
    }

    public PersonQuestionnaireEntity getPersonQuestionnaire() {
        return personQuestionnaire;
    }

    public PersonQuestionEntity getPersonQuestion() {
        return personQuestion;
    }

    public String getValue() {
        return value;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPerson(PersonEntity person) {
        this.person = person;
    }

    public void setQuestionnaire(QuestionnaireEntity questionnaire) {
        this.questionnaire = questionnaire;
    }

    public void setPersonQuestionnaire(PersonQuestionnaireEntity personQuestionnaire) {
        this.personQuestionnaire = personQuestionnaire;
    }

    public void setQuestion(QuestionEntity question) {
        this.question = question;
    }

    public void setPersonQuestion(PersonQuestionEntity personQuestion) {
        this.personQuestion = personQuestion;
    }

    public void setAnswer(AnswerEntity answer) {
        this.answer = answer;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getPersonId() {
        return person.getId();
    }

    public String getPersonName() {
        return person.getName();
    }

    public Long getPersonQuestionnaireId() {
        return getPersonQuestionnaire()!=null ? getPersonQuestionnaire().getId() : null;
    }

    public String getPersonQuestionnaireCreatedFormatted() {
        return getPersonQuestionnaire()!=null ? getPersonQuestionnaire().getCreatedFormatted() : null;
    }

    public MultiSliderLabel getMultiSliderLabel() {
        return multiSliderLabel;
    }

    public void setMultiSliderLabel(MultiSliderLabel multiSliderLabel) {
        this.multiSliderLabel = multiSliderLabel;
    }
}
