package cz.muni.irtis.repository.database.messages;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public enum MessageType {
    TYPE_SCREENSHOTS_OFF_WARNING("Screenshots off warning", 1),
    TYPE_QUESTIONNAIRES_WARNING("Questionnaires warning", 2),
    TYPE_QUESTIONNAIRES_COINS_ENCOURAGEMENT("Questionnaires coins encouragement", 3),

    TYPE_STUDIES_START("Study start", 11),
    TYPE_STUDIES_END("Study end", 12),
    TYPE_BURSTS_START("Burst start", 13),
    TYPE_BURSTS_END("Burst end", 14),
    TYPE_BURSTS_START_REMINDER("Burst start reminder", 15),
    TYPE_BURSTS_END_REMINDER("Burst end reminder", 16);


    private String type;
    private int value;

    private static final Map<Integer, String> values = Arrays.stream(
            MessageType.values())
                .collect(Collectors
                        .toMap(MessageType::getValue, MessageType::getType));

    MessageType(String type, int value) {
        this.type = type;
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public String getType() {
        return type;
    }

    public static String getType(int code) {
        return values.get(code);
    }
}
