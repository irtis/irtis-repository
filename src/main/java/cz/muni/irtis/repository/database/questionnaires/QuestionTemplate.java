package cz.muni.irtis.repository.database.questionnaires;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import cz.muni.irtis.repository.database.EntityBase;

import javax.persistence.*;
import java.util.Optional;
import java.util.Set;

@Entity
@Table(name = "question_template")
public class QuestionTemplate extends EntityBase {
    @Transient
    public static int MAX_DISPLAY_CHARS = 50;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer density;
    private Integer link;
    private boolean shuffle;
    private String text; // DB length is 255
    private String tag;

    @Access(AccessType.PROPERTY)
    private Integer type;

    @OneToMany(mappedBy="questionTemplate")
    private Set<QuestionEntity> questionEntities;

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="questionTemplate", orphanRemoval=true)
    @JsonManagedReference
    private Set<AnswerEntity> answerChoices;

    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    @JoinColumn(name="question_id")
    private Set<MultiSliderLabel> multiSliderLabels;

    @Transient
    private String qType;

    @Transient
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String selectString;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Integer getDensity() {
        return density;
    }

    public void setDensity(Integer density) {
        this.density = density;
    }

    public Integer getLink() {
        return link;
    }

    public void setLink(Integer link) {
        this.link = link;
    }

    public boolean isShuffle() {
        return shuffle;
    }

    public void setShuffle(boolean shuffle) {
        this.shuffle = shuffle;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
        this.qType = QuestionType.getType(type);
    }

    public Set<AnswerEntity> getAnswerChoices() {
        return answerChoices;
    }

    public void setAnswerChoices(Set<AnswerEntity> answers) {
        this.answerChoices = answers;
    }

    public String getqType() {
        return qType;
    }

    public void setqType(String qType) {
        Optional<Integer> maybeType = QuestionType.tryGetValue(qType);
        if (maybeType.isPresent()) {
            this.qType = qType;
            this.type = maybeType.get();
        }
    }

    @JsonProperty
    public String getSelectString() {
        if (id != null && text != null && type != null) {
            String displayText = text.length() <= MAX_DISPLAY_CHARS
                    ? text
                    : text.substring(0, MAX_DISPLAY_CHARS) + "...";
            String tagStr = (tag == null || tag.length() < 1) ? "" : "; " + tag;
            return id + tagStr + "; " + QuestionType.getType(type) + "; " + displayText;
        }
        return "";
    }

    @JsonIgnore
    public void setSelectString(String xxx) {
        this.selectString = getSelectString();
    }

    public Set<MultiSliderLabel> getMultiSliderLabels() {
        return multiSliderLabels;
    }

    public void setMultiSliderLabels(Set<MultiSliderLabel> multiSliderLabels) {
        this.multiSliderLabels = multiSliderLabels;
    }

    public String getTypeString() {
        return QuestionType.getType(type);
    }

    public void update(QuestionTemplate json) {
        this.density = json.getDensity();
        this.link = json.getLink();
//        this.position = json.getPosition();
        this.shuffle = json.isShuffle();
        this.text = json.getText();
        this.type = json.getType();
//        this.answerChoices = json.getAnswerChoices();
//        this.multiSliderLabels = json.getMultiSliderLabels();

        Optional<Integer> maybeType = QuestionType.tryGetValue(json.getqType());
        maybeType.ifPresent(this::setType);
    }
}
