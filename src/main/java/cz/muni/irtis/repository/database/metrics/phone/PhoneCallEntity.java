package cz.muni.irtis.repository.database.metrics.phone;


import cz.muni.irtis.repository.database.metrics.MetricIdentityEntity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "metrics_phone_calls")
public class PhoneCallEntity {
    @EmbeddedId
    private MetricIdentityEntity metricIdentity;
    private String name;
    @Column(name = "phone_number")
    private String phoneNumber;
    private String type;
    private int duration;
    @Column(name = "call_date")
    private Long callDate;


    public PhoneCallEntity() {
    }

    public PhoneCallEntity(MetricIdentityEntity metricIdentity, String name, String phoneNumber, String type, int duration, Long callDate) {
        this.metricIdentity = metricIdentity;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.type = type;
        this.duration = duration;
        this.callDate = callDate;
    }

    public MetricIdentityEntity getMetricIdentity() {
        return metricIdentity;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getType() {
        return type;
    }

    public int getDuration() {
        return duration;
    }

    public Long getCallDate() {
        return callDate;
    }
}
