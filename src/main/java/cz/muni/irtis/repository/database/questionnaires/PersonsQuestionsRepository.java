package cz.muni.irtis.repository.database.questionnaires;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonsQuestionsRepository extends JpaRepository<PersonQuestionEntity, Long> {
    public Optional<List<PersonQuestionEntity>> findAllByPersonQuestionnaireAndQuestion(
            @Param("person_questionnaire_id") PersonQuestionnaireEntity questionnaire,
            @Param("question_id") QuestionEntity question);

    public Optional<PersonQuestionEntity> findOneByPersonQuestionnaireAndQuestion(
            @Param("person_questionnaire_id") PersonQuestionnaireEntity questionnaire,
            @Param("question_id") QuestionEntity question);
}
