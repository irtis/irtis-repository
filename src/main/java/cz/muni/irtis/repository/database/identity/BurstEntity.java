package cz.muni.irtis.repository.database.identity;

import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.model.persons.Burst;
import cz.muni.irtis.repository.model.utils.Time;

import javax.persistence.*;

@Entity
@Table(name = "people_bursts")
public class BurstEntity extends EntityBase{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "person_id", nullable = false)
    private PersonEntity person;
    private String identifier;       // Name, identifier of a burst
    private String title;
    private Long starting;
    private Long ending;


    public BurstEntity() {
    }

    static public BurstEntity build(Burst item) {
        BurstEntity entity = new BurstEntity();
        entity.setId(item.getId());
        entity.setPerson(PersonEntity.build(item.getPerson()));
        entity.setIdentifier(item.getIdentifier());
        entity.setTitle(item.getTitle());
        entity.setStarting(item.getStarting());
        entity.setEnding(item.getEnding());
        return entity;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonEntity getPerson() {
        return person;
    }

    public void setPerson(PersonEntity person) {
        this.person = person;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getStarting() {
        return starting;
    }

    public void setStarting(Long starting) {
        this.starting = starting;
    }

    public Long getEnding() {
        return ending;
    }

    public void setEnding(Long ending) {
        this.ending = ending;
    }

    public String getStartingString() {
        return Time.getTimeFormatted(starting);
    }

    public String getEndingString() {
        return Time.getTimeFormatted(ending);
    }
}