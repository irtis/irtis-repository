package cz.muni.irtis.repository.database.metrics.wifi;

import javax.persistence.*;

@Entity
@Table(
    name = "metrics_wifis",
    indexes = {
        @Index(name = "index_ssid", columnList="ssid", unique = true)
    }
)
public class WifiEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String ssid;


    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public Long getId() {
        return id;
    }
}
