package cz.muni.irtis.repository.database.identity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PeopleBurstsRepository extends JpaRepository<BurstEntity, Long> {
    List<BurstEntity> findAllByIdentifier(String identifier);

    @Query(value =
            "select g.identifier " +
                    "from (select pb.identifier, max(pb.ending) as ending " +
                    "from people_bursts pb " +
                    "where pb.identifier is not null " +
                    "group by pb.identifier) as g " +
            "order by g.ending desc", nativeQuery = true)
    List<String> findAllBurstIdentifiers();
}
