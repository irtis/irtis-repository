package cz.muni.irtis.repository.database.messages;

import cz.muni.irtis.repository.database.identity.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface MessagesRepository extends JpaRepository<MessageEntity, Long> {
    List<MessageEntity> findAllByPersonId(Long person_id);

    List<MessageEntity> findAllByPersonInAndDownloadedBetween(List<PersonEntity> people, Long studyStart, Long studyEnd);

    List<MessageEntity> findAllByPersonInAndCreatedBetween(List<PersonEntity> people, Long studyStart, Long studyEnd);

    List<MessageEntity> findAllByPersonInAndValidFromBetween(List<PersonEntity> people, Long studyStart, Long studyEnd);

    Optional<List<MessageEntity>> findAllByPersonAndDownloadedIsNullOrderByCreatedDesc(
            @Param("person_id") PersonEntity person);
    Optional<MessageEntity> findOneById(
            @Param("id") Long id);

    Optional<MessageEntity> findFirstByPersonAndTypeOrderByCreatedDesc(@Param("person_id") PersonEntity person, @Param("type") Integer type);
}
