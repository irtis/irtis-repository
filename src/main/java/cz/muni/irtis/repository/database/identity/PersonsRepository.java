package cz.muni.irtis.repository.database.identity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface PersonsRepository extends JpaRepository<PersonEntity, Long> {
    List<PersonEntity> findAllByIdIn(List<Long> personIds);
    Optional<PersonEntity> findByIdAndPassword(@Param("id") Long login, @Param("password") String password);
    Optional<PersonEntity> findByToken(@Param("token") String token);

    @Query(
        value = "select p.* " +
            "FROM people p " +
            "WHERE (EXTRACT(EPOCH FROM NOW()) * 1000) - (select alive.time from alive where alive.person_id = p.id order by alive.time desc limit 1) < 3600*24*1000",
        nativeQuery = true)
    List<PersonEntity> findAllActive();

    @Query(
        value = "select p.* " +
                "FROM people p " +
                "WHERE (EXTRACT(EPOCH FROM NOW()) * 1000) - (select alive.time from alive where alive.person_id = p.id order by alive.time desc limit 1) > 3600*24*1000",
        nativeQuery = true)
    List<PersonEntity> findAllInActive();

    @Query(
            value = "SELECT p.* " +
                    "FROM people p " +
                    "WHERE id IN :ids", nativeQuery = true )
    List<PersonEntity> findByIdIn(@Param("ids") List<Long> idList);

    @Query(
            value = "SELECT p.* " +
                    "FROM people p " +
                    "WHERE id = :id", nativeQuery = true )
    Optional<PersonEntity> findById(@Param("id") Long id);

    @Query("select id from PersonEntity")
    List<Long> getIds();

    List<PersonEntity> findByManagerId(@Param("id") Long id);
}
