package cz.muni.irtis.repository.database.metrics.wifi;

import cz.muni.irtis.repository.database.metrics.MetricIdentityEntity;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "metrics_wifis_available")
public class WifiAvailableEntity {
    @EmbeddedId
    private MetricIdentityEntity metricIdentity;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "wifi_ssid_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private WifiEntity wifiSsid;

    public WifiAvailableEntity() {
    }

    public WifiAvailableEntity(MetricIdentityEntity metricIdentity, WifiEntity wifiSsid) {
        this.metricIdentity = metricIdentity;
        this.wifiSsid = wifiSsid;
    }

    public MetricIdentityEntity getMetricIdentity() {
        return metricIdentity;
    }

    public void setMetricIdentity(MetricIdentityEntity metricIdentity) {
        this.metricIdentity = metricIdentity;
    }

    public WifiEntity getWifiSsid() {
        return wifiSsid;
    }

    public void setWifiSsid(WifiEntity wifiSsid) {
        this.wifiSsid = wifiSsid;
    }
}
