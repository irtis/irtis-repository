package cz.muni.irtis.repository.database.metrics.export;

public class RuntimeExport extends MetricExport {
    // "taken_on","datetime_id","person_id","started","stopped","type"
    // time;personId;name;started;stopped,type

    private Long started;
    private Long stopped;
    private Integer type;

    public RuntimeExport(Long time, Long personId, Long started, Long stopped, Integer type) {
        this.started = started;
        this.stopped = stopped;
        this.type = type;
        super.setTime(time);
        super.setPersonId(personId);
    }

    public Long getStarted() {
        return started;
    }

    public void setStarted(Long started) {
        this.started = started;
    }

    public Long getStopped() {
        return stopped;
    }

    public void setStopped(Long stopped) {
        this.stopped = stopped;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
