package cz.muni.irtis.repository.database.questionnaires;

import cz.muni.irtis.repository.model.questionnaires.QuestionnairesRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.*;

@Repository
@Transactional
public class QuestionnaireRepositoryExtendedImpl implements QuestionnaireRepositoryExtended {

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Save and update entity. Set created for new entity and updated for existing.
     * @param incomingEntity questionnaire
     * @return updated managed questionnaire
     */
    @Override
    public QuestionnaireEntity saveWithHistory(QuestionnaireEntity incomingEntity) {
        if (incomingEntity.getId() == null) {
            incomingEntity.setCreated(System.currentTimeMillis());
            incomingEntity.setType(incomingEntity.getType());
            incomingEntity.setIdentifier(incomingEntity.getTitle().trim().toLowerCase().replace(' ', '-'));
            incomingEntity.setAnswerLinks(new HashSet<>());
            setSelfInitiatedInterval(incomingEntity);
            QuestionnaireEntity merged = entityManager.merge(incomingEntity);
            merged.createIdentifier();
            entityManager.merge(merged);
        } else {
            QuestionnaireEntity oldEntity = entityManager.find(QuestionnaireEntity.class, incomingEntity.getId());
            incomingEntity.setCreated(oldEntity.getCreated());
            incomingEntity.setUpdated(System.currentTimeMillis());
            incomingEntity.createIdentifier();
            incomingEntity.setGroup(oldEntity.getGroup());

            // set links
            Set<AnswerLink> answerLinks = incomingEntity.getAnswerLinks();
            int oldSize = answerLinks.size();
            for (AnswerLink al : answerLinks) {
                if (oldEntity.getAnswerLinks().contains(al)) {
                    continue;
                }
                al.setQuestionnaire(oldEntity);
                Optional<AnswerEntity> answerByChoice = entityManager.find(QuestionEntity.class, al.getTargetQuestion().getId())
                        .getQuestionTemplate().getAnswerChoices().stream()
                        .filter(choice -> choice.getText().equals(al.getChoice())).findFirst();
                if (answerByChoice.isPresent()) {
                    al.setAnswer(answerByChoice.get());
                    al.setId(new AnswerLinkPK(al));
                    entityManager.merge(al);
                } else {
                    throw new RuntimeException("Answer choice not found with text: " + al.getChoice());
                }
            }
            if (incomingEntity.getAnswerLinks().size() != oldSize) {
                incomingEntity.setAnswerLinks(answerLinks);
            }
            entityManager.merge(incomingEntity);
        }
        return incomingEntity;
    }

    public List<AnswerLink> getOwnerAnswerLinks(Long ownerId) {
        List<AnswerLink> answerEntities = entityManager.createQuery(
                "select new AnswerLink(al, ae.text) " +
                        "from AnswerLink al join AnswerEntity ae on al.answer.id = ae.id " +
                        "where :ownerId = al.ownerQuestion.id ", AnswerLink.class)
                .setParameter("ownerId", ownerId)
                .getResultList();
        return answerEntities;
    }

    private AnswerEntity getAnswerByChoice(QuestionEntity entity, String choice) {
        List<AnswerEntity> answerEntities = entityManager.createQuery(
                "select ae " +
                "from QuestionTemplate qt join qt.answerChoices ae " +
                "where :questionEntity = qt.id " +
                "and ae.text = :choice", AnswerEntity.class)
                .setParameter("questionEntity", entity.getQuestionTemplate().getId())
                .setParameter("choice", choice)
                .getResultList();

        if (answerEntities.size() > 0) {
            return answerEntities.get(0);
        }
        throw new RuntimeException("No answer entity found!");
    }

    // TODO: retrieve interval from study period
    private void setSelfInitiatedInterval(QuestionnaireEntity incomingEntity) {
        if (incomingEntity.getType() != QuestionnaireType.INVOKED.getValue()
            && incomingEntity.getType() != QuestionnaireType.TRIGGERED.getValue()) {
            return;
        }
        incomingEntity.setValidFrom(incomingEntity.getCreated());
        incomingEntity.setValidTo(incomingEntity.getCreated() + 3*604800000);// 3 weeks
    }
}
