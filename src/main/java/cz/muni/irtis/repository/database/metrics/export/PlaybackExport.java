package cz.muni.irtis.repository.database.metrics.export;

public class PlaybackExport extends MetricExport {
    //"taken_on","datetime_id","person_id","active"
    // time;personId;active

    private Boolean active;

    public PlaybackExport(Long time, Long personId, Boolean active) {
        this.active = active;
        super.setTime(time);
        super.setPersonId(personId);
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
