package cz.muni.irtis.repository.database.metrics.transfer;



import java.awt.*;

public class ActivityTemplateDTO {
    private String type;
    private String json;

    public ActivityTemplateDTO(String type, String json) {
        this.type = type;
        this.json = json;
    }

    public String getType() {
        return type;
    }

    public String getJson() {
        return json;
    }

    public String getTitle() {
        // Transport
        if(type.equals("IN_VEHICLE")) {
            return "Vehicle";
        } else if(type.equals("ON_BICYCLE")) {
            return "Bicycle";
        } else if(type.equals("ON_FOOT")) {
            return "Foot";

            // Acivity
        } else if(type.equals("RUNNING")) {
            return "Running";
        } else if(type.equals("WALKING")) {
            return "Walking";
        } else if(type.equals("STILL")) {
            return "Still";

        } else if(type.equals("TILTING")) {
            return "Tilting";
        } else {
            return "Unknown";
        }
    }

    public Color getColor() {
        if(type.toLowerCase().equals("still")) {
            return new Color(255,0,0);
        } else if(type.toLowerCase().equals("walking")) {
            return new Color(32, 134,255);
        } else if(type.toLowerCase().equals("running")) {
            return new Color(50,205,50);
        } else if(type.toLowerCase().equals("on_foot")) {
            return new Color(255,140,0);
        } else if(type.toLowerCase().equals("on_bicycle")) {
            return new Color(255,255,0);
        } else if(type.toLowerCase().equals("tilting")) {
            return new Color(199,21,133);
        } else if(type.toLowerCase().equals("in_vehicle")) {
            return new Color(233,150,122);
        } else {
            return new Color(128, 128, 128);
        }
    }

    public String getColorBackground() {
        return "rgba("+getColor().getRed()+", "+getColor().getGreen()+", "+getColor().getBlue()+", 0.2)";
    }
    public String getColorBorder() {
        return "rgba("+getColor().getRed()+", "+getColor().getGreen()+", "+getColor().getBlue()+", 1)";
    }
}