package cz.muni.irtis.repository.database.questionnaires;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

// TODO> try with other projects and rewrite if needed
@Repository
public interface AnswersRepository extends JpaRepository<AnswerEntity, Long> {
//    Optional<List<AnswerEntity>> findByQuestionnaireOrderByPositionAsc(@Param("questionnaire_id") QuestionnaireEntity questionnaire);
//    Optional<List<AnswerEntity>> findByQuestionOrderByPositionAsc(@Param("question_id") QuestionEntity question);

//    @Transactional
//    void deleteByQuestionnaire(@Param("questionnaire_id") QuestionnaireEntity questionnaire);
}
