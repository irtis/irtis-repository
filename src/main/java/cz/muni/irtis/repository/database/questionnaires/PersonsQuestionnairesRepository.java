package cz.muni.irtis.repository.database.questionnaires;

import cz.muni.irtis.repository.database.identity.PersonEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonsQuestionnairesRepository extends JpaRepository<PersonQuestionnaireEntity, Long>, PersonsQuestionnairesRepositoryExtended {
    // This is probably the longest function name I have ever used in my life :-D
    Optional<PersonQuestionnaireEntity> findFirstByPersonAndClosingGreaterThanEqualAndCompletedIsNullOrderByCreatedDesc(
            @Param("person_id") PersonEntity person, @Param("closing") Long now);
    Optional<PersonQuestionnaireEntity> findFirstByPersonAndCompletedIsNullOrderByCreatedDesc(@Param("person_id") PersonEntity person);
    Optional<PersonQuestionnaireEntity> findFirstByPersonAndDownloadedIsNullAndCompletedIsNullOrderByCreatedDesc(@Param("person_id") PersonEntity person);

    Page<PersonQuestionnaireEntity> findAllByPerson(@Param("person_id") PersonEntity person, Pageable page);

    List<PersonQuestionnaireEntity> findAllByPersonAndCreatedBetween(PersonEntity person, Long created, Long created2);

    List<PersonQuestionnaireEntity> findAllByPersonAndNotifiedBetween(PersonEntity person, Long created, Long created2);

    List<PersonQuestionnaireEntity> findAllByPersonAndStartingBetween(PersonEntity person, Long created, Long created2);

    List<PersonQuestionnaireEntity> findAllByAssignmentId(Long id);

    @Query("select pqe " +
            "from PersonQuestionnaireEntity pqe " +
            "where pqe.person = ?1 " +
            "and pqe.starting >= ?2 " +
            "and pqe.starting <= ?3 " +
            "and pqe.questionnaire.type = 1")
    List<PersonQuestionnaireEntity> findRegularByPersonAndStartingBetween(PersonEntity person, Long created, Long created2);

    @Query(
            value = "SELECT qq.position as position_in_questionnaire, qt.text as question_text, qt.type, pqa.value, " +
                        "qa.text as answer_text, qa.link, qa.position as answer_position_in_picker, pqq.skipped, " +
                        "msl.text as multi_slider_text " +
                    "FROM public.questionnaires q " +
                    "	JOIN public.questionnaires_questions qq ON qq.questionnaire_id = q.id " +
                    "   JOIN public.question_template qt ON qt.id = qq.question_template_id " +
                    "	JOIN public.people_questionnaires pq ON pq.questionnaire_id = q.id " +
                    "	LEFT JOIN public.people_questionnaires_answers pqa ON pqa.people_questionnaire_id = pq.id AND qq.id = pqa.question_id " +
                    "	LEFT JOIN public.questionnaires_answers qa ON pqa.answer_id = qa.id " +
                    "	LEFT JOIN public.people_questionnaires_questions pqq ON pqq.people_questionnaire_id = pq.id AND qq.id = pqq.question_id " +
                    "   LEFT JOIN public.multi_slider_labels msl ON msl.id = pqa.multi_slider_label_id " +
                    "WHERE pq.id = :pqId " +
                    "ORDER BY position_in_questionnaire, qq.id, pqa.id ASC", nativeQuery = true )
    List<Object> getAnsweredQuestionnaireById(@Param("pqId") Long pqId);

    @Query(
            value = "SELECT * FROM people_questionnaires e " +
                    "WHERE e.person_id = ?1 " +
                        "AND (e.starting >= ?2 OR e.starting IS NULL) " +
                        "AND e.uploaded IS NULL " +
                    "ORDER BY e.created DESC", nativeQuery = true)
    List<PersonQuestionnaireEntity> findAllAvailableByPerson(
        @Param("person_id") PersonEntity person, @Param("starting") Long now);

    List<PersonQuestionnaireEntity> findAllByStartingBetweenAndPersonIn(Long start, Long end, List<PersonEntity> personIds);
    List<PersonQuestionnaireEntity> findAllByCompletedBetweenAndPersonIn(Long start, Long end, List<PersonEntity> personIds);
}

