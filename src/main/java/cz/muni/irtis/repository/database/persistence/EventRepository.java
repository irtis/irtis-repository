package cz.muni.irtis.repository.database.persistence;

import cz.muni.irtis.repository.database.identity.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<EventEntity, Long> {
    List<EventEntity> findAllByPersonInAndTimeBetween(List<PersonEntity> people, Long studyStart, Long studyEnd);

    List<EventEntity> findAllByPersonAndTimeBetween(PersonEntity person, Long studyStart, Long studyEnd);
}
