package cz.muni.irtis.repository.database.questionnaires;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class AnswerLinkPK implements Serializable {
    @Column(name = "questionnaire_id")
    private Long questionnaireId;
    @Column(name = "target_question_id")
    private Long targetQuestionId;
    @Column(name = "owner_question_id")
    private Long ownerQuestionId;
    @Column(name = "answer_id")
    private Long answerId;

    public AnswerLinkPK() {
    }

    public AnswerLinkPK(Long questionnaireId, Long targetQuestionId, Long ownerQuestionId, Long answerId) {
        this.questionnaireId = questionnaireId;
        this.targetQuestionId = targetQuestionId;
        this.ownerQuestionId = ownerQuestionId;
        this.answerId = answerId;
    }

    public AnswerLinkPK(AnswerLink entity) {
        this.questionnaireId = entity.getQuestionnaire().getId();
        this.targetQuestionId = entity.getTargetQuestion().getId();
        this.ownerQuestionId = entity.getOwnerQuestion().getId();
        this.answerId = entity.getAnswer().getId();
    }

    public Long getQuestionnaireId() {
        return questionnaireId;
    }

    public void setQuestionnaireId(Long questionnaireId) {
        this.questionnaireId = questionnaireId;
    }

    public Long getTargetQuestionId() {
        return targetQuestionId;
    }

    public void setTargetQuestionId(Long targetQuestionId) {
        this.targetQuestionId = targetQuestionId;
    }

    public Long getOwnerQuestionId() {
        return ownerQuestionId;
    }

    public void setOwnerQuestionId(Long ownerQuestionId) {
        this.ownerQuestionId = ownerQuestionId;
    }

    public Long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Long answerId) {
        this.answerId = answerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnswerLinkPK that = (AnswerLinkPK) o;
        return Objects.equals(questionnaireId, that.questionnaireId) &&
                Objects.equals(targetQuestionId, that.targetQuestionId) &&
                Objects.equals(ownerQuestionId, that.ownerQuestionId) &&
                Objects.equals(answerId, that.answerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(questionnaireId, targetQuestionId, ownerQuestionId, answerId);
    }
}
