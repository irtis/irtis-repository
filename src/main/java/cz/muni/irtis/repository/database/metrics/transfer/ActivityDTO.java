package cz.muni.irtis.repository.database.metrics.transfer;



/**
 *
 * https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity
 */
public class ActivityDTO {
    private Long time;
    private String type;

    public ActivityDTO(Long time, String type) {
        this.time = time;
        this.type = type;
    }

    public Long getTime() {
        return time;
    }

    public String getType() {
        return type;
    }
}
