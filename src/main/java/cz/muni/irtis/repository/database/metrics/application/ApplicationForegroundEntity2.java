package cz.muni.irtis.repository.database.metrics.application;

import cz.muni.irtis.repository.database.metrics.MetricIdentityEntity;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "metrics_applications_foreground_2")
public class ApplicationForegroundEntity2 {
    @EmbeddedId
    private MetricIdentityEntity metricIdentity;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "application_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private ApplicationEntity application;

    public ApplicationForegroundEntity2() {
    }

    public ApplicationForegroundEntity2(MetricIdentityEntity metricIdentity, ApplicationEntity appName) {
        this.metricIdentity = metricIdentity;
        this.application = appName;
    }

    public MetricIdentityEntity getMetricIdentity() {
        return metricIdentity;
    }

    public void setMetricIdentity(MetricIdentityEntity metricIdentity) {
        this.metricIdentity = metricIdentity;
    }

    public ApplicationEntity getApplication() {
        return application;
    }

    public void setApplication(ApplicationEntity application) {
        this.application = application;
    }
}
