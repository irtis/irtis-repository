package cz.muni.irtis.repository.database.identity;

import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.model.persons.Group;
import cz.muni.irtis.repository.model.persons.Person;

import javax.persistence.*;

// TODO: rewrite to Many-to-many
@Entity
@Table(
    name = "people_groups"
)
public class PersonGroupEntity extends EntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;


    public PersonGroupEntity() {
    }

    static public PersonGroupEntity build(Group item) {
        PersonGroupEntity entity = new PersonGroupEntity();
        entity.setId(item.getId());
        entity.setTitle(item.getTitle());
        return entity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
