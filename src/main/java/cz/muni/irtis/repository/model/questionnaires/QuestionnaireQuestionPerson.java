package cz.muni.irtis.repository.model.questionnaires;


import cz.muni.irtis.repository.model.RepositoryInstanceBase;
import cz.muni.irtis.repository.model.persons.Person;


public class QuestionnaireQuestionPerson extends RepositoryInstanceBase {
    private Person person;
    private Questionnaire questionnaire;

    private Long created;       // Time when the survey was created by the administrator or system
    private Long updated;       // Time when the survey was updated by the administrator
    private Long downloaded;    // Time when the client downloaded the survey
    private Long starting;      // Time when the survey can be available to the user
    private Long ending;        // Time when the survey cannot be available to the user
    private Long closing;       // Time when the survey would be open for the user to answer (closing = ([starting, ending] -> NOW() + validity * 60000)

    private Long notified;      // Time when the survey was notified to the user via notification GUI
    private Long silenced;      // Time when the survey was silenced by the user
    private Long opened;        // Time when the survey was opened by the user
    private Long completed;     // Time when the survey was completed by the user

    private Long uploaded;      // Time when the survey was received from the user on the server


    public QuestionnaireQuestionPerson() {
    }

    public QuestionnaireQuestionPerson(Long id) {
        super(id);
    }

    static public QuestionnaireQuestionPerson build() {
        return build(QuestionnaireQuestionPerson.class);
    }

    static public QuestionnaireQuestionPerson build(Long id) {
        return build(id, QuestionnaireQuestionPerson.class);
    }


    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Questionnaire getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(Questionnaire questionnaire) {
        this.questionnaire = questionnaire;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Long getUpdated() {
        return updated;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }

    public Long getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(Long downloaded) {
        this.downloaded = downloaded;
    }

    public Long getStarting() {
        return starting;
    }

    public void setStarting(Long starting) {
        this.starting = starting;
    }

    public Long getEnding() {
        return ending;
    }

    public void setEnding(Long ending) {
        this.ending = ending;
    }

    public Long getClosing() {
        return closing;
    }

    public void setClosing(Long closing) {
        this.closing = closing;
    }

    public Long getNotified() {
        return notified;
    }

    public void setNotified(Long notified) {
        this.notified = notified;
    }

    public boolean isNotified() {
        return notified!=null && notified>0;
    }

    public Long getSilenced() {
        return silenced;
    }

    public void setSilenced(Long silenced) {
        this.silenced = silenced;
    }

    public Long getOpened() {
        return opened;
    }

    public void setOpened(Long opened) {
        this.opened = opened;
    }

    public Long getCompleted() {
        return completed;
    }

    public void setCompleted(Long completed) {
        this.completed = completed;
    }

    public boolean isCompleted() {
        return completed!=null && completed>0;
    }

    public Long getUploaded() {
        return uploaded;
    }

    public void setUploaded(Long uploaded) {
        this.uploaded = uploaded;
    }
}
