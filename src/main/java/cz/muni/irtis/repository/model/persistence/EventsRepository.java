package cz.muni.irtis.repository.model.persistence;

import cz.muni.irtis.repository.database.messages.MessageEntity;
import cz.muni.irtis.repository.database.persistence.EventEntity;
import cz.muni.irtis.repository.model.*;
import cz.muni.irtis.repository.model.messages.Message;
import cz.muni.irtis.repository.model.persons.Person;

import java.util.List;


public class EventsRepository extends RepositoryBase<EventEntity, Event> implements RepositoryStoreable<Event> {
    static public final Integer EVENT_CAPTURING_STARTED = 1;
    static public final Integer EVENT_CAPTURING_STOPPED = 2;
    static public final Integer EVENT_CAPTURING_SCREENSHOTS_STARTED = 3;
    static public final Integer EVENT_CAPTURING_SCREENSHOTS_STOPPED = 4;

    static public final Integer EVENT_QUESTIONNAIRES_NOTIFIED = 5;
    static public final Integer EVENT_QUESTIONNAIRES_POSTPONED = 6;
    static public final Integer EVENT_QUESTIONNAIRES_SILENCED = 7;
    static public final Integer EVENT_QUESTIONNAIRES_DOWNLOADED = 8;
    static public final Integer EVENT_QUESTIONNAIRES_UPLOADED = 9;
    static public final Integer EVENT_QUESTIONNAIRES_OPENED = 10;
    static public final Integer EVENT_QUESTIONNAIRES_COMPLETED = 11;
    static public final Integer EVENT_QUESTIONNAIRES_TEMPLATES_DOWNLOADED = 18;
    static public final Integer EVENT_QUESTIONNAIRES_INVOCATION_ACTIVE = 19;
    static public final Integer EVENT_QUESTIONNAIRES_INVOCATION_INACTIVE = 20;
    static public final Integer EVENT_QUESTIONNAIRES_INVOCATION_EMPTY = 21;
    static public final Integer EVENT_QUESTIONNAIRES_INVOCATION_NOTHINGTOSHOW = 22;

    static public final Integer EVENT_STORAGE_INSUFFICIENT_SPACE = 12;

    static public final Integer EVENT_SYNCHRONIZATION_PROCESSING = 13;

    static public final Integer EVENT_MESSAGES_DOWNLOADED = 14;
    static public final Integer EVENT_MESSAGES_NOTIFIED = 15;
    static public final Integer EVENT_MESSAGES_READ = 16;
    static public final Integer EVENT_MESSAGES_UPLOADED = 17;

    
    public EventsRepository() {
        super(EventEntity.class, Event.class);
    }

    static public EventsRepository build() {
        return new EventsRepository();
    }


    public List<Event> getItemByPerson(Person person) {
        return getItems(
                RepositoryFilter.build()
                    .add("person_id = ?", person.getId()),
                RepositoryOrder.build()
                    .add("time", "DESC"));
    }

    public Event getItemByPersonAndType(Person person, Integer type) {
        List<Event> result = getItems(
                RepositoryFilter.build()
                        .add("identifier = ?", type)
                        .add("person_id = ?", person.getId()),
                RepositoryOrder.build()
                        .add("time", "DESC"),
                RepositoryLimit.build().setCount(1));
        if(result!=null && !result.isEmpty()) {
            return result.get(0);
        } else {
            return null;
        }
    }

    public List<Event> getItemsByPerson(Person person) {
        return getItems(
                RepositoryFilter.build()
                    .add("person_id = ?", person.getId()),
                RepositoryOrder.build()
                    .add("time", "DESC"));
    }

    public List<Event> getItemsByPersonAndType(Person person, Integer type) {
        return getItems(
                RepositoryFilter.build()
                    .add("identifier = ?", type)
                    .add("person_id = ?", person.getId()),
                RepositoryOrder.build()
                    .add("time", "DESC"));
    }


    @Override
    public Event inject(EventEntity entity, Event item) {
        item.setPerson(Person.build(entity.getPerson(), Person.class));
        return item;
    }
}
