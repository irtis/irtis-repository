package cz.muni.irtis.repository.model;

import cz.muni.irtis.repository.database.EntityBase;

/**
 * This interface helps to create additional elements for an instance
 *
 * @param <Entity>
 * @param <Item>
 */
public interface RepositoryCreatable<Entity extends EntityBase, Item extends RepositoryInstanceBase> {
    Item inject(Entity entity, Item item);
}
