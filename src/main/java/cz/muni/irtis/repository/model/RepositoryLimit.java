package cz.muni.irtis.repository.model;

public class RepositoryLimit {
    private Integer offset;
    private Integer count;

    private RepositoryLimit(Integer count, Integer offset) {
        this.offset = offset;
        this.count = count;
    }

    static public RepositoryLimit build() {
        return new RepositoryLimit(null, null);
    }

    static public RepositoryLimit build(Integer count) {
        return new RepositoryLimit(count, null);
    }

    static public RepositoryLimit build(Integer count, Integer offset) {
        return new RepositoryLimit(count, offset);
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getCount() {
        return count;
    }

    public RepositoryLimit setCount(Integer count) {
        this.count = count;
        return this;
    }

    public boolean isEmpty() {
        return count==null && offset==null;
    }

    @Override
    public String toString() {
        return (offset!=null ? offset.toString() + ", " : "") + (count!=null ? count.toString() : "");
    }
}
