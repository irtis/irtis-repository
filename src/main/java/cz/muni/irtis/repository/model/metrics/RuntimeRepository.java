package cz.muni.irtis.repository.model.metrics;

import cz.muni.irtis.repository.database.PersistenceHandler;
import cz.muni.irtis.repository.database.metrics.RuntimeEntity;
import cz.muni.irtis.repository.database.metrics.application.ApplicationEntity;
import cz.muni.irtis.repository.model.*;
import cz.muni.irtis.repository.model.messages.Message;
import cz.muni.irtis.repository.model.persons.Person;
import cz.muni.irtis.repository.model.persons.PersonsRepository;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;


public class RuntimeRepository extends RepositoryBase<RuntimeEntity, Runtime> implements RepositoryStoreable<Runtime> {
    public static final int TYPE_APPLICATION = 1;
    public static final int TYPE_CAPTURE = 2;
    public static final int TYPE_CAPTURE_SCREEN = 3;
    
    public RuntimeRepository() {
        super(RuntimeEntity.class, Runtime.class);
    }

    static public RuntimeRepository build() {
        return new RuntimeRepository();
    }

    public List<Runtime> getItemsByPerson(Person person, RepositoryLimit limit) {
        return getItems(
            RepositoryFilter.build()
                .add("person_id = ?", person.getId()),
            RepositoryOrder.build()
                .add("started", "DESC"),
            limit
        );
    }

    public List<Runtime> getItemsByPersonAndType(Person person, Integer type, RepositoryLimit limit) {
        return this.getItems(
            RepositoryFilter.build()
                .add("type = ?", type)
                .add("person_id = ?", person.getId()),
            RepositoryOrder.build()
                .add("started", "DESC"),
            limit
        );
    }
    
    @Override
    public Runtime inject(RuntimeEntity entity, Runtime item) {
        return item;
    }
}
