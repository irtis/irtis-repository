package cz.muni.irtis.repository.model.persons.game;

public interface BurstCoin {
    Integer getValue();
}
