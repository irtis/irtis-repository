package cz.muni.irtis.repository.model.persons;

import cz.muni.irtis.repository.database.identity.BurstEntity;
import cz.muni.irtis.repository.database.identity.PersonConversationEntity;
import cz.muni.irtis.repository.model.*;

import java.util.List;


public class PersonConversationsRepository extends RepositoryBase<PersonConversationEntity, PersonConversation> implements RepositoryStoreable<PersonConversation> {
    public PersonConversationsRepository() {
        super(PersonConversationEntity.class, PersonConversation.class);
    }

    static public PersonConversationsRepository build() {
        return new PersonConversationsRepository();
    }


    public boolean isExisting(Person person, String owner, String message) {
        List<PersonConversation> result = getItems(
            RepositoryFilter.build()
                .add("person_id = ?", person.getId())
                .add(owner!=null ? "owner = '"+owner+"'" : "owner IS NULL")
                .add("message = ?", message),
            RepositoryLimit.build(1));
        return result!=null && !result.isEmpty();
    }

    @Override
    public PersonConversation inject(PersonConversationEntity entity, PersonConversation item) {
        item.setPerson(Person.build(entity.getPerson(), Person.class));
        return item;
    }

}
