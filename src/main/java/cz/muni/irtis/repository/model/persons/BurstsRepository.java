package cz.muni.irtis.repository.model.persons;

import com.google.gson.Gson;
import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.database.identity.BurstEntity;
import cz.muni.irtis.repository.database.identity.PersonEntity;
import cz.muni.irtis.repository.database.messages.MessageEntity;
import cz.muni.irtis.repository.model.*;
import cz.muni.irtis.repository.model.messages.Message;
import cz.muni.irtis.repository.model.persons.game.BurstCoinBronze;
import cz.muni.irtis.repository.model.persons.game.BurstCoinGold;
import cz.muni.irtis.repository.model.persons.game.BurstCoinSilver;
import cz.muni.irtis.repository.model.persons.game.BurstPurse;
import cz.muni.irtis.repository.model.questionnaires.*;
import cz.muni.irtis.repository.model.utils.Time;
import org.hibernate.Hibernate;
import org.joda.time.DateTime;

import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.List;


public class BurstsRepository extends RepositoryBase<BurstEntity, Burst> implements RepositoryStoreable<Burst> {
    public BurstsRepository() {
        super(BurstEntity.class, Burst.class);
    }

    static public BurstsRepository build() {
        return new BurstsRepository();
    }

    public List<Burst> getItemsByPerson(Person person) {
        List<Burst> result = getItems(
            RepositoryFilter.build()
                .add("person_id = ?", person.getId()),
            RepositoryOrder.build()
                .add("starting", "ASC"));
        return result;
    }

    public Burst getItemRunningByPerson(Person person) {
        List<Burst> result = getItems(
                RepositoryFilter.build()
                    .add("person_id = ?", person.getId())
                    .add("starting <= ?", Time.getTime())
                    .add("ending >= ?", Time.getTime()));
        if(result!=null && !result.isEmpty()) {
            return result.get(0);
        } else {
            return null;
        }
    }

    public boolean isActiveForPerson(Person person) {
        List<Burst> result = getItems(
            RepositoryFilter.build()
                .add("person_id = ?", person.getId())
                .add("starting <= ?", Time.getTime())
                .add("ending >= ?", Time.getTime()),
            RepositoryLimit.build(1));
        return result!=null && !result.isEmpty();
    }

    public List<Burst> getItemsRunning() {
        return getItems(RepositoryFilter.build()
            .add("starting <= ?", Time.getTime())
            .add("ending >= ?", Time.getTime()));
    }

    public List<Burst> getItemsFuture() {
        return getItems(RepositoryFilter.build()
            .add("starting > ?", Time.getTime()));
    }

    public List<Burst> getItemsPast() {
        return getItems(RepositoryFilter.build()
            .add("starting < ?", Time.getTime()));
    }

    public Burst getItemFirstByPerson(Person person) {
        List<Burst> result = getItems(
            RepositoryFilter.build()
                .add("person_id = ?", person.getId()),
            RepositoryOrder.build()
                .add("starting", "ASC"));
        if(result!=null && !result.isEmpty()) {
            return result.get(0);
        } else {
            return null;
        }
    }

    public Burst getItemLastByPerson(Person person) {
        List<Burst> result = getItems(
            RepositoryFilter.build()
                    .add("person_id = ?", person.getId()),
            RepositoryOrder.build()
                .add("starting", "DESC"));
        if(result!=null && !result.isEmpty()) {
            return result.get(0);
        } else {
            return null;
        }
    }

    public Burst getItemNextByPerson(Burst item, Person person) {
        List<Burst> result = getItems(
            RepositoryFilter.build()
                .add("person_id = ?", person.getId())
                .add("starting > ? ", item.getStarting()),
            RepositoryOrder.build()
                .add("starting", "ASC"));
        if(result!=null && !result.isEmpty()) {
            return result.get(0);
        } else {
            return null;
        }
    }

    public BurstPurse getBurstPurse(Person person, Burst item, Long starting, Long ending) {
        QuestionnairesPersonsRepository questionnaires = QuestionnairesPersonsRepository.build();
        BurstPurse purse = BurstPurse.build();
        purse.setPerson(person);

        // Regular type
        for(Long i = starting; i<ending; i+=Time.DAY) {
            Long start = i;
            Long end = i+Time.DAY;

            String times = Time.getTimeFormatted(start) + "("+start.toString()+")" + " - " + Time.getTimeFormatted(end) + "("+end.toString()+")";
            List<QuestionnairePerson> entitiesRegular = questionnaires.getItems(
                RepositoryFilter.build()
                    .add("person_id", person.getId())
                    .add("starting >= ?", start)
                    .add("ending <= ?", end),
                RepositoryOrder.build()
                    .add("starting", "ASC")
            );

            if(entitiesRegular!=null && !entitiesRegular.isEmpty()) {
                int completed = 0;
                int notified = 0;
                int total = 0;
                Iterator<QuestionnairePerson> iEntitiesRegular = entitiesRegular.iterator();
                while (iEntitiesRegular.hasNext()) {
                    QuestionnairePerson entity = iEntitiesRegular.next();
                    if(entity.getQuestionnaire().getType() == QuestionnairesRepository.TYPE_REGULAR) {
                        total++;
                        if (entity.isCompleted()) {
                            boolean isCompletedWithAnswers = false;
                            Long countAnswers = QuestionnairesQuestionsPersonsRepository.build()
                                    .getItemsCountAnswers(person, entity);
                            Long countAnswered = QuestionnairesQuestionsPersonsRepository.build()
                                    .getItemsCountAnswered(person, entity);

                            if (countAnswers == 1) {
                                isCompletedWithAnswers = countAnswered == 1;
                            } else if (countAnswers == 2) {
                                isCompletedWithAnswers = countAnswered >= 1;
                            } else {
                                float percent = (Float.valueOf(countAnswered) / Float.valueOf(countAnswers)) * 100;
                                isCompletedWithAnswers = percent >= 30.0f;
                            }

                            if (isCompletedWithAnswers) {
                                completed++;
                                purse.put(BurstCoinBronze.build());
                            }
                        }

                        if (entity.isNotified()) {
                            notified++;
                        }
                    }
                }

                if(completed!=0) {
                    if (completed >= (total - (total / 4))) { // 75% completed questionnaires
                        purse.put(BurstCoinSilver.build());
                    }
                    if (completed == total) { // 100% completed questionnaires
                        purse.put(BurstCoinGold.build());
                    }
                }

                purse.setNotified(purse.getNotified()!=null ? purse.getNotified() + notified : notified);
                purse.setCompleted(purse.getCompleted()!=null ? purse.getCompleted() + completed : completed);
                purse.setTotal(purse.getTotal()!=null ? purse.getTotal() + total : total);
            }
        }

        // Special type
        List<Burst> entitiesFollowingBurst = getItems(
            RepositoryFilter.build()
                    .add("person_id", person.getId())
                    .add("starting > ?", ending),
            RepositoryOrder.build()
                    .add("starting", "ASC")
        );
        if(entitiesFollowingBurst!=null && !entitiesFollowingBurst.isEmpty()) {
            ending = entitiesFollowingBurst.get(0).getStarting();
        } else {
            ending += Time.WEEK;
        }

        List<QuestionnairePerson> entitiesSpecial = questionnaires.getItems(
            RepositoryFilter.build()
                .add("person_id", person.getId())
                .add("starting >= ?", starting)
                .add("ending <= ?", ending),
            RepositoryOrder.build()
                .add("starting", "ASC")
        );
        if (entitiesSpecial != null && !entitiesSpecial.isEmpty()) {
            Iterator<QuestionnairePerson> iEntitiesSpecial = entitiesSpecial.iterator();
            while (iEntitiesSpecial.hasNext()) {
                QuestionnairePerson entity = iEntitiesSpecial.next();
                if(entity.getQuestionnaire().getType() == QuestionnairesRepository.TYPE_SPECIAL && entity.isCompleted()) {
                    boolean isCompletedWithAnswers = false;
                    Long countAnswers = QuestionnairesQuestionsPersonsRepository.build()
                            .getItemsCountAnswers(person, entity);
                    Long countAnswered = QuestionnairesQuestionsPersonsRepository.build()
                            .getItemsCountAnswered(person, entity);

                    if (countAnswers == 1) {
                        isCompletedWithAnswers = countAnswered == 1;
                    } else if (countAnswers == 2) {
                        isCompletedWithAnswers = countAnswered >= 1;
                    } else {
                        float percent = (Float.valueOf(countAnswered) / Float.valueOf(countAnswers)) * 100;
                        isCompletedWithAnswers = percent >= 30.0f;
                    }

                    if (isCompletedWithAnswers) {
                        purse.put(BurstCoinGold.build());
                    }
                }
            }
        }

        return purse;
    }


    public Integer getBurstPaceCurrent(Person person, Burst item) {
        QuestionnairesPersonsRepository questionnaires = QuestionnairesPersonsRepository.build();

        Long time = 0L;
        if(item.isActive()) {
            time = Time.getTime();
        } else if(item.getEnding() < Time.getTime()) {
            time = item.getEnding();
        } else if(item.getStarting() > Time.getTime()) {
            time = item.getStarting();
        }

        Long starting = new DateTime(time).withTimeAtStartOfDay().getMillis()-Time.DAY*2;
        Long ending = new DateTime(time).withTimeAtStartOfDay().getMillis()+Time.DAY;

        List<QuestionnairePerson> entitiesRegular = questionnaires.getItems(
            RepositoryFilter.build()
                .add("person_id", person.getId())
                .add("starting >= ?", starting)
                .add("ending <= ?", ending),
            RepositoryOrder.build()
                .add("starting", "ASC")
        );

        if(entitiesRegular!=null && !entitiesRegular.isEmpty()) {
            int completed = 0;
            int count = entitiesRegular.size();
            Iterator<QuestionnairePerson> iEntities = entitiesRegular.iterator();
            while (iEntities.hasNext()) {
                QuestionnairePerson entity = iEntities.next();
                if(entity.getQuestionnaire().getType() == QuestionnairesRepository.TYPE_REGULAR && entity.isCompleted()) {
                    boolean isCompletedWithAnswers = false;
                    Long countAnswers = QuestionnairesQuestionsPersonsRepository.build()
                            .getItemsCountAnswers(person, entity);
                    Long countAnswered = QuestionnairesQuestionsPersonsRepository.build()
                            .getItemsCountAnswered(person, entity);

                    if(countAnswers == 1) {
                        isCompletedWithAnswers = countAnswered == 1;
                    } else if(countAnswers == 2) {
                        isCompletedWithAnswers = countAnswered >= 1;
                    } else {
                        isCompletedWithAnswers = countAnswered >= countAnswers / 3;
                    }

                    if(isCompletedWithAnswers) {
                        completed++;
                    }
                }
            }

            float percent = (Float.valueOf(completed) / Float.valueOf(count)) * 100;
            return (int) percent;
        } else {
            return 0;
        }
    }

    public Integer getBurstPaceAverage(Person person, Burst item) {
        QuestionnairesPersonsRepository questionnaires = QuestionnairesPersonsRepository.build();
        Long starting = new DateTime(item.getStarting()).withTimeAtStartOfDay().getMillis()-Time.DAY*2;
        Long ending = new DateTime(item.getEnding()).withTimeAtStartOfDay().getMillis()+Time.DAY;

        List<QuestionnairePerson> entitiesRegular = questionnaires.getItems(
            RepositoryFilter.build()
                    .add("person_id", person.getId())
                    .add("starting >= ?", starting)
                    .add("ending <= ?", ending),
            RepositoryOrder.build()
                    .add("starting", "ASC")
        );

        if(entitiesRegular!=null && !entitiesRegular.isEmpty()) {
            int completed = 0;
            int count = entitiesRegular.size();
            Iterator<QuestionnairePerson> iEntities = entitiesRegular.iterator();
            while (iEntities.hasNext()) {
                QuestionnairePerson entity = iEntities.next();
                if(entity.getQuestionnaire().getType() == QuestionnairesRepository.TYPE_REGULAR && entity.isCompleted()) {
                    boolean isCompletedWithAnswers = false;
                    Long countAnswers = QuestionnairesQuestionsPersonsRepository.build()
                            .getItemsCountAnswers(person, entity);
                    Long countAnswered = QuestionnairesQuestionsPersonsRepository.build()
                            .getItemsCountAnswered(person, entity);

                    if(countAnswers == 1) {
                        isCompletedWithAnswers = countAnswered == 1;
                    } else if(countAnswers == 2) {
                        isCompletedWithAnswers = countAnswered >= 1;
                    } else {
                        isCompletedWithAnswers = countAnswered >= countAnswers / 3;
                    }

                    if(isCompletedWithAnswers) {
                        completed++;
                    }
                }
            }

            float percent = (Float.valueOf(completed) / Float.valueOf(count)) * 100;
            return (int) percent;
        } else {
            return 0;
        }
    }

    public Integer getBurstPacePrevious(Person person, Burst item) {
        Long starting = new DateTime(item.getStarting()).withTimeAtStartOfDay().getMillis()-Time.DAY*2;
        Long ending = new DateTime(item.getEnding()).withTimeAtStartOfDay().getMillis()+Time.DAY;

        List<Burst> entities = getItems(
            RepositoryFilter.build()
                .add("person_id", person.getId())
                .add("starting < ?", item.getStarting()),
            RepositoryOrder.build()
                .add("starting", "ASC")
        );

        if (entities != null && !entities.isEmpty()) {
            Burst entity = entities.get(0);
            return getBurstPaceAverage(person, entity);
        } else {
            return null;
        }
    }


    @Override
    public Burst inject(BurstEntity entity, Burst item) {
        item.setPerson(Person.build(entity.getPerson(), Person.class));
        return item;
    }

}
