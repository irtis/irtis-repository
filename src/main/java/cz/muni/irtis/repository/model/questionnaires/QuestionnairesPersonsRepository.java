package cz.muni.irtis.repository.model.questionnaires;

import cz.muni.irtis.repository.database.PersistenceHandler;
import cz.muni.irtis.repository.database.questionnaires.PersonQuestionnaireEntity;
import cz.muni.irtis.repository.database.questionnaires.QuestionnaireEntity;
import cz.muni.irtis.repository.model.*;
import cz.muni.irtis.repository.model.persons.Person;
import cz.muni.irtis.repository.model.utils.Time;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class QuestionnairesPersonsRepository extends RepositoryBase<PersonQuestionnaireEntity, QuestionnairePerson> implements RepositoryStoreable<QuestionnairePerson> {

    public QuestionnairesPersonsRepository() {
        super(PersonQuestionnaireEntity.class, QuestionnairePerson.class);
    }

    static public QuestionnairesPersonsRepository build() {
        return new QuestionnairesPersonsRepository();
    }

//    @Override
//    public List<QuestionnairePerson> getItems(RepositoryFilter filter, RepositoryOrder order, RepositoryLimit limit) {
//        Session session = PersistenceHandler.build().getSession();
//        List<QuestionnairePerson> items = null;
//        Transaction transaction = null;
//        try {
//            transaction = session.beginTransaction();
//            items = new ArrayList<>();
//            Class<?> typeEntityClass = Class.forName(PersonQuestionnaireEntity.class.getTypeName());
//            Class<?> typeItemClass = Class.forName(QuestionnairePerson.class.getTypeName());
//
//            String sql = "SELECT " + PersonQuestionnaireEntity.class.getSimpleName().substring(0, 1).toLowerCase() + " " +
//                    "FROM " + PersonQuestionnaireEntity.class.getTypeName() + " " + PersonQuestionnaireEntity.class.getSimpleName().substring(0, 1).toLowerCase() + " " +
//                    "JOIN " + QuestionnaireEntity.class.getTypeName() + " " + QuestionnaireEntity.class.getSimpleName().substring(0, 1).toLowerCase() + " ON  " +
//                        QuestionnaireEntity.class.getSimpleName().substring(0, 1).toLowerCase() + ".id = " + PersonQuestionnaireEntity.class.getSimpleName().substring(0, 1).toLowerCase() + ".questionnaire_id " +
//                    (filter!=null && !filter.isEmpty() ? "WHERE " + filter.toString() + " " : "") +
//                    (order!=null && !order.isEmpty() ? "ORDER BY " + order.toString() + " " : "");
//
//            Query query = session.createQuery(sql, typeEntityClass);
////            System.out.println(query.getQueryString());
//
//            if(limit!=null) {
//                if(limit.getOffset()!=null) {
//                    query.setFirstResult(limit.getOffset());
//                }
//                if(limit.getCount()!=null) {
//                    query.setMaxResults(limit.getCount());
//                }
//            }
//
//            List<PersonQuestionnaireEntity> entities = query.list();
//            items = infalte(entities);
//
//            transaction.commit();
//        } catch (Exception e) {
//            if (transaction!=null) {
//                transaction.rollback();
//            }
//            items = null;
//            e.printStackTrace();
//        } finally {
//            session.close();
//            return items;
//        }
//    }


    public List<QuestionnairePerson> getItemsAvailable(Person person) {
        return getItems(
            RepositoryFilter.build()
                .add("person_id = ?", person.getId())
                .add("(starting >= ? OR starting IS NULL)", Time.getTime())
                .add("uploaded IS NULL"),
            RepositoryOrder.build()
                .add("created", "DESC")
        );
    }

    public List<QuestionnairePerson> getItemsUploaded(Person person) {
        return getItems(
            RepositoryFilter.build()
                .add("person_id = ?", person.getId())
                .add("uploaded IS NOT NULL"),
            RepositoryOrder.build()
                .add("starting", "DESC")
        );
    }

    public List<QuestionnairePerson> getItemsUploadedByTime(Person person, Long tStarting, Long tEnding) {
        return getItems(
            RepositoryFilter.build()
                .add("person_id = ?", person.getId())
                .add("uploaded IS NOT NULL")
                .add("uploaded >= ?", tStarting)
                .add("uploaded <= ?", tEnding),
            RepositoryOrder.build()
                .add("starting", "DESC")
        );
    }

    public List<QuestionnairePerson> getItemsScheduledByTime(Person person, Long tStarting, Long tEnding) {
        return getItems(
            RepositoryFilter.build()
                .add("person_id = ?", person.getId())
                .add("starting >= ?", tStarting)
                .add("starting <= ?", tEnding),
            RepositoryOrder.build()
                .add("starting", "DESC")
        );
    }

    public List<QuestionnairePerson> getItemsStarted(Person person) {
        return getItems(
                RepositoryFilter.build()
                    .add("person_id = ?", person.getId())
                    .add("uploaded IS NOT NULL"),
                RepositoryOrder.build()
                    .add("starting", "DESC")
        );
    }

    @Override
    public QuestionnairePerson inject(PersonQuestionnaireEntity entity, QuestionnairePerson item) {
        item.setQuestionnaire(Questionnaire
                .build(entity.getQuestionnaire(), Questionnaire.class));
        return item;
    }
}
