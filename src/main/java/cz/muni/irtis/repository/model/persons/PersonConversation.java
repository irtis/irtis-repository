package cz.muni.irtis.repository.model.persons;

import cz.muni.irtis.repository.model.RepositoryInstanceBase;

public class PersonConversation extends RepositoryInstanceBase {
    private Person person;
    private Long time;
    private String title;
    private String owner;
    private String message;
    private String file;
    private Float execution;
    private Float confidence;

    public PersonConversation() {
    }

    public PersonConversation(Long id) {
        super(id);
    }

    static public PersonConversation build() {
        return build(PersonConversation.class);
    }

    static public PersonConversation build(Long id) {
        return build(id, PersonConversation.class);
    }

    public Person getPerson() {
        return person;
    }

    public PersonConversation setPerson(Person person) {
        this.person = person;
        return this;
    }

    public Long getTime() {
        return time;
    }

    public PersonConversation setTime(Long time) {
        this.time = time;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public PersonConversation setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getOwner() {
        return owner;
    }

    public PersonConversation setOwner(String owner) {
        this.owner = owner;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public PersonConversation setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getFile() {
        return file;
    }

    public PersonConversation setFile(String file) {
        this.file = file;
        return this;
    }

    public Float getExecution() {
        return execution;
    }

    public PersonConversation setExecution(Float execution) {
        this.execution = execution;
        return this;
    }

    public Float getConfidence() {
        return confidence;
    }

    public PersonConversation setConfidence(Float confidence) {
        this.confidence = confidence;
        return this;
    }
}
