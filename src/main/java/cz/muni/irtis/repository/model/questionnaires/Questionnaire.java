package cz.muni.irtis.repository.model.questionnaires;

import cz.muni.irtis.repository.model.RepositoryInstanceBase;
import cz.muni.irtis.repository.model.persons.Person;

import javax.persistence.Column;

public class Questionnaire extends RepositoryInstanceBase {
    private Integer type;
    private String identifier;
    private String title;
    private Integer validity;
    private Integer silenceDelay;
    private String group;
    private Long created;
    private Long updated;
    private String tag;
    private Boolean configurable;


    public Questionnaire() {
    }

    public Questionnaire(Long id) {
        super(id);
    }

    static public Questionnaire build() {
        return build(Questionnaire.class);
    }

    static public Questionnaire build(Long id) {
        return build(id, Questionnaire.class);
    }

    public Integer getType() {
        return type;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getTitle() {
        return title;
    }

    public Integer getValidity() {
        return validity;
    }

    public Integer getSilenceDelay() {
        return silenceDelay;
    }

    public String getGroup() {
        return group;
    }

    public Long getCreated() {
        return created;
    }

    public Long getUpdated() {
        return updated;
    }

    public String getTag() {
        return tag;
    }

    public Boolean getConfigurable() {
        return configurable;
    }
}
