package cz.muni.irtis.repository.model.metrics;

import cz.muni.irtis.repository.database.PersistenceConfigLoader;
import cz.muni.irtis.repository.database.PersistenceHandler;
import cz.muni.irtis.repository.database.metrics.TimeEntity;
import cz.muni.irtis.repository.database.metrics.screen.ScreenshotEntity;
import cz.muni.irtis.repository.database.metrics.screen.ScreenshotApplicationObject;
import cz.muni.irtis.repository.model.*;
import cz.muni.irtis.repository.model.persons.Person;
import cz.muni.irtis.repository.model.persons.PersonsRepository;
import cz.muni.irtis.repository.model.utils.Time;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Table;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ScreenshotsRepository extends RepositoryBase<ScreenshotEntity, Screenshot> implements RepositoryStoreable<Screenshot> {

    public ScreenshotsRepository() {
        super(ScreenshotEntity.class, Screenshot.class);
    }

    static public ScreenshotsRepository build() {
        return new ScreenshotsRepository();
    }

    public List<Screenshot> getItemsWithApplication(RepositoryLimit limit) {
        Session session = PersistenceHandler.build().getSession();
        List<Screenshot> items = null;
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            List<ScreenshotApplicationObject> results = session
                .createSQLQuery("" +
                    "SELECT screenshots.id_screenshots, screenshots.time, applications.name, screenshots.filename, screenshots.person_id, screenshots.metadata " +
                    "FROM " +
                        "( " +
                            "SELECT mt.time/1000 as \"time\", applications.name, foreground.person_id " +
                            "FROM " +
                            "metrics_time mt, " +
                            "metrics_applications_foreground foreground, " +
                            "metrics_applications applications " +
                            "WHERE " +
                            "foreground.datetime_id = mt.id " +
                            "AND applications.id = foreground.application_id " +
                            "ORDER BY mt.time desc " +
                            (limit!=null && !limit.isEmpty() ? "LIMIT " + limit.toString() + " " : "") +
                        ") as applications, " +
                        "( " +
                            "SELECT mt.time/1000 as \"time\", screenshots.datetime_id as id_screenshots, screenshots.filename, screenshots.metadata, screenshots.person_id " +
                            "FROM " +
                            "metrics_time mt, " +
                            "metrics_screenshots screenshots " +
                            "WHERE " +
                            "screenshots.datetime_id = mt.id " +
                            "ORDER BY mt.time desc " +
                            (limit!=null && !limit.isEmpty() ? "LIMIT " + limit.toString() + " " : "") +
                        ") as screenshots " +
                    "WHERE applications.time = screenshots.time " +
                    "AND applications.person_id = screenshots.person_id " +
                    (limit!=null && !limit.isEmpty() ? "LIMIT " + limit.toString() + " " : "")
                )
                .setResultTransformer(ScreenshotApplicationObject.buildResultTransformer())
                .getResultList();
            transaction.commit();

            System.out.println("" +
                    "SELECT screenshots.id_screenshots, screenshots.time, applications.name, screenshots.filename, screenshots.person_id, screenshots.metadata " +
                    "FROM " +
                    "( " +
                    "SELECT mt.time/1000 as \"time\", applications.name, foreground.person_id " +
                    "FROM " +
                    "metrics_time mt, " +
                    "metrics_applications_foreground foreground, " +
                    "metrics_applications applications " +
                    "WHERE " +
                    "foreground.datetime_id = mt.id " +
                    "AND applications.id = foreground.application_id " +
                    "ORDER BY mt.time desc " +
                    (limit!=null && !limit.isEmpty() ? "LIMIT " + limit.toString() + " " : "") +
                    ") as applications, " +
                    "( " +
                    "SELECT mt.time/1000 as \"time\", screenshots.datetime_id as id_screenshots, screenshots.filename, screenshots.metadata, screenshots.person_id " +
                    "FROM " +
                    "metrics_time mt, " +
                    "metrics_screenshots screenshots " +
                    "WHERE " +
                    "screenshots.datetime_id = mt.id " +
                    "ORDER BY mt.time desc " +
                    (limit!=null && !limit.isEmpty() ? "LIMIT " + limit.toString() + " " : "") +
                    ") as screenshots " +
                    "WHERE applications.time = screenshots.time " +
                    "AND applications.person_id = screenshots.person_id " +
                    (limit!=null && !limit.isEmpty() ? "LIMIT " + limit.toString() + " " : ""));

            if(results!=null && !results.isEmpty()) {
                Iterator<ScreenshotApplicationObject> iResults = results.iterator();
                while (iResults.hasNext()) {
                    ScreenshotApplicationObject result = iResults.next();
                    Screenshot item = Screenshot.build();
                    item.setFile(new File(result.getFilename()));
                    item.setTime(result.getTime());

                    Application application = Application.build();
                    application.setId(result.getApplicationId());
                    application.setIdentifier(result.getApplicationName());
                    item.setApplication(application);

                    items.add(item);
                }
            }
        } catch (Exception e) {
            if (transaction!=null) {
                transaction.rollback();
            }
            items = null;
            e.printStackTrace();
        } finally {
            session.close();
            return items;
        }
    }


    public List<Screenshot> getItems(RepositoryFilter filter, RepositoryLimit limit) {
        Session session = PersistenceHandler.build().getSession();
        List<Screenshot> items = null;
        Transaction transaction = null;

        try {
            transaction = session.beginTransaction();
            List<ScreenshotApplicationObject> results = session
                .createSQLQuery("" +
                    "SELECT screenshots.datetime_id, screenshots.cached_time, applications.id, applications.name, screenshots.filename, screenshots.person_id, screenshots.metadata " +
                    "FROM metrics_applications applications, metrics_applications_foreground foreground " +
                    "JOIN metrics_screenshots screenshots ON foreground.cached_time = screenshots.cached_time " +
                    "WHERE applications.id = foreground.application_id " +
                    (filter!=null && !filter.isEmpty() ? " AND " + filter.toString() + " " : "") +
                    "ORDER BY foreground.cached_time ASC " +
                    (limit!=null && !limit.isEmpty() ? "LIMIT " + limit.toString() + " " : "")
                )
                .setResultTransformer(ScreenshotApplicationObject.buildResultTransformer())
                .getResultList();
            transaction.commit();

            if(results!=null && !results.isEmpty()) {
                Iterator<ScreenshotApplicationObject> iResults = results.iterator();
                items = new ArrayList<>();
                while (iResults.hasNext()) {
                    ScreenshotApplicationObject result = iResults.next();
                    Screenshot item = Screenshot.build();
                    item.setPerson(Person.build(result.getPersonId()));
                    File file = new File("../irtis-server/data/users/"+result.getPersonId().toString()+"/screenshots/"+result.getFilename());
                    if(!file.exists()) {
                        file = new File("data/users/"+result.getPersonId().toString()+"/screenshots/"+result.getFilename());
                    }
                    item.setFile(file);
                    item.setMetadata(result.getMetadata());
                    item.setTime(result.getTime());

                    Application application = Application.build();
                    application.setId(result.getApplicationId());
                    application.setIdentifier(result.getApplicationName());
                    item.setApplication(application);

                    item.setId(result.getId());

                    items.add(item);
                }
            }
        } catch (Exception e) {
            if (transaction!=null) {
                transaction.rollback();
            }
            items = null;
            e.printStackTrace();
        } finally {
            session.close();
            return items;
        }
    }

    public List<Screenshot> getItemsByPerson(Person person, RepositoryLimit limit) {
        return getItems(
            RepositoryFilter.build()
                .add("foreground.person_id", person.getId())
                .add("screenshots.person_id", person.getId()),
            limit);
    }

    public List<Screenshot> getItemsByPersonAndApplication(Person person, String identifier, RepositoryLimit limit) {
        return getItems(
            RepositoryFilter.build()
                .add("applications.name", identifier)
                .add("foreground.person_id", person.getId())
                .add("screenshots.person_id", person.getId()),
            limit);
    }

    public List<Screenshot> getItemsByApplication(String identifier, RepositoryLimit limit) {
        return getItems(
            RepositoryFilter.build()
                .add("applications.name", identifier),
            limit);
    }


    @Override
    public Screenshot inject(ScreenshotEntity entity, Screenshot item) {
        item.setFile(new File(entity.getFilename()));
//        item.setApplication(Application.build(entity.get(), Application.class));
//        if(entity.get)
        item.setTime(entity.getMetricIdentity().getDatetimeId());
        return item;
    }
}
