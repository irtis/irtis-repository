package cz.muni.irtis.repository.model.persons;

public class PersonDeviceCount {
    private String device;
    private Long count;

    public PersonDeviceCount(String device, Long count) {
        this.device = device;
        this.count = count;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
