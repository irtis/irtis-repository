package cz.muni.irtis.repository.model;

import java.util.List;

/**
 * Storeable
 *
 * Interface for Repository models
 */
public interface RepositoryStoreable<I extends RepositoryInstanceBase> {
    /**
     * store()
     *
     * Store an item into the repository
     *
     * @param item
     */
    void store(I item);

    /**
     * delete()
     *
     * Remove an item from the repository
     *
     * @param item
     */
    void delete(I item);

    /**
     * getItems()
     *
     * Get list of items from the repository
     *
     * @param filter
     * @param order
     * @param limit
     * @return ArrayList<I>
     */
    List<I> getItems(RepositoryFilter filter, RepositoryOrder order, RepositoryLimit limit);

    /**
     * getItemsCount()
     *
     * Get number of items
     *
     * @param filter
     * @return
     */
    Long getItemsCount(RepositoryFilter filter);

    /**
     * getItem()
     *
     * Get an item by the identifier from the repository
     *
     * @param item
     * @return I
     */
    I getItem(I item);
}
