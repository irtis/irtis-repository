package cz.muni.irtis.repository.model.persons.game;

public class BurstCoinBronze implements BurstCoin {

    static public BurstCoinBronze build() {
        return new BurstCoinBronze();
    }

    @Override
    public Integer getValue() {
        return 1;
    }
}
