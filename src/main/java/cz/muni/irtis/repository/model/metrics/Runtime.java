package cz.muni.irtis.repository.model.metrics;

import cz.muni.irtis.repository.model.RepositoryInstanceBase;

public class Runtime extends RepositoryInstanceBase {
    private Long started;
    private Long stopped;
    private Integer type;


    public Runtime() {
    }

    public Runtime(Long id) {
        super(id);
    }

    static public Runtime build() {
        return build(Runtime.class);
    }

    static public Runtime build(Long id) {
        return build(id, Runtime.class);
    }

    public Long getStarted() {
        return started;
    }

    public void setStarted(Long started) {
        this.started = started;
    }

    public Long getStopped() {
        return stopped;
    }

    public void setStopped(Long stopped) {
        this.stopped = stopped;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
