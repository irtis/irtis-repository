package cz.muni.irtis.repository.model.access;

import cz.muni.irtis.repository.database.access.AccessEntity;
import cz.muni.irtis.repository.database.access.ApiEntity;
import cz.muni.irtis.repository.model.RepositoryBase;
import cz.muni.irtis.repository.model.RepositoryFilter;
import cz.muni.irtis.repository.model.RepositoryOrder;
import cz.muni.irtis.repository.model.RepositoryStoreable;
import cz.muni.irtis.repository.model.messages.Message;
import cz.muni.irtis.repository.model.persons.Person;

import java.util.List;


public class ApiRepository extends RepositoryBase<ApiEntity, Api> implements RepositoryStoreable<Api> {

    public ApiRepository() {
        super(ApiEntity.class, Api.class);
    }

    static public ApiRepository build() {
        return new ApiRepository();
    }

    public Api getItemByIdentifier(String identifier) {
        List<Api> result = getItems(
            RepositoryFilter.build()
                .add("identifier = ?", identifier));
        if(result!=null && !result.isEmpty()) {
            return result.get(0);
        } else {
            return null;
        }
    }

    @Override
    public Api inject(ApiEntity entity, Api item) {
        return item;
    }
}
