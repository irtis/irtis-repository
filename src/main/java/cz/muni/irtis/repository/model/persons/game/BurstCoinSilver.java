package cz.muni.irtis.repository.model.persons.game;

public class BurstCoinSilver implements BurstCoin {

    static public BurstCoinSilver build() {
        return new BurstCoinSilver();
    }

    @Override
    public Integer getValue() {
        return 5;
    }
}
