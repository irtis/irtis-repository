package cz.muni.irtis.repository.model.messages;

import cz.muni.irtis.repository.model.RepositoryInstanceBase;
import cz.muni.irtis.repository.model.persons.Person;

public class Message extends RepositoryInstanceBase {
    private Person person;
    private Person creator;
    private Integer type;
    private String text;
    private String response;
    private Long created;
    private Long updated;
    private Long validFrom;
    private Long validTo;
    private Long downloaded;
    private Long notified;
    private Long opened;
    private Long read;
    private Long uploaded;

    public Message() {
    }

    public Message(Long id) {
        super(id);
    }

    static public Message build() {
        return build(Message.class);
    }

    static public Message build(Long id) {
        return build(id, Message.class);
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Person getCreator() {
        return creator;
    }

    public void setCreator(Person creator) {
        this.creator = creator;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Long getUpdated() {
        return updated;
    }

    public void setUpdated(Long updated) {
        this.updated = updated;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public Long getValidTo() {
        return validTo;
    }

    public void setValidTo(Long validTo) {
        this.validTo = validTo;
    }

    public Long getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(Long downloaded) {
        this.downloaded = downloaded;
    }

    public Long getNotified() {
        return notified;
    }

    public void setNotified(Long notified) {
        this.notified = notified;
    }

    public Long getOpened() {
        return opened;
    }

    public void setOpened(Long opened) {
        this.opened = opened;
    }

    public Long getRead() {
        return read;
    }

    public void setRead(Long read) {
        this.read = read;
    }

    public Long getUploaded() {
        return uploaded;
    }

    public void setUploaded(Long uploaded) {
        this.uploaded = uploaded;
    }
}
