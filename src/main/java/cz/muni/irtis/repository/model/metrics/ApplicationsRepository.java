package cz.muni.irtis.repository.model.metrics;

import cz.muni.irtis.repository.database.messages.MessageEntity;
import cz.muni.irtis.repository.database.metrics.application.ApplicationEntity;
import cz.muni.irtis.repository.model.RepositoryBase;
import cz.muni.irtis.repository.model.RepositoryFilter;
import cz.muni.irtis.repository.model.RepositoryOrder;
import cz.muni.irtis.repository.model.RepositoryStoreable;
import cz.muni.irtis.repository.model.messages.Message;
import cz.muni.irtis.repository.model.persons.Person;

import java.util.List;


public class ApplicationsRepository extends RepositoryBase<ApplicationEntity, Application> implements RepositoryStoreable<Application> {

    public ApplicationsRepository() {
        super(ApplicationEntity.class, Application.class);
    }


    @Override
    public Application inject(ApplicationEntity entity, Application item) {
        return item;
    }
}
