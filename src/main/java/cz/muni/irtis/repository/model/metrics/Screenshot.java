package cz.muni.irtis.repository.model.metrics;

import cz.muni.irtis.repository.model.RepositoryInstanceBase;
import cz.muni.irtis.repository.model.persons.Person;

import java.io.File;

public class Screenshot extends RepositoryInstanceBase {
    private Person person;
    private Application application;
    private File file;
    private String metadata;
    private Long time;
    private Boolean processed;

    public Screenshot() {
    }

    public Screenshot(Long id) {
        super(id);
    }

    static public Screenshot build() {
        return build(Screenshot.class);
    }

    static public Screenshot build(Long id) {
        return build(id, Screenshot.class);
    }


    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Boolean getProcessed() {
        return processed;
    }

    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }
}
