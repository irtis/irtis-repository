package cz.muni.irtis.repository.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

// TODO: check security against SQl injection
public class RepositoryFilter {
    private Map<String, String> items;

    private RepositoryFilter() {
        this.items = new HashMap<>();
    }

    static public RepositoryFilter build() {
        return new RepositoryFilter();
    }

    public RepositoryFilter add(String variable, String value, String conjunction) {
        if(variable.contains("?")) {
            items.put(variable.replace("?", !isStringNumeric(value) ? "'"+value+"'" : value), conjunction);
        } else if(value!=null && !value.isEmpty()) {
            items.put(variable + " = " + (!isStringNumeric(value) ? "'"+value+"'" : value), conjunction);
        } else {
            items.put(variable, conjunction);
        }
        return this;
    }

    public RepositoryFilter add(String variable, String value) {
        return add(variable, value, "AND");
    }

    public RepositoryFilter add(String variable, Integer value) {
        return add(variable, value.toString(), "AND");
    }

    public RepositoryFilter add(String variable, Long value) {
        return add(variable, value.toString(), "AND");
    }

    public RepositoryFilter add(String variable) {
        return add(variable, null,"AND");
    }

    public boolean isEmpty() {
        return items!=null && items.isEmpty();
    }

    private boolean isStringNumeric(String string) {
        Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
        if (string == null) {
            return false;
        }
        return pattern.matcher(string).matches();
    }

    @Override
    public String toString() {
        if(!isEmpty()) {
            StringBuilder output = new StringBuilder();
            Iterator<Map.Entry<String, String>> iItems = items.entrySet().iterator();
            while (iItems.hasNext()) {
                Map.Entry<String, String> entry = iItems.next();
                output.append((output.length() > 0 ? " " + entry.getValue().toUpperCase() + " " : "") + entry.getKey());
            }
            return output.toString();
        } else {
            return null;
        }
    }
}
