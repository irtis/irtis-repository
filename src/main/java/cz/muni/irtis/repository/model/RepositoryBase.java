package cz.muni.irtis.repository.model;

import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.database.PersistenceHandler;
import cz.muni.irtis.repository.database.identity.PersonEntity;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.LazyInitializer;
import org.hibernate.query.Query;

import javax.persistence.Table;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * RepositoryBase
 *
 * Please, use The Hibernate Query Language (HQL)
 *  - https://docs.jboss.org/hibernate/orm/5.2/userguide/html_single/Hibernate_User_Guide.html#hql
 *  - https://docs.jboss.org/hibernate/orm/3.6/reference/en-US/html/queryhql.html (deprecated)
 *
 * Asynchronicity
 *  - Swing Worker: https://docs.oracle.com/javase/8/docs/api/index.html?javax/swing/SwingWorker.html
 *  - Java Executors: http://tutorials.jenkov.com/java-util-concurrent/executorservice.html
 */
abstract public class RepositoryBase<Entity extends EntityBase, Item extends RepositoryInstanceBase>
        implements RepositoryStoreable<Item>,
        RepositoryCreatable<Entity, Item> {
    private static ExecutorService executor = null;
    private List<RepositoryTask> running;

    private Type typeEntity;
    private Type typeItem;


    public interface RepositoryTaskListener<T> {
        void onStart();
        T onTask();
        void onDone(T result);
    }


    public RepositoryBase(Type typeEntity, Type typeItem) {
        this.running = new ArrayList<>();
        this.typeEntity = typeEntity;
        this.typeItem = typeItem;
    }

    public Session getDatabase() {
        return PersistenceHandler.build().getSession();
    }


    @Override
    public void store(Item item) {
        Session session = PersistenceHandler.build().getSession();
        Transaction transaction = session.beginTransaction();
        try {
            Class<?> typeEntityClass = Class.forName(typeEntity.getTypeName());
            Class<?> typeItemClass = Class.forName(typeItem.getTypeName());
            Method methodBuild = typeEntityClass.getMethod("build", typeItemClass);
            Entity entity = (Entity) methodBuild.invoke(this, item);
            if(entity!=null) {
                session.saveOrUpdate(entity);
            }
            transaction.commit();
        } catch (Exception e) {
            if (transaction!=null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        session.close();
    }

    @Override
    public void delete(Item item) {
        Session session = PersistenceHandler.build().getSession();
        Transaction transaction = session.beginTransaction();
        try {
            Class<?> typeEntityClass = Class.forName(typeEntity.getTypeName());
            Class<?> typeItemClass = Class.forName(typeItem.getTypeName());
            Method methodBuild = typeEntityClass.getMethod("build", typeItemClass);
            Entity entity = (Entity) methodBuild.invoke(this, item);
            if(entity!=null) {
                session.delete(entity);
            }
            transaction.commit();
        } catch (Exception e) {
            if (transaction!=null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        session.close();
    }

    @Override
    public Item getItem(Item item) {
        Session session = PersistenceHandler.build().getSession();
        Transaction transaction = session.beginTransaction();
        try {
            Class<?> typeEntityClass = Class.forName(typeEntity.getTypeName());
            Class<?> typeItemClass = Class.forName(typeItem.getTypeName());
            Method methodBuild = typeEntityClass.getMethod("build", typeItemClass);
            Entity entity = (Entity) methodBuild.invoke(this, item);
            if(entity!=null) {
                entity = (Entity) session.get(entity.getClass(), entity.getId());
            }
            transaction.commit();
            item = (Item) Item.build(entity, item.getClass());
        } catch (Exception e) {
            if (transaction!=null) {
                transaction.rollback();
            }
            item = null;
            e.printStackTrace();
        } finally {
            session.close();
            return item;
        }
    }


    @Override
    public List<Item> getItems(RepositoryFilter filter, RepositoryOrder order, RepositoryLimit limit) {
        Session session = PersistenceHandler.build().getSession();
        List<Item> items = null;
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            items = new ArrayList<>();
            Class<?> typeEntityClass = Class.forName(typeEntity.getTypeName());
            Class<?> typeItemClass = Class.forName(typeItem.getTypeName());

//            Table table = typeEntityClass.getAnnotation(Table.class);
//            String alias = typeEntity.getTypeName().substring(0, 1).toLowerCase();

            String sql = "SELECT " + typeEntity.getTypeName().substring(0, 1).toLowerCase() + " " +
                "FROM " + typeEntity.getTypeName() + " " + typeEntity.getTypeName().substring(0, 1).toLowerCase() + " " +
                (filter!=null && !filter.isEmpty() ? "WHERE " + filter.toString() + " " : "") +
                (order!=null && !order.isEmpty() ? "ORDER BY " + order.toString() + " " : "");

            Query query = session.createQuery(sql, typeEntityClass);
//            System.out.println(query.getQueryString());

            if(limit!=null) {
                if(limit.getOffset()!=null) {
                    query.setFirstResult(limit.getOffset());
                }
                if(limit.getCount()!=null) {
                    query.setMaxResults(limit.getCount());
                }
            }

            List<Entity> entities = query.list();
            items = infalte(entities);

            transaction.commit();
        } catch (Exception e) {
            if (transaction!=null) {
                transaction.rollback();
            }
            items = null;
            e.printStackTrace();
        } finally {
            session.close();
            return items;
        }
    }

    public List<Item> getItems(RepositoryFilter filter, RepositoryOrder order) {
        return getItems(filter, order, null);
    }

    public List<Item> getItems(RepositoryFilter filter, RepositoryLimit limit) {
        return getItems(filter, null, limit);
    }

    public List<Item> getItems(RepositoryFilter filter) {
        return getItems(filter, null, null);
    }

    public List<Item> getItems(RepositoryOrder order) {
        return getItems(null, order, null);
    }

    public List<Item> getItems(RepositoryLimit limit) {
        return getItems(null, null, limit);
    }

    public List<Item> getItems() {
        return getItems(null, null, null);
    }


    public Long getItemsCount(RepositoryFilter filter) {
        Session session = PersistenceHandler.build().getSession();
        Long count = 0L;
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Class<?> typeEntityClass = Class.forName(typeEntity.getTypeName());
            Class<?> typeItemClass = Class.forName(typeItem.getTypeName());

            String sql = "SELECT COUNT("+typeEntity.getTypeName().substring(0, 1).toLowerCase()+") " +
                    "FROM " + typeEntity.getTypeName() + " " + typeEntity.getTypeName().substring(0, 1).toLowerCase() + " " +
                    (filter!=null && !filter.isEmpty() ? "WHERE " + filter.toString() + " " : "");

            Query query = session.createQuery(sql);

            List entities = query.list();
            count = (Long) entities.get(0);
//            count = ((Integer) result[0]).intValue();

            transaction.commit();
        } catch (Exception e) {
            if (transaction!=null) {
                transaction.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
            return count;
        }
    }


    protected List<Item> infalte(List<Entity> entities) {
        List<Item> items = null;
        try {
            items = new ArrayList<>();
            Class<?> typeEntityClass = Class.forName(typeEntity.getTypeName());
            Class<?> typeItemClass = Class.forName(typeItem.getTypeName());

            Iterator<Entity> iEntities = entities.iterator();
            while (iEntities.hasNext()) {
                Entity entity = iEntities.next();
                List<Field> fields = Arrays.asList(entity.getClass().getDeclaredFields());
                Iterator<Field> iFields = fields.iterator();
                while (iFields.hasNext()) {
                    Field field = iFields.next();
                    field.setAccessible(true);
                    Object value = field.get(entity);
                    Hibernate.initialize(value);
                    if (value instanceof HibernateProxy) {
                        value = ((HibernateProxy) value).getHibernateLazyInitializer()
                                .getImplementation();
                        field.set(entity, value);
                    }
                }

                Method methodBuild = typeItemClass.getMethod("build", EntityBase.class, Type.class);
                Item item = (Item) methodBuild.invoke(this, entity, typeItem);
                items.add(inject(entity, item));
            }
        } catch (Exception e) {
            throw e;
        } finally {
            return items;
        }
    }


    public RepositoryBase run(RepositoryTaskListener listener) {
        if(executor!=null) {
            executor = Executors.newFixedThreadPool(10);
        }

        RepositoryTask task = new RepositoryTask(listener);
        running.add(task);
        task.run();

        return this;
    }

    public void cancel() {
        if(executor!=null) {
//            executor.cancel();
        }
        if(running!=null && !running.isEmpty()) {
            Iterator<RepositoryTask> iRunning = running.iterator();
            while (iRunning.hasNext()) {
                RepositoryTask task = iRunning.next();
                task.cancel();
            }
            running.clear();
        }
    }

    private class RepositoryTask implements Runnable {
        private Future future;
        private RepositoryTaskListener listener;

        public RepositoryTask(RepositoryTaskListener listener) {
            this.listener = listener;
        }

        @Override
        public void run() {
            // Start
            listener.onStart();
            // Execute
            future = executor.submit(() -> listener.onTask());
            // Finish
            // @todo repeated thread listener
            if(future.isDone()) {
                try {
                    listener.onDone(future.get());
                } catch (Exception e) {
                    listener.onDone(null);
                }
            }
        }

        public void cancel() {
            if(future!=null && !future.isDone() && !future.isCancelled()) {
                future.cancel(true);
            }
        }
    }
}
