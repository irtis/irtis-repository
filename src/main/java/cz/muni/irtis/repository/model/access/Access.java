package cz.muni.irtis.repository.model.access;

import cz.muni.irtis.repository.database.access.ApiEntity;
import cz.muni.irtis.repository.model.RepositoryInstanceBase;
import cz.muni.irtis.repository.model.persons.Person;
import cz.muni.irtis.repository.model.utils.Time;


public class Access extends RepositoryInstanceBase {
    private Person person;
    private Api api;
    private String ip;
    private String link;
    private String application;
    private String language;
    private Long time;

    public Access() {
    }

    public Access(Long id) {
        super(id);
    }

    static public Access build() {
        return build(Access.class);
    }

    static public Access build(Long id) {
        return build(id, Access.class);
    }


    public Person getPerson() {
        return person;
    }

    public Access setPerson(Person person) {
        this.person = person;
        return this;
    }

    public Api getApi() {
        return api;
    }

    public Access setApi(Api api) {
        this.api = api;
        return this;
    }

    public String getIp() {
        return ip;
    }

    public Access setIp(String ip) {
        this.ip = ip;
        return this;
    }

    public String getLink() {
        return link;
    }

    public Access setLink(String link) {
        this.link = link;
        return this;
    }

    public String getApplication() {
        return application;
    }

    public Access setApplication(String application) {
        this.application = application;
        return this;
    }

    public String getLanguage() {
        return language;
    }

    public Access setLanguage(String language) {
        this.language = language;
        return this;
    }

    public Long getTime() {
        return time;
    }

    public Access setTime(Long time) {
        this.time = time;
        return this;
    }
}
