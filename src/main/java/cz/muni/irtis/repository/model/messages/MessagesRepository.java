package cz.muni.irtis.repository.model.messages;

import cz.muni.irtis.repository.database.identity.PersonEntity;
import cz.muni.irtis.repository.database.messages.MessageEntity;
import cz.muni.irtis.repository.model.*;
import cz.muni.irtis.repository.model.access.AccessRepository;
import cz.muni.irtis.repository.model.persons.Group;
import cz.muni.irtis.repository.model.persons.Person;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;


public class MessagesRepository extends RepositoryBase<MessageEntity, Message> implements RepositoryStoreable<Message> {
    static public Integer TYPE_SCREENSHOTS_OFF_WARNING = 1;
    static public Integer TYPE_QUESTIONNAIRES_WARNING = 2;
    static public Integer TYPE_QUESTIONNAIRES_COINS_ENCOURAGEMENT = 3;

    static public Integer TYPE_STUDIES_START = 11;
    static public Integer TYPE_STUDIES_END = 12;
    static public Integer TYPE_BURSTS_START = 13;
    static public Integer TYPE_BURSTS_END = 14;
    static public Integer TYPE_BURSTS_START_REMINDER = 15;
    static public Integer TYPE_BURSTS_END_REMINDER = 16;


    public MessagesRepository() {
        super(MessageEntity.class, Message.class);
    }

    static public MessagesRepository build() {
        return new MessagesRepository();
    }


    public List<Message> getItemByPerson(Person person) {
        return getItems(
                RepositoryFilter.build()
                    .add("person_id = ?", person.getId()),
                RepositoryOrder.build()
                    .add("created", "DESC"));
    }

    public Message getItemByPersonAndType(Person person, Integer type) {
        List<Message> result = getItems(
                RepositoryFilter.build()
                        .add("type = ?", type)
                        .add("person_id = ?", person.getId()),
                RepositoryOrder.build()
                        .add("created", "DESC"),
                RepositoryLimit.build().setCount(1));
        if(result!=null && !result.isEmpty()) {
            return result.get(0);
        } else {
            return null;
        }
    }

    public List<Message> getItemsByPersonAndType(Person person, Integer type) {
        return getItems(
                RepositoryFilter.build()
                    .add("type = ?", type)
                    .add("person_id = ?", person.getId()),
                RepositoryOrder.build()
                    .add("created", "DESC"));
    }


    public List<Message> getItemsNotDownloadedByPerson(Person person) {
        return getItems(
                RepositoryFilter.build()
                    .add("downloaded IS NULL")
                    .add("person_id = ?", person.getId()),
                RepositoryOrder.build()
                    .add("created", "DESC"));
    }

    public List<Message> getItemsUploadedByPerson(Person person) {
        return getItems(
                RepositoryFilter.build()
                    .add("uploaded IS NOT NULL")
                    .add("person_id = ?", person.getId()),
                RepositoryOrder.build()
                    .add("uploaded", "DESC"));
    }

    @Override
    public Message inject(MessageEntity entity, Message item) {
        item.setPerson(Person.build(entity.getPerson(), Person.class));
        item.setCreator(Person.build(entity.getCreator(), Person.class));
        return item;
    }
}
