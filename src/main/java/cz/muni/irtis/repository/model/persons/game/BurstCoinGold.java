package cz.muni.irtis.repository.model.persons.game;

public class BurstCoinGold implements BurstCoin {

    static public BurstCoinGold build() {
        return new BurstCoinGold();
    }

    @Override
    public Integer getValue() {
        return 10;
    }
}
