package cz.muni.irtis.repository.model.metrics;

import cz.muni.irtis.repository.database.metrics.RuntimeEntity;
import cz.muni.irtis.repository.database.metrics.screen.ScreenEntity;
import cz.muni.irtis.repository.model.*;
import cz.muni.irtis.repository.model.persons.Person;

import java.util.List;


public class ScreenRepository extends RepositoryBase<ScreenEntity, Screen> implements RepositoryStoreable<Screen> {
    public ScreenRepository() {
        super(ScreenEntity.class, Screen.class);
    }

    static public ScreenRepository build() {
        return new ScreenRepository();
    }

    public List<Screen> getItemsByPerson(Person person, RepositoryLimit limit) {
        return getItems(
            RepositoryFilter.build()
                .add("person_id = ?", person.getId()),
            RepositoryOrder.build()
                .add("started", "DESC"),
            limit
        );
    }
    
    @Override
    public Screen inject(ScreenEntity entity, Screen item) {
        return item;
    }
}
