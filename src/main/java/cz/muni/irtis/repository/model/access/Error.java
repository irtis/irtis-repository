package cz.muni.irtis.repository.model.access;

import cz.muni.irtis.repository.model.RepositoryInstanceBase;
import cz.muni.irtis.repository.model.persons.Person;
import cz.muni.irtis.repository.model.utils.Time;


public class Error extends RepositoryInstanceBase {
    private Person person;
    private Integer type;
    private String identifier;
    private String content;
    private String location;
    private Long time;

    public Error() {
    }

    public Error(Long id) {
        super(id);
    }

    static public Error build() {
        return build(Error.class);
    }

    static public Error build(Long id) {
        return build(id, Error.class);
    }

    static public Error build(Person person, Integer type, String identifier, String content, String location, Long time) {
        Error instance = build(Error.class);
        instance.setPerson(person);
        instance.setType(type);
        instance.setIdentifier(identifier);
        instance.setContent(content);
        instance.setLocation(location);
        instance.setTime(time);
        return instance;
    }

    static public Error build(Person person, Integer type, String identifier, String content) {
        return build(person, type, identifier, content, null, Time.getTime());
    }


    public Person getPerson() {
        return person;
    }

    public Error setPerson(Person person) {
        this.person = person;
        return this;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getContent() {
        return content;
    }

    public Error setContent(String content) {
        this.content = content;
        return this;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Long getTime() {
        return time;
    }

    public Error setTime(Long time) {
        this.time = time;
        return this;
    }
}
