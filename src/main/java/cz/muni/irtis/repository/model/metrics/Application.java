package cz.muni.irtis.repository.model.metrics;

import cz.muni.irtis.repository.model.RepositoryInstanceBase;
import cz.muni.irtis.repository.model.persons.Person;

public class Application extends RepositoryInstanceBase {
    private String identifier;
    private String name;


    public Application() {
    }

    public Application(Long id) {
        super(id);
    }

    static public Application build() {
        return build(Application.class);
    }

    static public Application build(Long id) {
        return build(id, Application.class);
    }


    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
