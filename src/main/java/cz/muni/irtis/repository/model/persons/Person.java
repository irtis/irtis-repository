package cz.muni.irtis.repository.model.persons;

import cz.muni.irtis.repository.model.RepositoryInstanceBase;
import cz.muni.irtis.repository.model.persons.game.BurstPurse;

import java.util.Objects;

public class Person extends RepositoryInstanceBase {
    private Group group;
    private String password;
    private String name;
    private String email;
    private String phone;
    private String token;
    private String device;
    private String api;


    public Person() {
    }

    public Person(Long id) {
        super(id);
    }

    static public Person build() {
        return build(Person.class);
    }

    static public Person build(Long id) {
        return build(id, Person.class);
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }


    @Override
    public boolean equals(Object o) {
        return getId().equals(((Person) o).getId());
    }
}
