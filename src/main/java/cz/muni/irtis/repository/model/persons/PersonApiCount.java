package cz.muni.irtis.repository.model.persons;

public class PersonApiCount {
    private String api;
    private Long count;

    public PersonApiCount(String api, Long count) {
        this.api = api;
        this.count = count;
    }

    public String getApi() {
        return api;
    }

    public Long getCount() {
        return count;
    }
}
