package cz.muni.irtis.repository.model.persistence;

import cz.muni.irtis.repository.database.identity.PersonEntity;
import cz.muni.irtis.repository.model.RepositoryInstanceBase;
import cz.muni.irtis.repository.model.persons.Person;

import javax.persistence.*;

public class Event extends RepositoryInstanceBase {
    private Person person;
    private Integer identifier;
    private Long time;

    public Event() {
    }

    public Event(Long id) {
        super(id);
    }

    static public Event build() {
        return build(Event.class);
    }

    static public Event build(Long id) {
        return build(id, Event.class);
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Integer getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Integer identifier) {
        this.identifier = identifier;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
