package cz.muni.irtis.repository.model.questionnaires;

import cz.muni.irtis.repository.database.questionnaires.QuestionnaireEntity;
import cz.muni.irtis.repository.model.RepositoryBase;
import cz.muni.irtis.repository.model.RepositoryFilter;
import cz.muni.irtis.repository.model.RepositoryStoreable;
import cz.muni.irtis.repository.model.persons.PersonsRepository;

import java.util.List;

public class QuestionnairesRepository extends RepositoryBase<QuestionnaireEntity, Questionnaire> implements RepositoryStoreable<Questionnaire> {
    public static final int TYPE_REGULAR = 1;    // Regular that is constantly repeated every same time throughout the burst
    public static final int TYPE_INVOKED = 2;    // Created by the user on the client side
    public static final int TYPE_TRIGGERED = 3;  // Triggered by the robot on the account of some incident
    public static final int TYPE_SPECIAL = 4;  // Out of ordinary, once in a time, questionnairy that can be place outside burst, does not earn any coins

    public QuestionnairesRepository() {
        super(QuestionnaireEntity.class, Questionnaire.class);
    }

    static public QuestionnairesRepository build() {
        return new QuestionnairesRepository();
    }

    public Questionnaire getItemByIdentifier(String identifier) {
        List<Questionnaire> result = getItems(RepositoryFilter.build().add("identifier = ?", identifier));
        if(result!=null && !result.isEmpty()) {
            return result.get(0);
        } else {
            return null;
        }
    }

    @Override
    public Questionnaire inject(QuestionnaireEntity entity, Questionnaire item) {
        return item;
    }
}
