package cz.muni.irtis.repository.model;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import cz.muni.irtis.repository.database.EntityBase;

import javax.persistence.Transient;
import java.lang.reflect.Constructor;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class RepositoryInstanceBase<Item extends RepositoryInstanceBase> {
    protected Long id;

    public RepositoryInstanceBase() {
    }

    public RepositoryInstanceBase(Long id) {
        this.id = id;
    }

    static public <Item extends RepositoryInstanceBase> Item build(Type type) {
        Item instance = null;
        try {
            Class<?> typeClass = Class.forName(type.getTypeName());
            Constructor<?> construct = typeClass.getConstructor();
            instance = (Item) construct.newInstance();
        } catch (Exception e) {
            System.err.println("No default constructor is defined.");
            throw new RuntimeException(e);
        }
        return instance;
    }

    static public <Item extends RepositoryInstanceBase> Item build(Long id, Type type) {
        Item instance = null;
        try {
            Class<?> typeClass = Class.forName(type.getTypeName());
            Constructor<?> construct = typeClass.getConstructor(Long.class);
            instance = (Item) construct.newInstance(new Object[] {id});
        } catch (Exception e) {
            System.err.println("No constructor for `Long id` is defined.");
            throw new RuntimeException(e);
        }
        return instance;
    }

    static public <Item extends RepositoryInstanceBase> Item build(String json, Type type) {
        try {
            Class<?> typeClass = Class.forName(type.getTypeName());
            return (Item) GsonFactory.build(typeClass).fromJson(json, type);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    static public <Entity extends EntityBase, Item extends RepositoryInstanceBase> Item build(Entity entity, Type type) {
        if(entity!=null) {
            String json = GsonFactory.build(entity.getClass()).toJson(entity);
            Item item = null;
            if (json != null && !json.isEmpty()) {
                try {
                    Class<?> typeClass = Class.forName(type.getTypeName());
                    item = GsonFactory.build(typeClass).fromJson(json, type);
                } catch (Exception e) {
                    e.printStackTrace();
                    item = null;
                }
            }
            return item;
        }
        return null;
    }



    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    static public class GsonFactory {
        static public Gson build(final Class<?> type, final List<String> exclude) {
            return new GsonBuilder()
                    .addSerializationExclusionStrategy(new ExclusionStrategy() {
                        @Override
                        public boolean shouldSkipField(FieldAttributes f) {
                            List<Class> exclusions = Arrays.asList(new Class[]{
                                    Exclude.class, Transient.class
                            });

                            Iterator<Class> iExclusions = exclusions.iterator();
                            while (iExclusions.hasNext()) {
                                Class exclusion = iExclusions.next();
                                if (f.getAnnotation(exclusion) != null) {
                                    return true;
                                }
                            }

                            if(exclude!=null && exclude.contains(f.getName())) {
                                return true;
                            }

                            return false;
                        }

                        @Override
                        public boolean shouldSkipClass(Class<?> clazz) {
                            if (clazz.isPrimitive()) {
                                return false;
                            } else if (type != null && clazz.equals(type)) {
                                return false;
                            } else {
                                List<Class> inclusion = Arrays.asList(new Class[]{
                                        Integer.class, Long.class, Float.class, Double.class, Boolean.class, String.class
                                });
                                if (inclusion.contains(clazz)) {
                                    return false;
                                }
                            }
                            return true;
                        }
                    }).addDeserializationExclusionStrategy(new ExclusionStrategy() {
                        @Override
                        public boolean shouldSkipField(FieldAttributes f) {
                            List<Class> exclusions = Arrays.asList(new Class[]{
                                    Exclude.class, Transient.class
                            });

                            Iterator<Class> iExclusions = exclusions.iterator();
                            while (iExclusions.hasNext()) {
                                Class exclusion = iExclusions.next();
                                if (f.getAnnotation(exclusion) != null) {
                                    return true;
                                }
                            }

                            return false;
                        }

                        @Override
                        public boolean shouldSkipClass(Class<?> type) {
                            return false;
                        }
                    })
                    .create();
        }

        static public Gson build(final Class<?> type) {
            return GsonFactory.build(type, null);
        }
    }
}
