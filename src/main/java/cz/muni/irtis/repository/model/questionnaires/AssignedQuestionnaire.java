package cz.muni.irtis.repository.model.questionnaires;

import cz.muni.irtis.repository.database.questionnaires.QuestionnaireEntity;
import cz.muni.irtis.repository.model.utils.Time;

import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

public class AssignedQuestionnaire {
    private Long id;
    private QuestionnaireEntity questionnaire;
    private List<Long> personIds = new ArrayList<>(); // miss
    private List<Long> groupIds = new ArrayList<>(); // miss
    private String validFrom;
    private String validTo;
    private String startingTime;
    private String endingTime;
    private int repeatAfter;
    private String expire;
    private Integer maxNotifications;
    private String notificationPeriod;
    private boolean downloaded = false;
    private Long startingTimeLong;
    private Long endingTimeLong;
    private String buffer;
    private String invoked;
    private Long invokedLong;

    public AssignedQuestionnaire() {
    }

    public AssignedQuestionnaire(Long assignmentId) {
        this.id = assignmentId;
    }

    public AssignedQuestionnaire(Long assignmentId, QuestionnaireEntity questionnaire, Long startingTime, Long endingTime, Integer expire, Integer buffer, Long invokedLong) {
        this.id = assignmentId;
        this.questionnaire = questionnaire;
        this.startingTimeLong = startingTime;
        this.endingTimeLong = endingTime;
        this.startingTime = Time.getTimeFormattedForUi(startingTime);
        this.endingTime = Time.getTimeFormattedForUi(endingTime);
        this.expire = Time.getHHmmFromMinutes(expire);
        this.buffer = Time.getHHmmFromMinutes(buffer);
        this.invoked = Time.getTimeFormattedForUi(invokedLong);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public QuestionnaireEntity getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(QuestionnaireEntity questionnaire) {
        this.questionnaire = questionnaire;
    }

    public List<Long> getPersonIds() {
        return personIds;
    }

    public void setPersonIds(List<Long> personIds) {
        this.personIds = personIds;
    }

    public List<Long> getGroupIds() {
        return groupIds;
    }

    public void setGroupIds(List<Long> groupIds) {
        this.groupIds = groupIds;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getStartingTime() {
        return startingTime;
    }

    public void setStartingTime(String startingTime) {
        this.startingTime = startingTime;
    }

    public String getEndingTime() {
        return endingTime;
    }

    public void setEndingTime(String endingTime) {
        this.endingTime = endingTime;
    }

    public int getRepeatAfter() {
        return repeatAfter;
    }

    public void setRepeatAfter(int repeatAfter) {
        this.repeatAfter = repeatAfter;
    }

    public String getExpire() {
        return expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    public String getInvoked() {
        return invoked;
    }

    public void setInvoked(String invoked) {
        this.invoked = invoked;
    }

    public void update(AssignedQuestionnaire json) {
        this.groupIds = json.getGroupIds();
        this.personIds = json.getPersonIds();
        this.validFrom = json.getValidFrom();
        this.validTo = json.getValidTo();
        this.startingTime = json.getStartingTime();
        this.repeatAfter = json.getRepeatAfter();
        this.expire = json.getExpire();
        this.buffer = json.getBuffer();
        this.maxNotifications = json.getMaxNotifications();
        this.notificationPeriod = json.getNotificationPeriod();
        this.invoked = json.getInvoked();
    }

    public Integer getMaxNotifications() {
        return maxNotifications;
    }

    public void setMaxNotifications(Integer maxNotifications) {
        this.maxNotifications = maxNotifications;
    }

    public String getNotificationPeriod() {
        return notificationPeriod;
    }

    public void setNotificationPeriod(String notificationPeriod) {
        this.notificationPeriod = notificationPeriod;
    }

    public Long getStartingTimeLong() {
        return startingTimeLong;
    }

    public void setStartingTimeLong(Long startingTimeLong) {
        this.startingTimeLong = startingTimeLong;
    }

    public Long getEndingTimeLong() {
        return endingTimeLong;
    }

    public void setEndingTimeLong(Long endingTimeLong) {
        this.endingTimeLong = endingTimeLong;
    }

    public Long getInvokedLong() {
        return invokedLong;
    }

    public void setInvokedLong(Long invokedLong) {
        this.invokedLong = invokedLong;
    }

    /**
     * Expire in minutes
     * @return minutes
     */
    public int getExpireInt() {
        try {
            return Time.getMinutesFromHHmm(expire);
        } catch (DateTimeParseException e) {
            return 45;
        }
    }

    /**
     * Expire in minutes
     * @return minutes
     */
    public int getBufferInt() {
        try {
            return Time.getMinutesFromHHmm(buffer);
        } catch (DateTimeParseException e) {
            return 90;
        }

    }

    public boolean isDownloaded() {
        return downloaded;
    }

    public void setDownloaded(boolean downloaded) {
        this.downloaded = downloaded;
    }

    public String getBuffer() {
        return buffer;
    }

    public void setBuffer(String buffer) {
        this.buffer = buffer;
    }
}
