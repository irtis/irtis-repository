package cz.muni.irtis.repository.model.questionnaires;

import cz.muni.irtis.repository.database.questionnaires.PersonQuestionEntity;
import cz.muni.irtis.repository.database.questionnaires.PersonQuestionnaireEntity;
import cz.muni.irtis.repository.model.*;
import cz.muni.irtis.repository.model.persons.Person;
import cz.muni.irtis.repository.model.utils.Time;

import java.util.List;

public class QuestionnairesQuestionsPersonsRepository extends RepositoryBase<PersonQuestionEntity, QuestionnaireQuestionPerson> implements RepositoryStoreable<QuestionnaireQuestionPerson> {

    public QuestionnairesQuestionsPersonsRepository() {
        super(PersonQuestionEntity.class, QuestionnaireQuestionPerson.class);
    }

    static public QuestionnairesQuestionsPersonsRepository build() {
        return new QuestionnairesQuestionsPersonsRepository();
    }

    public Long getItemsCountAnswers(Person person, QuestionnairePerson questionnaire) {
        Long count = getItemsCount(
            RepositoryFilter.build()
                .add("person_id", person.getId())
                .add("people_questionnaire_id", questionnaire.getId())
                .add("skipped IS NOT NULL"));
        return count;
    }

    public Long getItemsCountAnswered(Person person, QuestionnairePerson questionnaire) {
        Long count = getItemsCount(
            RepositoryFilter.build()
                .add("person_id", person.getId())
                .add("people_questionnaire_id", questionnaire.getId())
                .add("skipped = false"));
        return count;
    }

    @Override
    public QuestionnaireQuestionPerson inject(PersonQuestionEntity entity, QuestionnaireQuestionPerson item) {
        item.setQuestionnaire(Questionnaire
                .build(entity.getQuestionnaire(), Questionnaire.class));
        return item;
    }
}
