package cz.muni.irtis.repository.model.utils;


import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Time {
    public static final long SECOND = 1000;
    public static final long MINUTE = 60*SECOND;
    public static final long HOUR = 60*MINUTE;
    public static final long DAY = 24*HOUR;
    public static final long WEEK = 7*DAY;
    public static final long MONTH = 30*DAY;

    public static long getTime(Integer day, Integer month, Integer year, Integer hour, Integer minute, Integer second) {
        Calendar time = Calendar.getInstance();
        time.set(Calendar.YEAR, year);
        time.set(Calendar.MONTH, month-1);
        time.set(Calendar.DAY_OF_MONTH, day);
        time.set(Calendar.HOUR_OF_DAY, hour);
        time.set(Calendar.MINUTE, minute);
        time.set(Calendar.SECOND, second);
        time.set(Calendar.MILLISECOND, 0);
        return time.getTimeInMillis();
    }

    public static long getTime(Integer day, Integer month, Integer year) {
        return getTime(day, month, year, 0, 0, 0);
    }

    public static long getTime() {
        return System.currentTimeMillis();
    }

    public static int getTimeUnix(Long time) {
        return (int) (time / 1000L);
    }

    public static int getTimeUnix() {
        return getTimeUnix(getTime());
    }

    static public String getTimeFormatted(Long time, String pattern) {
        if (time == null) {
            return "";
        }
        DateFormat format = new SimpleDateFormat(pattern);
        Date result = new Date(time);
        return format.format(result);
    }

    static public String getTimeFormatted(Integer time, String pattern) {
        DateFormat format = new SimpleDateFormat(pattern);
        Date result = new Date(time*1000);
        return format.format(result);
    }

    static public String getTimeFormatted(Long time) {
        return getTimeFormatted(time, "yyyy-MM-dd HH:mm:ss.SSS");
    }

    public static String getTimeFormattedForUi(Long time) {
        return getTimeFormatted(time, "yyyy-MM-dd HH:mm");
    }

    public static String getHHmmFromMinutes(Integer minutes) {
        if (minutes == null) {
            return null;
        }
        long hours = TimeUnit.MINUTES.toHours(minutes);
        long remainMinutes = minutes - TimeUnit.HOURS.toMinutes(hours);
        return String.format("%02d:%02d", hours, remainMinutes);
    }

    public static int getMinutesFromHHmm(String time) {
        return (int) ChronoUnit.MINUTES.between(LocalTime.MIDNIGHT, LocalTime.parse(time));
    }

    static public String getTimeFormatted(Integer time) {
        return getTimeFormatted(time, "yyyy-MM-dd HH:mm:ss.SSS");
    }

    public static String getFileFormatDateTime(Long time) {
        return getTimeFormatted(time, "yyy-MM-dd-HH-mm");
    }

    static public Integer getYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }
    static public Integer getYear(Integer time) {
        return Integer.valueOf(getTimeFormatted(time, "yyyy"));
    }
    static public Integer getYear(Long time) {
        return Integer.valueOf(getTimeFormatted(time, "yyyy"));
    }

    static public Integer getMonth() {
        return Calendar.getInstance().get(Calendar.MONTH);
    }
    static public Integer getMonth(Integer time) {
        return Integer.valueOf(getTimeFormatted(time, "MM"));
    }
    static public Integer getMonth(Long time) {
        return Integer.valueOf(getTimeFormatted(time, "MM"));
    }

    static public Integer getDay() {
        return Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    }
    static public Integer getDay(Integer time) {
        return Integer.valueOf(getTimeFormatted(time,"dd"));
    }
    static public Integer getDay(Long time) {
        return Integer.valueOf(getTimeFormatted(time,"dd"));
    }

    static public Integer getHour() {
        return Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
    }
    static public Integer getHour(Integer time) {
        return Integer.valueOf(getTimeFormatted(time, "HH"));
    }
    static public Integer getHour(Long time) {
        return Integer.valueOf(getTimeFormatted(time, "HH"));
    }

    static public Integer getMinute() {
        return Calendar.getInstance().get(Calendar.MINUTE);
    }
    static public Integer getMinute(Integer time) {
        return Integer.valueOf(getTimeFormatted(time, "mm"));
    }
    static public Integer getMinute(Long time) {
        return Integer.valueOf(getTimeFormatted(time, "mm"));
    }

    /**
     * isDayTime()
     *
     * From 06:00 - 22:00
     *
     * @return
     */
    static public Boolean isDayTime() {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        return hour > 6 && hour < 22;
    }

    /**
     * isNightTime()
     *
     * From 22:00 - 06:00
     *
     * @return
     */
    static public Boolean isNightTime() {
        return !isDayTime();
    }

    /**
     * getElapsedTimeFormatted()
     *
     * @param time
     * @param format  %d %h:%m:%s
     * @return
     */
    static public String getElapsedTimeFormatted(Long time, String format) {
        String output = "";
        Long seconds = time % 60;
        Long minutes = (time / 60) % 60;
        Long hours = (time / 60 / 60) % 24;
        Long days = time / 60 / 60 / 24;
        if(!format.contains("%d")) {
            hours = time / 60 / 60;
        }

        Matcher matcher = Pattern.compile("(\\%d|\\%h|\\%m|\\%s)+?").matcher(format);
        while (matcher.find()) {
            String part = matcher.group();
            if(output.isEmpty()) {
                output = format;
            }
            if (part.equals("%d")) {
                output = output.replace(part, String.format("%02d", days));
            } else if (part.equals("%h")) {
                output = output.replace(part, String.format("%02d", hours));
            } else if (part.equals("%m")) {
                output = output.replace(part, String.format("%02d", minutes));
            } else if (part.equals("%s")) {
                output = output.replace(part, String.format("%02d", seconds));
            }
        }
        if(output.isEmpty()) {
            output = String.format("%02d:%02d:%02d", hours, minutes, seconds);
        }

        return output;
    }

    /**
     * getElapsedTimeFormatted()
     *
     * @param time
     * @return
     */
    static public String getElapsedTimeFormatted(Long time) {
        return getElapsedTimeFormatted(time,"%h:%m:%s");
    }

    /**
     * hasElapsedTimeDays()
     * @param time
     * @return
     */
    static public boolean hasElapsedDays(Long time) {
        return (time / 60 / 60 / 24) > 0;
    }

    /**
     * hasElapsedTimeHours()
     * @param time
     * @return
     */
    static public boolean hasElapsedHours(Long time) {
        return (time / 60 / 60) > 0;
    }

    /**
     * hasElapsedTimeMinutes()
     * @param time
     * @return
     */
    static public boolean hasElapsedMinutes(Long time) {
        return (time / 60) > 0;
    }

    public static Optional<Long> getLocalMilis(String date, String time) {
        if (date != null && date.length() > 0) {
            if (time == null || time.length() < 1) {
                time = "00:00";
            }
            LocalDateTime start = LocalDateTime.parse(date + "T" + time);
            return Optional.of(start
                    .toInstant(
                            ZoneId.of("Europe/Prague").getRules().getOffset(start))
                    .toEpochMilli());
        }
        return Optional.empty();
    }

    public static String getUiHours(long time){
        Date date = new Date(time);
        Format format = new SimpleDateFormat("HH:mm");
        return format.format(date);
    }

    public static String getUiDate(long time){
        Date date = new Date(time);
        Format format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }

    private String getTimeFormatted(long time){
        Date date = new Date(time);
        Format format = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        return format.format(date);
    }



}
