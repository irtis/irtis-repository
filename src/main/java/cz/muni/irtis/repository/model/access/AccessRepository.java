package cz.muni.irtis.repository.model.access;

import cz.muni.irtis.repository.database.PersistenceHandler;
import cz.muni.irtis.repository.database.access.AccessEntity;
import cz.muni.irtis.repository.database.identity.BurstEntity;
import cz.muni.irtis.repository.database.messages.MessageEntity;
import cz.muni.irtis.repository.database.questionnaires.PersonQuestionnaireEntity;
import cz.muni.irtis.repository.model.RepositoryBase;
import cz.muni.irtis.repository.model.RepositoryFilter;
import cz.muni.irtis.repository.model.RepositoryOrder;
import cz.muni.irtis.repository.model.RepositoryStoreable;
import cz.muni.irtis.repository.model.messages.Message;
import cz.muni.irtis.repository.model.persons.Person;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Table;
import java.util.List;


public class AccessRepository extends RepositoryBase<AccessEntity, Access> implements RepositoryStoreable<Access> {

    public AccessRepository() {
        super(AccessEntity.class, Access.class);
    }

    static public AccessRepository build() {
        return new AccessRepository();
    }


    public Access getItemLastByApi(Api api) {
        List<Access> result = getItems(
            RepositoryFilter.build()
                .add("api_id = ?", api.getId()),
            RepositoryOrder.build()
                .add("time", "DESC"));
        if(result!=null && !result.isEmpty()) {
            return result.get(0);
        } else {
            return null;
        }
    }

    @Override
    public Access inject(AccessEntity entity, Access item) {
        item.setPerson(Person.build(entity.getPerson(), Person.class));
        item.setApi(Api.build(entity.getApi(), Api.class));
        return item;
    }
}
