package cz.muni.irtis.repository.model.persons.game;

import cz.muni.irtis.repository.model.persons.Person;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BurstPurse {
    private Person person;
    private List<BurstCoin> coins;
    private Integer notified = 0;
    private Integer completed = 0;
    private Integer total = 0;

    private BurstPurse() {
        coins = new ArrayList<>();
    }

    static public BurstPurse build() {
        return new BurstPurse();
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public void put(BurstCoin coin) {
        coins.add(coin);
    }

    public void put(List<BurstCoin> coins) {
        Iterator<BurstCoin> iCoins = coins.iterator();
        while (iCoins.hasNext()) {
            put(iCoins.next());
        }
    }

    public List<BurstCoin> getCoins() {
        return coins;
    }

    public Integer getAmount() {
        Integer amount = 0;
        Iterator<BurstCoin> iCoins = coins.iterator();
        while (iCoins.hasNext()) {
            amount += iCoins.next().getValue();
        }
        return amount;
    }

    public Integer getAmountBronze() {
        Integer amount = 0;
        Iterator<BurstCoin> iCoins = coins.iterator();
        while (iCoins.hasNext()) {
            BurstCoin coin = iCoins.next();
            if(coin instanceof BurstCoinBronze) {
                amount += coin.getValue();
            }
        }
        return amount;
    }

    public Integer getAmountSilver() {
        Integer amount = 0;
        Iterator<BurstCoin> iCoins = coins.iterator();
        while (iCoins.hasNext()) {
            BurstCoin coin = iCoins.next();
            if(coin instanceof BurstCoinSilver) {
                amount += coin.getValue();
            }
        }
        return amount;
    }

    public Integer getAmountGold() {
        Integer amount = 0;
        Iterator<BurstCoin> iCoins = coins.iterator();
        while (iCoins.hasNext()) {
            BurstCoin coin = iCoins.next();
            if(coin instanceof BurstCoinGold) {
                amount += coin.getValue();
            }
        }
        return amount;
    }

    public Integer getCount() {
        return coins.size();
    }

    public Integer getCountBronze() {
        Integer count = 0;
        Iterator<BurstCoin> iCoins = coins.iterator();
        while (iCoins.hasNext()) {
            BurstCoin coin = iCoins.next();
            if(coin instanceof BurstCoinBronze) {
                count++;
            }
        }
        return count;
    }

    public String getCountBronzeFormatted() {
        return getCountBronze().toString()+"x";
    }

    public Integer getCountSilver() {
        Integer count = 0;
        Iterator<BurstCoin> iCoins = coins.iterator();
        while (iCoins.hasNext()) {
            BurstCoin coin = iCoins.next();
            if(coin instanceof BurstCoinSilver) {
                count++;
            }
        }
        return count;
    }

    public String getCountSilverFormatted() {
        return getCountSilver().toString()+"x";
    }

    public Integer getCountGold() {
        Integer count = 0;
        Iterator<BurstCoin> iCoins = coins.iterator();
        while (iCoins.hasNext()) {
            BurstCoin coin = iCoins.next();
            if(coin instanceof BurstCoinGold) {
                count++;
            }
        }
        return count;
    }

    public String getCountGoldFormatted() {
        return getCountGold().toString()+"x";
    }

    public Integer getNotified() {
        return notified;
    }

    public void setNotified(Integer notified) {
        this.notified = notified;
    }

    public void putNotified(Integer notified) {
        this.notified += notified;
    }

    public Integer getCompleted() {
        return completed;
    }

    public void setCompleted(Integer completed) {
        this.completed = completed;
    }

    public void putCompleted(Integer completed) {
        this.completed += completed;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public void putTotal(Integer total) {
        this.total += total;
    }

    public Float getCompliance() {
        Float percents = 0f;
        if(getTotal()!=null && getCountBronze()!=null) {
            Float d = (float) getCountBronze() / getTotal();
            Float p = d * 100.0f;
            Float r = p / 100.0f;

            percents = ((float) getCountBronze() / getTotal()) * 100.0f;
        }
        return percents;
    }
}
