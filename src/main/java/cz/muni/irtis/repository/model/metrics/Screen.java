package cz.muni.irtis.repository.model.metrics;

import cz.muni.irtis.repository.model.RepositoryInstanceBase;
import cz.muni.irtis.repository.model.persons.Person;

public class Screen extends RepositoryInstanceBase {
    private Person person;
    private Boolean state;

    public Screen() {
    }

    public Screen(Long id) {
        super(id);
    }

    static public Screen build() {
        return build(Screen.class);
    }

    static public Screen build(Long id) {
        return build(id, Screen.class);
    }

    public Person getPerson() {
        return person;
    }

    public Boolean getState() {
        return state;
    }
}
