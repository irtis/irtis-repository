package cz.muni.irtis.repository.model.access;

import cz.muni.irtis.repository.database.access.AccessEntity;
import cz.muni.irtis.repository.database.access.ErrorEntity;
import cz.muni.irtis.repository.model.*;
import cz.muni.irtis.repository.model.persons.Person;

import java.util.List;


public class ErrorsRepository extends RepositoryBase<ErrorEntity, Error> implements RepositoryStoreable<Error> {
    public static final int TYPE_EXCEPTION = 1;
    public static final int TYPE_ERROR = 2;
    public static final int TYPE_INFORMATION = 3;

    public ErrorsRepository() {
        super(ErrorEntity.class, Error.class);
    }

    static public ErrorsRepository build() {
        return new ErrorsRepository();
    }

    public Error getLastForPerson(Person person, Integer type) {
        RepositoryFilter filter = RepositoryFilter.build()
                .add("person_id = ?", person.getId());

        if(type!=null) {
            filter.add("type = ?", type);
        }

        List<Error> items = getItems(
            filter,
            RepositoryOrder.build().add("time", "DESC"),
            RepositoryLimit.build().setCount(1)
        );

        if(items!=null && !items.isEmpty()) {
            return items.get(0);
        } else {
            return null;
        }
    }

    public Error getLastForPerson(Person person) {
        return getLastForPerson(person, null);
    }

    @Override
    public Error inject(ErrorEntity entity, Error item) {
        item.setPerson(Person.build(entity.getPerson(), Person.class));
        return item;
    }
}
