package cz.muni.irtis.repository.model.persons;

import cz.muni.irtis.repository.database.PersistenceHandler;
import cz.muni.irtis.repository.database.identity.PersonEntity;
import cz.muni.irtis.repository.model.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;


public class PersonsRepository extends RepositoryBase<PersonEntity, Person> implements RepositoryStoreable<Person> {

    public PersonsRepository() {
        super(PersonEntity.class, Person.class);
    }

    static public PersonsRepository build() {
        return new PersonsRepository();
    }    

    public Person getItemByToken(String token) {
        List<Person> result = getItems(RepositoryFilter.build().add("token = ?", token));
        if(result!=null && !result.isEmpty()) {
            return result.get(0);
        } else {
            return null;
        }
    }

    public List<PersonApiCount> getApisCount() {
        Session session = PersistenceHandler.build().getSession();
        List<PersonApiCount> items = null;
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            items = new ArrayList<>();

            String sql = "" +
                "SELECT new cz.muni.irtis.repository.model.persons.PersonApiCount(e.api, COUNT(e.api)) " +
                "FROM PersonEntity e " +
                "WHERE e.api IS NOT NULL " +
                "GROUP BY e.api " +
                "ORDER BY e.api DESC";
            Query query = session.createQuery(sql, PersonApiCount.class);
            System.out.println(query.getQueryString());

            items = query.list();
            transaction.commit();
        } catch (Exception e) {
            if (transaction!=null) {
                transaction.rollback();
            }
            items = null;
            e.printStackTrace();
        } finally {
            session.close();
            return items;
        }
    }

    public List<PersonDeviceCount> getDevicesCount() {
        Session session = PersistenceHandler.build().getSession();
        List<PersonDeviceCount> items = null;
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            items = new ArrayList<>();

            String sql = "" +
                    "SELECT new cz.muni.irtis.repository.model.persons.PersonDeviceCount(e.device, COUNT(e.device)) " +
                    "FROM PersonEntity e " +
                    "WHERE e.device IS NOT NULL " +
                    "GROUP BY e.device " +
                    "ORDER BY COUNT(e.device) DESC";
            Query query = session.createQuery(sql, PersonDeviceCount.class);
            System.out.println(query.getQueryString());

            items = query.list();
            transaction.commit();
        } catch (Exception e) {
            if (transaction!=null) {
                transaction.rollback();
            }
            items = null;
            e.printStackTrace();
        } finally {
            session.close();
            return items;
        }
    }

    public Person isAuthorized(Long id, Long password) {
        List<Person> result = getItems(RepositoryFilter.build()
            .add("id = ?", id)
            .add("token = ?", password));
        if(result!=null && !result.isEmpty()) {
            return result.get(0);
        } else {
            return null;
        }
    }

    @Override
    public Person inject(PersonEntity entity, Person item) {
        item.setGroup(Group.build(entity.getGroup(), Group.class));
        return item;
    }
}
