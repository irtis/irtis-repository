package cz.muni.irtis.repository.model.utils;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * FileTimeConverter
 *
 * Previously DatetimeConverter
 */
public class FileTimeConverter {
    public static final String SHORT_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static final String TIMESTAMP_PATTERN = "yyyyMMddHHmmssSSS";
    private static final String EXTENSION = ".png";

    private final DateTimeFormatter shortFormatter = DateTimeFormatter.ofPattern(SHORT_PATTERN);
    private final DateTimeFormatter timestampFormatter = DateTimeFormatter.ofPattern(TIMESTAMP_PATTERN);

    public DateTimeFormatter getShortFormatter() {
        return shortFormatter;
    }

    public DateTimeFormatter getTimestampFormatter() {
        return timestampFormatter;
    }

    public int getExtensionIndex(String fileName) {
        if (fileName.length() < TIMESTAMP_PATTERN.length() + EXTENSION.length()) {
            throw new RuntimeException("Filename '" + fileName + "' can't be parsed into datetime: too short.");
        }
        if(fileName.contains("..")) {
            throw new RuntimeException("Filename '" + fileName +"' contains invalid path sequence '..'");
        }

        int indexOfExtension = fileName.indexOf(EXTENSION);
        if (indexOfExtension < 0) {
            throw new RuntimeException("Filename '" + fileName + "' can't be parsed into datetime: unknown extension.");
        }
        return indexOfExtension;
    }

    public LocalDateTime getDatetimeFromFileName(String fileName) {
        int indexOfExtension = getExtensionIndex(fileName);
        return LocalDateTime.parse(fileName.substring(0, indexOfExtension), timestampFormatter);
    }

    public Long getTimeFromFileName(String fileName) {
        int indexOfExtension = getExtensionIndex(fileName);
        SimpleDateFormat format = new SimpleDateFormat(TIMESTAMP_PATTERN);
        try {
            Date date = format.parse(fileName.substring(0, indexOfExtension));
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return -1L;
        }
    }
}
