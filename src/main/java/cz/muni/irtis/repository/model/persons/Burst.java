package cz.muni.irtis.repository.model.persons;

import cz.muni.irtis.repository.database.EntityBase;
import cz.muni.irtis.repository.database.identity.BurstEntity;
import cz.muni.irtis.repository.database.identity.PersonEntity;
import cz.muni.irtis.repository.model.RepositoryInstanceBase;
import cz.muni.irtis.repository.model.utils.Time;

import java.lang.reflect.Type;

public class Burst extends RepositoryInstanceBase {
    private Person person;
    private String identifier;
    private String title;
    private Long starting;
    private Long ending;

    public Burst() {
    }

    public Burst(Long id) {
        super(id);
    }

    static public Burst build() {
        return build(Burst.class);
    }

    static public Burst build(Long id) {
        return build(id, Burst.class);
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getStarting() {
        return starting;
    }

    public void setStarting(Long starting) {
        this.starting = starting;
    }

    public Long getEnding() {
        return ending;
    }

    public void setEnding(Long ending) {
        this.ending = ending;
    }

    public boolean isActive() {
        return starting <= Time.getTime() && ending >= Time.getTime();
    }
}
