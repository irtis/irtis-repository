package cz.muni.irtis.repository.model.access;

import cz.muni.irtis.repository.model.RepositoryInstanceBase;

public class Api extends RepositoryInstanceBase {
    private String name;
    private String identifier;
    private String key;
    private Boolean active;

    public Api() {
    }

    public Api(Long id) {
        super(id);
    }

    static public Api build() {
        return build(Api.class);
    }

    static public Api build(Long id) {
        return build(id, Api.class);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
