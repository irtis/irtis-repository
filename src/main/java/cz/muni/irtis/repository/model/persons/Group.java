package cz.muni.irtis.repository.model.persons;

import cz.muni.irtis.repository.model.RepositoryInstanceBase;

public class Group extends RepositoryInstanceBase {
    private String title;

    public Group() {
    }

    public Group(Long id) {
        super(id);
    }

    static public Group build() {
        return build(Group.class);
    }

    static public Group build(Long id) {
        return build(id, Group.class);
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
