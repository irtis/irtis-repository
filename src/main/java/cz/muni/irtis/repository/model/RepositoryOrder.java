package cz.muni.irtis.repository.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class RepositoryOrder {
    private Map<String, String> items;

    private RepositoryOrder() {
        this.items = new HashMap<>();
    }

    static public RepositoryOrder build() {
        return new RepositoryOrder();
    }

    public RepositoryOrder add(String ordering, String ascension) {
        items.put(ordering, ascension);
        return this;
    }

    public RepositoryOrder add(String ordering) {
        return add(ordering, "ASC");
    }


    public boolean isEmpty() {
        return items!=null && items.isEmpty();
    }

    @Override
    public String toString() {
        if(!isEmpty()) {
            StringBuilder output = new StringBuilder();
            // BTW, there is a for-each loop since Java 5 (2004!).
            Iterator<Map.Entry<String, String>> iItems = items.entrySet().iterator();
            while (iItems.hasNext()) {
                Map.Entry<String, String> entry = iItems.next();
                output.append((output.length() > 0 ? ", " : "") + entry.getKey() + " " + entry.getValue().toUpperCase());
            }
            return output.toString();
        } else {
            return null;
        }
    }
}
