package cz.muni.irtis.repository;

import cz.muni.irtis.repository.database.PersistenceConfigLoader;
import cz.muni.irtis.repository.database.PersistenceHandler;
import cz.muni.irtis.repository.model.RepositoryFilter;
import cz.muni.irtis.repository.model.RepositoryLimit;
import cz.muni.irtis.repository.model.RepositoryOrder;
import cz.muni.irtis.repository.model.messages.Message;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.Iterator;
import java.util.List;
import java.util.Properties;

/**
 * Repository
 *
 * Testing point for the Repository API
 */
public class Repository {
    public static void main(String[] args) {
        System.out.println("Repository started");

        Session session = PersistenceHandler.build().getSession();

//        Message item = Message.build(1L);
//        item.setText("Ahoj jsme native");

//        MessagesRepository messages = new MessagesRepository();
//        item = messages.getItem(item);

//        item.setCreated(System.currentTimeMillis());
//        messages.store(item);
//
//        item = Message.build();
//        item.setText("Novinka");
//        messages.store(item);

//        List<Message> items = messages.getItems(RepositoryFilter.build().add("created IS NULL"), RepositoryOrder.build().add("id"), RepositoryLimit.build(3));
//        if(items!=null) {
//            Iterator<Message> iMessages = items.iterator();
//            while (iMessages.hasNext()) {
//                Message message = iMessages.next();
//                System.out.println(message.getId().toString());
//            }
//        }
    }
}
